######################################################
#        SYSTEM MAKE FILE                            #
######################################################
# Use this to allow quick/easy modifications and/or  #
# updates to the software. Requires make to be       #
# installed.                                         #
#                                                    #
# Note: This assumes you already have PHP setup and  #
#   properly configured in your path environment     #
######################################################
# Windows                                            #
#                                                    #
# http://gnuwin32.sourceforge.net/downlinks/make.php #
######################################################
# OS X / Linux                                       #
#                                                    #
# Likely Already Installed. Otherwise use the os's   #
# package manager (IE: apt-get, brew, macports, etc) #
######################################################

BIN_PATH := bin

MAKEFILE_DOWNLOAD_COMMAND = wget -O $@
MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

CACHE_PATH := storage/cache

DOCKER_IMAGE ?= twistersfury/zephir:shared-development
DOCKER_NETWORK = zephir

DEBUG_ENABLE ?= false

APP_DIRECTORY ?= /app

ZEPHIR_BUILD_SCRIPT=ci/scripts/zephir-build.sh

ZEPHIR_EXT_NAME=tf_chatbot

#ZEPHIR_EXT_NAME
ZEPHIR_IMAGE=registry.gitlab.com/twistersfury/chatbot/php:development
PHP_IMAGE=$(ZEPHIR_IMAGE)
CODECEPT_IMAGE=$(ZEPHIR_IMAGE)

ZEPHIR_BUILD_IMAGE=registry.gitlab.com/twistersfury/phalcon-queue/php:development

# Lower Case Function Helper
lc = $(subst A,a,$(subst B,b,$(subst C,c,$(subst D,d,$(subst E,e,$(subst F,f,$(subst G,g,$(subst H,h,$(subst I,i,$(subst J,j,$(subst K,k,$(subst L,l,$(subst M,m,$(subst N,n,$(subst O,o,$(subst P,p,$(subst Q,q,$(subst R,r,$(subst S,s,$(subst T,t,$(subst U,u,$(subst V,v,$(subst W,w,$(subst X,x,$(subst Y,y,$(subst Z,z,$1))))))))))))))))))))))))))

######################################################
#                 Default Run Command                #
######################################################
.PHONY: default
default: analyse

######################################################
# Include Codecept
######################################################
ifndef MAKEFILE_CODECEPT

include $(BIN_PATH)/codecept.mk

$(BIN_PATH)/codecept.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/master/make/codecept.mk
endif

######################################################
# Include Zephir
######################################################
ifndef MAKEFILE_ZEPHIR

include $(BIN_PATH)/zephir.mk

$(BIN_PATH)/zephir.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/master/make/zephir.mk
endif

######################################################
#               Hard File Dependencies               #
######################################################

# Primary Cache Directory
$(CACHE_PATH):
	mkdir -p $@

$(BIN_PATH):
	mkdir -p $@

.env: .env.example
	cp .env.example .env

######################################################
#                  Commmand Line                     #
######################################################

.PHONY: clear-log
clear-log:
	rm -f storage/logs/phalcon.log

.PHONY: cli-connect
cli-connect: PHP_IMAGE=$(ZEPHIR_IMAGE)
cli-connect: PHP_FILE=/app/cli.php
cli-connect: PHP_FILE_ARGS="\\TwistersFury\\ChatBot\\Cli\\Task\\Connect"
cli-connect: PHP_VOLUMES=
cli-connect: docker-stop clear-log zephir-build $(BIN_PATH)/.compose php-compose


.PHONY: cli-queue
cli-queue: PHP_IMAGE=$(ZEPHIR_IMAGE)
cli-queue: PHP_FILE=/app/cli.php
cli-queue: PHP_FILE_ARGS="\\TwistersFury\\Phalcon\\Queue\\Cli\\Task\\Queue" "consume"
cli-queue: PHP_VOLUMES=
cli-queue: $(BIN_PATH)/.compose php-compose

.PHONY: cli-exit
cli-exit: PHP_IMAGE=$(ZEPHIR_IMAGE)
cli-exit: PHP_FILE=/app/cli.php
cli-exit: PHP_FILE_ARGS="\\TwistersFury\\Phalcon\\Queue\\Cli\\Task\\RunJob" "main" "\\TwistersFury\\ChatBot\\Cli\\Job\\QuitJob"
cli-exit: PHP_VOLUMES=
cli-exit: $(BIN_PATH)/.compose php-compose

.PHONY: cli-restart
cli-restart: PHP_IMAGE=$(ZEPHIR_IMAGE)
cli-restart: PHP_FILE=/app/cli.php
cli-restart: PHP_FILE_ARGS="\\TwistersFury\\Phalcon\\Queue\\Cli\\Task\\RunJob" "main" "\\TwistersFury\\Phalcon\\Queue\\Job\\Restart"
cli-restart: PHP_VOLUMES=
cli-restart: $(BIN_PATH)/.compose php-compose

######################################################
#            Installation Dependencies               #
######################################################

.PHONY: install
install: vendor

.PHONY: clean
clean: zephir-full-clean docker-clean
	rm -rf $(BIN_PATH)
	rm -rf $(CACHE_PATH)
	rm -rf vendor
	rm -f compile.log
	rm -f compile-errors.log
	rm -rf .zephir

######################################################
#                   Make Overides                    #
######################################################
.PHONY: docker-compose
docker-compose: .env

