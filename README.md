# Introduction

Twitch\Discord ChatBot

## Getting Started

`make test`

This will install all required dependencies, including composer.

`make test-coverage`

Run the tests in Coverage Mode, mounting `./report` and `./tests/_output` into the image. 

## Debugging/Testing

docker-compose.override.yml:
```yaml
version: "3.5"
services:
  nginx:
    ports:
      - 8080:80/tcp
  php:
    build:
      args:
          ZEPHIR_DEBUG: 1
          ZEPHIR_RELEASE: 0
          REPORT_COVERAGE: 1


```

### Segmentation Fault (Error 139)

1. Locate the most recent command (typically a docker command). Copy/Paste the command and change the command to bash.
2. Once in bash, run `php -i`. If you see a seg fault immeidately, the issue is in the compile of the extension.
3. Run `gdb php`
4. Once in the `gdb` Prompt, run `run`
5. After the run, run `bt`. This will show you a backtrace to identify the error's location.
