<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return [
    "cache" => [
        'adapters' => [
            "redis"
        ],
        'redis' => [
            'defaultSerializer' => 'json',
            'lifetime'          => 300,
            'host'              => $_ENV['ENV_REDIS_HOST'] ?? 'redis.twistersfury.test',
            'port'              => 6379,
            'index'             => 0,
            'prefix'            => ''
        ]
    ]
];
