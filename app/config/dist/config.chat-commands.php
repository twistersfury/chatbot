<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return [
    "chatCommands" => [
        "BOOM" => \TwistersFury\ChatBot\Command\Boom::class
    ]
];
