<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return [
    'connection' => [
        'driver' => 'irc',
        'irc' => [
            'name' => 'irc',
            'adapter' => 'irc',
            'host' => 'irc.twistersfury.test',
            'port' => 4441,
            'user' => [
                'ident'    => 'Feni',
                'realname' => 'Feni',
                'nickname' => 'TacoBot'
            ],
            'channels' => [
                '#Test'
            ]
        ],
        'wait' => 100
    ]
];
