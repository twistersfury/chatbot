<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return [
    'listeners' => [
        'curse' => [
            'event' => 'packet',
            'listener' => \TwistersFury\ChatBot\Listener\Packet\Curse::class
        ],
        'quotes' => [
            'event' => 'packet',
            'listener' => \TwistersFury\ChatBot\Listener\Packet\Quotes::class
        ],
    ]
];
