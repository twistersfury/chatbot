<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return [
    'queue' => [
        'host' => $_ENV['ENV_BEANSTALK_HOST'] ?? 'beanstalk.twistersfury.test',
        'port' => 11300
    ],
    'queues' => [
        'irc' => [
            'host' => $_ENV['ENV_BEANSTALK_HOST'] ?? 'beanstalk.twistersfury.test',
            'port' => 11300,
            'tube' => 'irc',
            //            'keepAlive' => true
        ]
    ]
];
