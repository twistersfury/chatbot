<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return [
    "quotes" => [
        "hello" => "Hello World!",
        "boom" => "Boom Baby!"
    ]
];
