<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */
return [
    "throttle" => [
        "defaults" => [
            "count" => 20,
            "seconds" => 30
        ]
    ]
];
