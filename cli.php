<?php

use TwistersFury\ChatBot\Di\Cli;
use TwistersFury\ChatBot\Cli\Kernel;

try {
    $di = new Cli();
    $bootstrap   = new Kernel($di, __DIR__);
    $application = $bootstrap->getApplication();

    //Building Console Arguments
    $consoleArgs = array(
        'task'   => isset($argv[1]) ? $argv[1] : NULL,
        'action' => isset($argv[2]) ? $argv[2] : NULL
    );

    array_shift($argv);
    array_shift($argv);
    array_shift($argv);

    $consoleArgs['params'] = $argv;

    $application->handle($consoleArgs);
} catch (Exception $exception) {
    if (ini_get('tf_chatbot.debug.mode')) {
        throw $exception;
    }

    exit(1);
} catch (Error $exception) {
    if (ini_get('tf_chatbot.debug.mode')) {
        throw $exception;
    }

    echo "Error Executing Script: " . $exception->getMessage();
    exit(2);
}
