
/* This file was generated automatically by Zephir do not modify it! */

#ifndef PHP_TF_CHATBOT_H
#define PHP_TF_CHATBOT_H 1

#ifdef PHP_WIN32
#define ZEPHIR_RELEASE 1
#endif

#include "kernel/globals.h"

#define PHP_TF_CHATBOT_NAME        "tf_chatbot"
#define PHP_TF_CHATBOT_VERSION     "0.0.1"
#define PHP_TF_CHATBOT_EXTNAME     "tf_chatbot"
#define PHP_TF_CHATBOT_AUTHOR      "Phoenix Osiris <phoenix@twistersfury.com>"
#define PHP_TF_CHATBOT_ZEPVERSION  "0.15.2-$Id$"
#define PHP_TF_CHATBOT_DESCRIPTION "ChatBot"

typedef struct _zephir_struct_debug { 
	zend_bool mode;
} zephir_struct_debug;



ZEND_BEGIN_MODULE_GLOBALS(tf_chatbot)

	int initialized;

	/** Function cache */
	HashTable *fcache;

	zephir_fcall_cache_entry *scache[ZEPHIR_MAX_CACHE_SLOTS];

	/* Cache enabled */
	unsigned int cache_enabled;

	/* Max recursion control */
	unsigned int recursive_lock;

	
	zephir_struct_debug debug;

ZEND_END_MODULE_GLOBALS(tf_chatbot)

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_EXTERN_MODULE_GLOBALS(tf_chatbot)

#ifdef ZTS
	#define ZEPHIR_GLOBAL(v) ZEND_MODULE_GLOBALS_ACCESSOR(tf_chatbot, v)
#else
	#define ZEPHIR_GLOBAL(v) (tf_chatbot_globals.v)
#endif

#ifdef ZTS
	ZEND_TSRMLS_CACHE_EXTERN()
	#define ZEPHIR_VGLOBAL ((zend_tf_chatbot_globals *) (*((void ***) tsrm_get_ls_cache()))[TSRM_UNSHUFFLE_RSRC_ID(tf_chatbot_globals_id)])
#else
	#define ZEPHIR_VGLOBAL &(tf_chatbot_globals)
#endif

#define ZEPHIR_API ZEND_API

#define zephir_globals_def tf_chatbot_globals
#define zend_zephir_globals_def zend_tf_chatbot_globals

extern zend_module_entry tf_chatbot_module_entry;
#define phpext_tf_chatbot_ptr &tf_chatbot_module_entry

#endif
