
/* This file was generated automatically by Zephir do not modify it! */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#include "php_ext.h"
#include "tf_chatbot.h"

#include <ext/standard/info.h>

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/globals.h"
#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"



zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_packet_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_outgoingpacket_ce;
zend_class_entry *twistersfury_chatbot_command_commandinterface_ce;
zend_class_entry *twistersfury_chatbot_connection_driver_driverinterface_ce;
zend_class_entry *twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_hascommands_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_user_ce;
zend_class_entry *twistersfury_chatbot_listener_interfaces_packet_incoming_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_errorinterface_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_hasevent_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_join_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_message_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_unknowninterface_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_abstractpacket_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_ce;
zend_class_entry *twistersfury_chatbot_factory_abstractfactory_ce;
zend_class_entry *twistersfury_chatbot_command_abstractcommand_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_packet_ce;
zend_class_entry *twistersfury_chatbot_helper_socket_socket_ce;
zend_class_entry *twistersfury_chatbot_listener_packet_abstractpacket_ce;
zend_class_entry *twistersfury_chatbot_irc_driver_abstractirc_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_outgoing_abstractoutgoing_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_abstractfactory_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_keepalive_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_user_ce;
zend_class_entry *twistersfury_chatbot_0__closure_ce;
zend_class_entry *twistersfury_chatbot_1__closure_ce;
zend_class_entry *twistersfury_chatbot_2__closure_ce;
zend_class_entry *twistersfury_chatbot_3__closure_ce;
zend_class_entry *twistersfury_chatbot_4__closure_ce;
zend_class_entry *twistersfury_chatbot_5__closure_ce;
zend_class_entry *twistersfury_chatbot_6__closure_ce;
zend_class_entry *twistersfury_chatbot_cli_job_packet_ce;
zend_class_entry *twistersfury_chatbot_cli_job_quitjob_ce;
zend_class_entry *twistersfury_chatbot_cli_kernel_ce;
zend_class_entry *twistersfury_chatbot_cli_task_connecttask_ce;
zend_class_entry *twistersfury_chatbot_command_boom_ce;
zend_class_entry *twistersfury_chatbot_command_factory_ce;
zend_class_entry *twistersfury_chatbot_command_hello_ce;
zend_class_entry *twistersfury_chatbot_command_join_ce;
zend_class_entry *twistersfury_chatbot_command_parser_ce;
zend_class_entry *twistersfury_chatbot_connection_config_channel_ce;
zend_class_entry *twistersfury_chatbot_connection_config_manager_ce;
zend_class_entry *twistersfury_chatbot_connection_driver_exceptions_missingdriverconfig_ce;
zend_class_entry *twistersfury_chatbot_connection_driver_factory_ce;
zend_class_entry *twistersfury_chatbot_connection_job_sendpacket_ce;
zend_class_entry *twistersfury_chatbot_connection_manager_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_builder_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_emptypacket_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_join_ce;
zend_class_entry *twistersfury_chatbot_connection_packet_unknown_ce;
zend_class_entry *twistersfury_chatbot_connection_throttle_exceptions_throttled_ce;
zend_class_entry *twistersfury_chatbot_connection_throttle_listener_driver_ce;
zend_class_entry *twistersfury_chatbot_connection_throttle_manager_ce;
zend_class_entry *twistersfury_chatbot_connection_throttle_tracker_ce;
zend_class_entry *twistersfury_chatbot_di_cli_ce;
zend_class_entry *twistersfury_chatbot_di_http_ce;
zend_class_entry *twistersfury_chatbot_di_serviceprovider_command_ce;
zend_class_entry *twistersfury_chatbot_di_serviceprovider_connection_ce;
zend_class_entry *twistersfury_chatbot_factory_exceptions_invalid_ce;
zend_class_entry *twistersfury_chatbot_helper_socket_connectionexception_ce;
zend_class_entry *twistersfury_chatbot_helper_socket_wrapper_ce;
zend_class_entry *twistersfury_chatbot_irc_driver_classic_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_factory_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_join_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_notice_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_outgoing_channel_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_outgoing_privatemessage_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_parser_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_privatemessage_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_error_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_nickname_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_pass_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_ping_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_pong_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_quitpacket_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_raw_user_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_capability_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_created_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_isupport_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_mode_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_myinfo_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_welcome_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_server_yourhost_ce;
zend_class_entry *twistersfury_chatbot_irc_packet_user_ce;
zend_class_entry *twistersfury_chatbot_job_registerlistener_ce;
zend_class_entry *twistersfury_chatbot_kernel_ce;
zend_class_entry *twistersfury_chatbot_listener_packet_curse_ce;
zend_class_entry *twistersfury_chatbot_listener_packet_quotes_ce;
zend_class_entry *twistersfury_chatbot_support_debug_ce;
zend_class_entry *twistersfury_chatbot_twitch_driver_irc_ce;
zend_class_entry *twistersfury_chatbot_twitch_listener_packet_ce;

ZEND_DECLARE_MODULE_GLOBALS(tf_chatbot)

PHP_INI_BEGIN()
	STD_PHP_INI_BOOLEAN("tf_chatbot.debug.mode", "0", PHP_INI_ALL, OnUpdateBool, debug.mode, zend_tf_chatbot_globals, tf_chatbot_globals)
PHP_INI_END()

static PHP_MINIT_FUNCTION(tf_chatbot)
{
	REGISTER_INI_ENTRIES();
	zephir_module_init();
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_Packet);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_OutgoingPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_CommandInterface);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Driver_DriverInterface);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Throttle_Interfaces_ThrottledPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_HasCommands);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_User);
	ZEPHIR_INIT(TwistersFury_ChatBot_Listener_Interfaces_Packet_Incoming);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_ErrorInterface);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_HasEvent);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_Join);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_Message);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_UnknownInterface);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Interfaces_IncomingPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_AbstractPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw);
	ZEPHIR_INIT(TwistersFury_ChatBot_Factory_AbstractFactory);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_AbstractCommand);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Packet);
	ZEPHIR_INIT(TwistersFury_ChatBot_Helper_Socket_Socket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Listener_Packet_AbstractPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Driver_AbstractIrc);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_AbstractFactory);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_KeepAlive);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_User);
	ZEPHIR_INIT(TwistersFury_ChatBot_Cli_Job_Packet);
	ZEPHIR_INIT(TwistersFury_ChatBot_Cli_Job_QuitJob);
	ZEPHIR_INIT(TwistersFury_ChatBot_Cli_Kernel);
	ZEPHIR_INIT(TwistersFury_ChatBot_Cli_Task_ConnectTask);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_Boom);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_Factory);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_Hello);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_Join);
	ZEPHIR_INIT(TwistersFury_ChatBot_Command_Parser);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Config_Channel);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Config_Manager);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Driver_Exceptions_MissingDriverConfig);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Driver_Factory);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Job_SendPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Manager);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Builder);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_EmptyPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Join);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Packet_Unknown);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Throttle_Exceptions_Throttled);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Throttle_Manager);
	ZEPHIR_INIT(TwistersFury_ChatBot_Connection_Throttle_Tracker);
	ZEPHIR_INIT(TwistersFury_ChatBot_Di_Cli);
	ZEPHIR_INIT(TwistersFury_ChatBot_Di_Http);
	ZEPHIR_INIT(TwistersFury_ChatBot_Di_ServiceProvider_Command);
	ZEPHIR_INIT(TwistersFury_ChatBot_Di_ServiceProvider_Connection);
	ZEPHIR_INIT(TwistersFury_ChatBot_Factory_Exceptions_Invalid);
	ZEPHIR_INIT(TwistersFury_ChatBot_Helper_Socket_ConnectionException);
	ZEPHIR_INIT(TwistersFury_ChatBot_Helper_Socket_Wrapper);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Driver_Classic);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Factory);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Join);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Notice);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Outgoing_Channel);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Outgoing_PrivateMessage);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Parser);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_PrivateMessage);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_Error);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_Nickname);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_Pass);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_Ping);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_Pong);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_QuitPacket);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_Raw_User);
	ZEPHIR_INIT(TwistersFury_ChatBot_Irc_Packet_User);
	ZEPHIR_INIT(TwistersFury_ChatBot_Job_RegisterListener);
	ZEPHIR_INIT(TwistersFury_ChatBot_Kernel);
	ZEPHIR_INIT(TwistersFury_ChatBot_Listener_Packet_Curse);
	ZEPHIR_INIT(TwistersFury_ChatBot_Listener_Packet_Quotes);
	ZEPHIR_INIT(TwistersFury_ChatBot_Support_Debug);
	ZEPHIR_INIT(TwistersFury_ChatBot_Twitch_Driver_Irc);
	ZEPHIR_INIT(TwistersFury_ChatBot_Twitch_Listener_Packet);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_Capability);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_Created);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_ISupport);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_Mode);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_MyInfo);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_Welcome);
	ZEPHIR_INIT(Twistersfury_ChatBot_Irc_Packet_Server_YourHost);
	ZEPHIR_INIT(twistersfury_chatbot_0__closure);
	ZEPHIR_INIT(twistersfury_chatbot_1__closure);
	ZEPHIR_INIT(twistersfury_chatbot_2__closure);
	ZEPHIR_INIT(twistersfury_chatbot_3__closure);
	ZEPHIR_INIT(twistersfury_chatbot_4__closure);
	ZEPHIR_INIT(twistersfury_chatbot_5__closure);
	ZEPHIR_INIT(twistersfury_chatbot_6__closure);
	
	return SUCCESS;
}

#ifndef ZEPHIR_RELEASE
static PHP_MSHUTDOWN_FUNCTION(tf_chatbot)
{
	
	zephir_deinitialize_memory();
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}
#endif

/**
 * Initialize globals on each request or each thread started
 */
static void php_zephir_init_globals(zend_tf_chatbot_globals *tf_chatbot_globals)
{
	tf_chatbot_globals->initialized = 0;

	/* Cache Enabled */
	tf_chatbot_globals->cache_enabled = 1;

	/* Recursive Lock */
	tf_chatbot_globals->recursive_lock = 0;

	/* Static cache */
	memset(tf_chatbot_globals->scache, '\0', sizeof(zephir_fcall_cache_entry*) * ZEPHIR_MAX_CACHE_SLOTS);

	
	
}

/**
 * Initialize globals only on each thread started
 */
static void php_zephir_init_module_globals(zend_tf_chatbot_globals *tf_chatbot_globals)
{
	
}

static PHP_RINIT_FUNCTION(tf_chatbot)
{
	zend_tf_chatbot_globals *tf_chatbot_globals_ptr;
	tf_chatbot_globals_ptr = ZEPHIR_VGLOBAL;

	php_zephir_init_globals(tf_chatbot_globals_ptr);
	zephir_initialize_memory(tf_chatbot_globals_ptr);

	
	return SUCCESS;
}

static PHP_RSHUTDOWN_FUNCTION(tf_chatbot)
{
	
	zephir_deinitialize_memory();
	return SUCCESS;
}



static PHP_MINFO_FUNCTION(tf_chatbot)
{
	php_info_print_box_start(0);
	php_printf("%s", PHP_TF_CHATBOT_DESCRIPTION);
	php_info_print_box_end();

	php_info_print_table_start();
	php_info_print_table_header(2, PHP_TF_CHATBOT_NAME, "enabled");
	php_info_print_table_row(2, "Author", PHP_TF_CHATBOT_AUTHOR);
	php_info_print_table_row(2, "Version", PHP_TF_CHATBOT_VERSION);
	php_info_print_table_row(2, "Build Date", __DATE__ " " __TIME__ );
	php_info_print_table_row(2, "Powered by Zephir", "Version " PHP_TF_CHATBOT_ZEPVERSION);
	php_info_print_table_end();
	
	DISPLAY_INI_ENTRIES();
}

static PHP_GINIT_FUNCTION(tf_chatbot)
{
#if defined(COMPILE_DL_TF_CHATBOT) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	php_zephir_init_globals(tf_chatbot_globals);
	php_zephir_init_module_globals(tf_chatbot_globals);
}

static PHP_GSHUTDOWN_FUNCTION(tf_chatbot)
{
	
}


zend_function_entry php_tf_chatbot_functions[] = {
	ZEND_FE_END

};

static const zend_module_dep php_tf_chatbot_deps[] = {
	ZEND_MOD_REQUIRED("phalcon")
	ZEND_MOD_REQUIRED("tf_shared")
	ZEND_MOD_REQUIRED("tf_queue")
	ZEND_MOD_END
};

zend_module_entry tf_chatbot_module_entry = {
	STANDARD_MODULE_HEADER_EX,
	NULL,
	php_tf_chatbot_deps,
	PHP_TF_CHATBOT_EXTNAME,
	php_tf_chatbot_functions,
	PHP_MINIT(tf_chatbot),
#ifndef ZEPHIR_RELEASE
	PHP_MSHUTDOWN(tf_chatbot),
#else
	NULL,
#endif
	PHP_RINIT(tf_chatbot),
	PHP_RSHUTDOWN(tf_chatbot),
	PHP_MINFO(tf_chatbot),
	PHP_TF_CHATBOT_VERSION,
	ZEND_MODULE_GLOBALS(tf_chatbot),
	PHP_GINIT(tf_chatbot),
	PHP_GSHUTDOWN(tf_chatbot),
#ifdef ZEPHIR_POST_REQUEST
	PHP_PRSHUTDOWN(tf_chatbot),
#else
	NULL,
#endif
	STANDARD_MODULE_PROPERTIES_EX
};

/* implement standard "stub" routine to introduce ourselves to Zend */
#ifdef COMPILE_DL_TF_CHATBOT
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(tf_chatbot)
#endif
