
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(twistersfury_chatbot_2__closure)
{
	ZEPHIR_REGISTER_CLASS(twistersfury\\chatbot, 2__closure, twistersfury_chatbot, 2__closure, twistersfury_chatbot_2__closure_method_entry, ZEND_ACC_FINAL_CLASS);

	return SUCCESS;
}

PHP_METHOD(twistersfury_chatbot_2__closure, __invoke)
{
	zval _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *config = NULL, config_sub, __$null, _0$$3, _1$$3, _3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&config_sub);
	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_OBJECT_OF_CLASS_OR_NULL(config, zephir_get_internal_ce(SL("phalcon\\config\\config")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &config);
	if (!config) {
		config = &config_sub;
		config = &__$null;
	} else {
		ZEPHIR_SEPARATE_PARAM(config);
	}


	if (Z_TYPE_P(config) == IS_NULL) {
		ZEPHIR_INIT_VAR(&_1$$3);
		ZVAL_STRING(&_1$$3, "config");
		ZEPHIR_CALL_METHOD(&_0$$3, this_ptr, "getshared", NULL, 0, &_1$$3);
		zephir_check_call_status();
		ZEPHIR_OBS_NVAR(config);
		zephir_read_property(config, &_0$$3, ZEND_STRL("connection"), PH_NOISY_CC);
	}
	ZEPHIR_INIT_VAR(&_2);
	zephir_create_array(&_2, 1, 0);
	zephir_array_fast_append(&_2, config);
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "TwistersFury\\ChatBot\\Connection\\Manager");
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "get", NULL, 0, &_3, &_2);
	zephir_check_call_status();
	RETURN_MM();
}

