
extern zend_class_entry *twistersfury_chatbot_2__closure_ce;

ZEPHIR_INIT_CLASS(twistersfury_chatbot_2__closure);

PHP_METHOD(twistersfury_chatbot_2__closure, __invoke);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_2__closure___invoke, 0, 0, 0)
	ZEND_ARG_OBJ_INFO(0, config, Phalcon\\Config\\Config, 1)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_2__closure_method_entry) {
	PHP_ME(twistersfury_chatbot_2__closure, __invoke, arginfo_twistersfury_chatbot_2__closure___invoke, ZEND_ACC_PUBLIC|ZEND_ACC_FINAL)
	PHP_FE_END
};
