
extern zend_class_entry *twistersfury_chatbot_4__closure_ce;

ZEPHIR_INIT_CLASS(twistersfury_chatbot_4__closure);

PHP_METHOD(twistersfury_chatbot_4__closure, __invoke);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_4__closure___invoke, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_4__closure_method_entry) {
#if PHP_VERSION_ID >= 80000
	PHP_ME(twistersfury_chatbot_4__closure, __invoke, arginfo_twistersfury_chatbot_4__closure___invoke, ZEND_ACC_PUBLIC|ZEND_ACC_FINAL)
#else
	PHP_ME(twistersfury_chatbot_4__closure, __invoke, NULL, ZEND_ACC_PUBLIC|ZEND_ACC_FINAL)
#endif
	PHP_FE_END
};
