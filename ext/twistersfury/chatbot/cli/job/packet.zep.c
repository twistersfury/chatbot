
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/fcall.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Cli_Job_Packet)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Cli\\Job, Packet, twistersfury_chatbot, cli_job_packet, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_cli_job_packet_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_cli_job_packet_ce, SL("packet"), ZEND_ACC_PRIVATE);
	zend_class_implements(twistersfury_chatbot_cli_job_packet_ce, 1, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\job\\jobinterface")));
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, getPacket)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "packet");
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, __construct)
{
	zval *packet, packet_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_packet_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &packet);


	zephir_update_property_zval(this_ptr, ZEND_STRL("packet"), packet);
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, __sleep)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	zephir_create_array(return_value, 1, 0);
	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "packet");
	zephir_array_fast_append(return_value, &_0);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, __wakeup)
{
	zend_class_entry *_1;
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	_1 = zephir_fetch_class_str_ex(SL("Phalcon\\Di\\Di"), ZEND_FETCH_CLASS_AUTO);
	ZEPHIR_CALL_CE_STATIC(&_0, _1, "getdefault", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "setdi", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, handle)
{
	zval _0, _1, _2, _5, _6, _3$$4, _4$$4, _7$$6, _8$$6;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&_4$$4);
	ZVAL_UNDEF(&_7$$6);
	ZVAL_UNDEF(&_8$$6);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Packet Job Processing Packet");
	ZEPHIR_CALL_METHOD(NULL, &_0, "info", NULL, 0, &_1);
	zephir_check_call_status();

	/* try_start_1: */

		ZEPHIR_CALL_METHOD(NULL, this_ptr, "processevents", NULL, 8);
		zephir_check_call_status_or_jump(try_end_1);

	try_end_1:

	if (EG(exception)) {
		ZEPHIR_INIT_NVAR(&_1);
		ZVAL_OBJ(&_1, EG(exception));
		Z_ADDREF_P(&_1);
		ZEPHIR_INIT_VAR(&_2);
		if (zephir_is_instance_of(&_1, SL("Phalcon\\Events\\Exception"))) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&_2, &_1);
			ZEPHIR_OBS_VAR(&_3$$4);
			zephir_read_property(&_3$$4, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
			ZEPHIR_INIT_VAR(&_4$$4);
			ZVAL_STRING(&_4$$4, "Event Exception");
			ZEPHIR_CALL_METHOD(NULL, &_3$$4, "debug", NULL, 0, &_4$$4);
			zephir_check_call_status();
		}
	}

	/* try_start_2: */

		ZEPHIR_CALL_METHOD(NULL, this_ptr, "handlejobs", NULL, 9);
		zephir_check_call_status_or_jump(try_end_2);

	try_end_2:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_5);
		ZVAL_OBJ(&_5, EG(exception));
		Z_ADDREF_P(&_5);
		ZEPHIR_INIT_VAR(&_6);
		if (zephir_is_instance_of(&_5, SL("TwistersFury\\ChatBot\\Cli\\Job\\Invalid"))) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&_6, &_5);
			ZEPHIR_OBS_VAR(&_7$$6);
			zephir_read_property(&_7$$6, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
			ZEPHIR_INIT_VAR(&_8$$6);
			ZVAL_STRING(&_8$$6, "Invalid Command");
			ZEPHIR_CALL_METHOD(NULL, &_7$$6, "debug", NULL, 0, &_8$$6);
			zephir_check_call_status();
		}
	}
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, handleJobs)
{
	zval _0, _1, jobs, job, *_2, _3, _4$$3, _5$$3, _6$$4, _7$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&jobs);
	ZVAL_UNDEF(&job);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$4);
	ZVAL_UNDEF(&_7$$4);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Packet Job - Handle Jobs");
	ZEPHIR_CALL_METHOD(NULL, &_0, "info", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&jobs, this_ptr, "parsepacket", NULL, 10);
	zephir_check_call_status();
	zephir_is_iterable(&jobs, 0, "twistersfury/chatbot/Cli/Job/Packet.zep", 75);
	if (Z_TYPE_P(&jobs) == IS_ARRAY) {
		ZEND_HASH_FOREACH_VAL(Z_ARRVAL_P(&jobs), _2)
		{
			ZEPHIR_INIT_NVAR(&job);
			ZVAL_COPY(&job, _2);
			ZEPHIR_OBS_NVAR(&_4$$3);
			zephir_read_property(&_4$$3, this_ptr, ZEND_STRL("queue"), PH_NOISY_CC);
			ZEPHIR_CALL_METHOD(&_5$$3, &job, "getqueueoptions", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, &_4$$3, "put", NULL, 0, &job, &_5$$3);
			zephir_check_call_status();
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, &jobs, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_3, &jobs, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_3)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&job, &jobs, "current", NULL, 0);
			zephir_check_call_status();
				ZEPHIR_OBS_NVAR(&_6$$4);
				zephir_read_property(&_6$$4, this_ptr, ZEND_STRL("queue"), PH_NOISY_CC);
				ZEPHIR_CALL_METHOD(&_7$$4, &job, "getqueueoptions", NULL, 0);
				zephir_check_call_status();
				ZEPHIR_CALL_METHOD(NULL, &_6$$4, "put", NULL, 0, &job, &_7$$4);
				zephir_check_call_status();
			ZEPHIR_CALL_METHOD(NULL, &jobs, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&job);
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, processEvents)
{
	zval _3;
	zval _0, _2, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_1 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_13);
	ZVAL_UNDEF(&_14);
	ZVAL_UNDEF(&_15);
	ZVAL_UNDEF(&_16);
	ZVAL_UNDEF(&_3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	if (!(zephir_instance_of_ev(&_0, twistersfury_chatbot_connection_packet_interfaces_hasevent_ce))) {
		RETURN_THIS();
	}
	ZEPHIR_OBS_VAR(&_2);
	zephir_read_property(&_2, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 1, 0);
	ZEPHIR_INIT_VAR(&_4);
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	zephir_get_class(&_4, &_5, 0);
	zephir_array_update_string(&_3, SL("class"), &_4, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_4);
	ZVAL_STRING(&_4, "Packet Job - Fire Events");
	ZEPHIR_CALL_METHOD(NULL, &_2, "debug", NULL, 0, &_4, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_7, &_6, "getchannelconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_8, &_7, "geteventsmanager", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_9, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_10, &_9, "geteventname", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_11, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_8, "fire", NULL, 0, &_10, &_11);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_12, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_4);
	ZVAL_STRING(&_4, "eventsManager");
	ZEPHIR_CALL_METHOD(&_13, &_12, "get", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_14, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_15, &_14, "geteventname", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_16, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_13, "fire", NULL, 0, &_15, &_16);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, parsePacket)
{
	zval _3$$3;
	zval _0, _2$$3, _4$$3, _5$$3, _6$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_1 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_3$$3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getpacket", &_1, 0);
	zephir_check_call_status();
	if (zephir_instance_of_ev(&_0, twistersfury_chatbot_connection_packet_interfaces_hascommands_ce)) {
		ZEPHIR_OBS_VAR(&_2$$3);
		zephir_read_property(&_2$$3, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(&_3$$3);
		zephir_create_array(&_3$$3, 2, 0);
		ZEPHIR_CALL_METHOD(&_4$$3, this_ptr, "getpacket", &_1, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_5$$3, &_4$$3, "getmessage", NULL, 0);
		zephir_check_call_status();
		zephir_array_update_string(&_3$$3, SL("message"), &_5$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_CALL_METHOD(&_5$$3, this_ptr, "getpacket", &_1, 0);
		zephir_check_call_status();
		zephir_array_update_string(&_3$$3, SL("packet"), &_5$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_VAR(&_6$$3);
		ZVAL_STRING(&_6$$3, "Parsing Packet");
		ZEPHIR_CALL_METHOD(NULL, &_2$$3, "debug", NULL, 0, &_6$$3, &_3$$3);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_5$$3, this_ptr, "getpacket", &_1, 0);
		zephir_check_call_status();
		ZEPHIR_RETURN_CALL_METHOD(&_5$$3, "buildcommands", NULL, 0);
		zephir_check_call_status();
		RETURN_MM();
	}
	array_init(return_value);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, getOptions)
{
	zval *this_ptr = getThis();



	array_init(return_value);
	return;
}

