
extern zend_class_entry *twistersfury_chatbot_cli_job_packet_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Cli_Job_Packet);

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, getPacket);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, __construct);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, __sleep);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, __wakeup);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, handle);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, handleJobs);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, processEvents);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, parsePacket);
PHP_METHOD(TwistersFury_ChatBot_Cli_Job_Packet, getOptions);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet_getpacket, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet___construct, 0, 0, 1)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet___sleep, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet___wakeup, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet_handle, 0, 0, TwistersFury\\Phalcon\\Queue\\Job\\JobInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet_handlejobs, 0, 0, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet_processevents, 0, 0, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet_parsepacket, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_cli_job_packet_getoptions, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_cli_job_packet_method_entry) {
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, getPacket, arginfo_twistersfury_chatbot_cli_job_packet_getpacket, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, getPacket, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, __construct, arginfo_twistersfury_chatbot_cli_job_packet___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, __sleep, arginfo_twistersfury_chatbot_cli_job_packet___sleep, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, __wakeup, arginfo_twistersfury_chatbot_cli_job_packet___wakeup, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, handle, arginfo_twistersfury_chatbot_cli_job_packet_handle, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, handleJobs, arginfo_twistersfury_chatbot_cli_job_packet_handlejobs, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, processEvents, arginfo_twistersfury_chatbot_cli_job_packet_processevents, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, parsePacket, arginfo_twistersfury_chatbot_cli_job_packet_parsepacket, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Cli_Job_Packet, getOptions, arginfo_twistersfury_chatbot_cli_job_packet_getoptions, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
