
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/fcall.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Cli_Job_QuitJob)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Cli\\Job, QuitJob, twistersfury_chatbot, cli_job_quitjob, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\job\\exitjob")), twistersfury_chatbot_cli_job_quitjob_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Job_QuitJob, handle)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Initializing Exit");
	ZEPHIR_CALL_METHOD(NULL, &_0, "notice", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_2);
	zephir_read_property(&_2, this_ptr, ZEND_STRL("irc"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(NULL, &_2, "put", NULL, 0, this_ptr);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_PARENT(twistersfury_chatbot_cli_job_quitjob_ce, getThis(), "handle", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

