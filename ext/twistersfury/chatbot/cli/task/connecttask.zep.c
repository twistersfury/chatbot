
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Cli_Task_ConnectTask)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Cli\\Task, ConnectTask, twistersfury_chatbot, cli_task_connecttask, zephir_get_internal_ce(SL("phalcon\\cli\\task")), twistersfury_chatbot_cli_task_connecttask_method_entry, 0);

	zend_declare_property_bool(twistersfury_chatbot_cli_task_connecttask_ce, SL("allowExit"), 0, ZEND_ACC_PRIVATE);
	zend_declare_property_long(twistersfury_chatbot_cli_task_connecttask_ce, SL("wait"), 100, ZEND_ACC_PRIVATE);
	zend_class_implements(twistersfury_chatbot_cli_task_connecttask_ce, 1, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\interfaces\\initializationaware")));
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Task_ConnectTask, initialize)
{
	zval _0, _1, _2, _3, _4, _5, _6, _7, _8$$3, _9$$3, _10$$3, _11$$3, _12$$3, _13$$4, _14$$4, _15$$4, _16$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_9$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_12$$3);
	ZVAL_UNDEF(&_13$$4);
	ZVAL_UNDEF(&_14$$4);
	ZVAL_UNDEF(&_15$$4);
	ZVAL_UNDEF(&_16$$4);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "dispatcher");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZVAL_LONG(&_4, 0);
	ZEPHIR_CALL_METHOD(&_3, &_1, "getparam", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "config");
	ZEPHIR_CALL_METHOD(&_6, &_5, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "wait");
	ZEPHIR_CALL_METHOD(&_7, &_6, "get", NULL, 0, &_2);
	zephir_check_call_status();
	if (zephir_is_true(&_3)) {
		ZEPHIR_CALL_METHOD(&_8$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_10$$3);
		ZVAL_STRING(&_10$$3, "dispatcher");
		ZEPHIR_CALL_METHOD(&_9$$3, &_8$$3, "get", NULL, 0, &_10$$3);
		zephir_check_call_status();
		ZVAL_LONG(&_12$$3, 0);
		ZEPHIR_CALL_METHOD(&_11$$3, &_9$$3, "getparam", NULL, 0, &_12$$3);
		zephir_check_call_status();
		zephir_update_property_zval(this_ptr, ZEND_STRL("wait"), &_11$$3);
	} else if (zephir_is_true(&_7)) {
		ZEPHIR_CALL_METHOD(&_13$$4, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_15$$4);
		ZVAL_STRING(&_15$$4, "config");
		ZEPHIR_CALL_METHOD(&_14$$4, &_13$$4, "get", NULL, 0, &_15$$4);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(&_15$$4);
		ZVAL_STRING(&_15$$4, "wait");
		ZEPHIR_CALL_METHOD(&_16$$4, &_14$$4, "get", NULL, 0, &_15$$4);
		zephir_check_call_status();
		zephir_update_property_zval(this_ptr, ZEND_STRL("wait"), &_16$$4);
	}
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Task_ConnectTask, mainAction)
{
	zend_bool _7;
	zval _3, _39, _22$$5, _23$$5, _34$$9;
	zval _0, _1, _2, _4, packet, _6, _8, _9, exception, _36, _37, _38, _40, _11$$3, _24$$3, _25$$3, _26$$3, _14$$4, _15$$4, _16$$4, _17$$4, _18$$5, _19$$5, _20$$5, _21$$5, _28$$7, _30$$8, _31$$9, _32$$9, _33$$9, _35$$9;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_5 = NULL, *_10 = NULL, *_12 = NULL, *_13 = NULL, *_27 = NULL, *_29 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&packet);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&exception);
	ZVAL_UNDEF(&_36);
	ZVAL_UNDEF(&_37);
	ZVAL_UNDEF(&_38);
	ZVAL_UNDEF(&_40);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_24$$3);
	ZVAL_UNDEF(&_25$$3);
	ZVAL_UNDEF(&_26$$3);
	ZVAL_UNDEF(&_14$$4);
	ZVAL_UNDEF(&_15$$4);
	ZVAL_UNDEF(&_16$$4);
	ZVAL_UNDEF(&_17$$4);
	ZVAL_UNDEF(&_18$$5);
	ZVAL_UNDEF(&_19$$5);
	ZVAL_UNDEF(&_20$$5);
	ZVAL_UNDEF(&_21$$5);
	ZVAL_UNDEF(&_28$$7);
	ZVAL_UNDEF(&_30$$8);
	ZVAL_UNDEF(&_31$$9);
	ZVAL_UNDEF(&_32$$9);
	ZVAL_UNDEF(&_33$$9);
	ZVAL_UNDEF(&_35$$9);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_39);
	ZVAL_UNDEF(&_22$$5);
	ZVAL_UNDEF(&_23$$5);
	ZVAL_UNDEF(&_34$$9);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 1, 0);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Y-m-d H:i:s");
	ZEPHIR_CALL_FUNCTION(&_4, "date", &_5, 11, &_2);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("start"), &_4, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Connect Task Start");
	ZEPHIR_CALL_METHOD(NULL, &_1, "info", NULL, 0, &_2, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getmanager", NULL, 12);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_4, "openconnection", NULL, 0);
	zephir_check_call_status();
	while (1) {
		zephir_read_property(&_6, this_ptr, ZEND_STRL("allowExit"), PH_NOISY_CC | PH_READONLY);
		_7 = ZEPHIR_IS_FALSE_IDENTICAL(&_6);
		if (_7) {
			ZEPHIR_CALL_METHOD(&_8, this_ptr, "getmanager", NULL, 12);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&_9, &_8, "isconnected", &_10, 0);
			zephir_check_call_status();
			_7 = zephir_is_true(&_9);
		}
		if (!(_7)) {
			break;
		}
		ZEPHIR_CALL_METHOD(&_11$$3, this_ptr, "getmanager", NULL, 12);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&packet, &_11$$3, "readpacket", &_12, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "processpacket", &_13, 13, &packet);
		zephir_check_call_status();

		/* try_start_1: */

			ZEPHIR_CALL_METHOD(&_14$$4, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_INIT_NVAR(&_16$$4);
			ZVAL_STRING(&_16$$4, "irc");
			ZEPHIR_CALL_METHOD(&_15$$4, &_14$$4, "get", NULL, 0, &_16$$4);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_CALL_METHOD(&_17$$4, &_15$$4, "peekready", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			if (zephir_is_true(&_17$$4)) {
				ZEPHIR_CALL_METHOD(&_18$$5, this_ptr, "getdi", NULL, 0);
				zephir_check_call_status_or_jump(try_end_1);
				ZEPHIR_INIT_NVAR(&_20$$5);
				ZVAL_STRING(&_20$$5, "kernel");
				ZEPHIR_CALL_METHOD(&_19$$5, &_18$$5, "get", NULL, 0, &_20$$5);
				zephir_check_call_status_or_jump(try_end_1);
				ZEPHIR_CALL_METHOD(&_21$$5, &_19$$5, "getapplication", NULL, 0);
				zephir_check_call_status_or_jump(try_end_1);
				ZEPHIR_INIT_NVAR(&_22$$5);
				zephir_create_array(&_22$$5, 3, 0);
				add_assoc_stringl_ex(&_22$$5, SL("task"), SL("TwistersFury\\Phalcon\\Queue\\Cli\\Task\\Queue"));
				add_assoc_stringl_ex(&_22$$5, SL("action"), SL("consume"));
				ZEPHIR_INIT_NVAR(&_23$$5);
				zephir_create_array(&_23$$5, 2, 0);
				ZEPHIR_INIT_NVAR(&_20$$5);
				ZVAL_STRING(&_20$$5, "irc");
				zephir_array_fast_append(&_23$$5, &_20$$5);
				ZEPHIR_INIT_NVAR(&_20$$5);
				ZVAL_LONG(&_20$$5, 1);
				zephir_array_fast_append(&_23$$5, &_20$$5);
				zephir_array_update_string(&_22$$5, SL("params"), &_23$$5, PH_COPY | PH_SEPARATE);
				ZEPHIR_CALL_METHOD(NULL, &_21$$5, "handle", NULL, 0, &_22$$5);
				zephir_check_call_status_or_jump(try_end_1);
			}

		try_end_1:

		if (EG(exception)) {
			ZEPHIR_INIT_NVAR(&_24$$3);
			ZVAL_OBJ(&_24$$3, EG(exception));
			Z_ADDREF_P(&_24$$3);
			ZEPHIR_INIT_NVAR(&_25$$3);
			if (zephir_is_instance_of(&_24$$3, SL("TwistersFury\\Phalcon\\Queue\\Exceptions\\ExitException"))) {
				zend_clear_exception();
				ZEPHIR_CPY_WRT(&_25$$3, &_24$$3);
				break;
			}
		}
		ZEPHIR_CALL_METHOD(&_26$$3, this_ptr, "getwait", &_27, 0);
		zephir_check_call_status();
		if (EXPECTED(ZEPHIR_GT_LONG(&_26$$3, 0))) {
			ZEPHIR_CALL_METHOD(&_28$$7, this_ptr, "getwait", &_27, 0);
			zephir_check_call_status();
			ZEPHIR_CALL_FUNCTION(NULL, "usleep", &_29, 14, &_28$$7);
			zephir_check_call_status();
		}
	}

	/* try_start_2: */

		ZEPHIR_CALL_METHOD(&_30$$8, this_ptr, "getmanager", NULL, 12);
		zephir_check_call_status_or_jump(try_end_2);
		ZEPHIR_CALL_METHOD(NULL, &_30$$8, "closeconnection", NULL, 0);
		zephir_check_call_status_or_jump(try_end_2);

	try_end_2:

	if (EG(exception)) {
		ZEPHIR_INIT_NVAR(&_2);
		ZVAL_OBJ(&_2, EG(exception));
		Z_ADDREF_P(&_2);
		if (zephir_is_instance_of(&_2, SL("TwistersFury\\ChatBot\\Cli\\Task\\Throwable"))) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&exception, &_2);
			ZEPHIR_CALL_METHOD(&_31$$9, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_33$$9);
			ZVAL_STRING(&_33$$9, "logger");
			ZEPHIR_CALL_METHOD(&_32$$9, &_31$$9, "get", NULL, 0, &_33$$9);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_34$$9);
			zephir_create_array(&_34$$9, 1, 0);
			ZEPHIR_CALL_METHOD(&_35$$9, &exception, "getmessage", NULL, 0);
			zephir_check_call_status();
			zephir_array_update_string(&_34$$9, SL("message"), &_35$$9, PH_COPY | PH_SEPARATE);
			ZEPHIR_INIT_NVAR(&_33$$9);
			ZVAL_STRING(&_33$$9, "Error Closing Connection");
			ZEPHIR_CALL_METHOD(NULL, &_32$$9, "warning", NULL, 0, &_33$$9, &_34$$9);
			zephir_check_call_status();
		}
	}
	ZEPHIR_CALL_METHOD(&_36, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_38);
	ZVAL_STRING(&_38, "logger");
	ZEPHIR_CALL_METHOD(&_37, &_36, "get", NULL, 0, &_38);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_39);
	zephir_create_array(&_39, 1, 0);
	ZEPHIR_INIT_NVAR(&_38);
	ZVAL_STRING(&_38, "Y-m-d H:i:s");
	ZEPHIR_CALL_FUNCTION(&_40, "date", &_5, 11, &_38);
	zephir_check_call_status();
	zephir_array_update_string(&_39, SL("start"), &_40, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_38);
	ZVAL_STRING(&_38, "Connect Task Stop");
	ZEPHIR_CALL_METHOD(NULL, &_37, "info", NULL, 0, &_38, &_39);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Task_ConnectTask, processPacket)
{
	zval _9, _13;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, __$true, __$false, _0$$3, _1$$3, _2$$4, _3$$5, _4$$5, _5$$5, _6, _7, _8, _10, _11, _12;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_BOOL(&__$true, 1);
	ZVAL_BOOL(&__$false, 0);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_2$$4);
	ZVAL_UNDEF(&_3$$5);
	ZVAL_UNDEF(&_4$$5);
	ZVAL_UNDEF(&_5$$5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_13);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_packet_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	if (UNEXPECTED(zephir_instance_of_ev(packet, twistersfury_chatbot_connection_packet_errorinterface_ce))) {
		ZEPHIR_OBS_VAR(&_0$$3);
		zephir_read_property(&_0$$3, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
		ZEPHIR_CALL_METHOD(&_1$$3, packet, "getmessage", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, &_0$$3, "error", NULL, 0, &_1$$3);
		zephir_check_call_status();
		if (1) {
			zephir_update_property_zval(this_ptr, ZEND_STRL("allowExit"), &__$true);
		} else {
			zephir_update_property_zval(this_ptr, ZEND_STRL("allowExit"), &__$false);
		}
		RETURN_MM_NULL();
	} else if (zephir_instance_of_ev(packet, twistersfury_chatbot_connection_packet_keepalive_ce)) {
		ZEPHIR_CALL_METHOD(&_2$$4, this_ptr, "getmanager", NULL, 12);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, &_2$$4, "keepalive", NULL, 0, packet);
		zephir_check_call_status();
		RETURN_MM_NULL();
	} else if (zephir_instance_of_ev(packet, twistersfury_chatbot_connection_packet_unknowninterface_ce)) {
		ZEPHIR_CALL_METHOD(&_3$$5, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_5$$5);
		ZVAL_STRING(&_5$$5, "logger");
		ZEPHIR_CALL_METHOD(&_4$$5, &_3$$5, "get", NULL, 0, &_5$$5);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(&_5$$5);
		ZVAL_STRING(&_5$$5, "Unknown Packet");
		ZEPHIR_CALL_METHOD(NULL, &_4$$5, "notice", NULL, 0, &_5$$5);
		zephir_check_call_status();
		RETURN_MM_NULL();
	} else if (zephir_instance_of_ev(packet, twistersfury_chatbot_connection_packet_emptypacket_ce)) {
		RETURN_MM_NULL();
	}
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_8);
	ZVAL_STRING(&_8, "logger");
	ZEPHIR_CALL_METHOD(&_7, &_6, "get", NULL, 0, &_8);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_9);
	zephir_create_array(&_9, 1, 0);
	ZEPHIR_CALL_FUNCTION(&_10, "serialize", NULL, 3, packet);
	zephir_check_call_status();
	zephir_array_update_string(&_9, SL("packet"), &_10, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_8);
	ZVAL_STRING(&_8, "Packet Received");
	ZEPHIR_CALL_METHOD(NULL, &_7, "info", NULL, 0, &_8, &_9);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_11);
	zephir_read_property(&_11, this_ptr, ZEND_STRL("queue"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_10, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_13);
	zephir_create_array(&_13, 1, 0);
	zephir_array_fast_append(&_13, packet);
	ZEPHIR_INIT_NVAR(&_8);
	ZVAL_STRING(&_8, "TwistersFury\\ChatBot\\Cli\\Job\\Packet");
	ZEPHIR_CALL_METHOD(&_12, &_10, "get", NULL, 0, &_8, &_13);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_11, "put", NULL, 0, &_12);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Task_ConnectTask, getManager)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "connectionManager");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Cli_Task_ConnectTask, getWait)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "wait");
}

