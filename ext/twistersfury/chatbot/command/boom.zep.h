
extern zend_class_entry *twistersfury_chatbot_command_boom_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Boom);

PHP_METHOD(TwistersFury_ChatBot_Command_Boom, processCommand);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_command_boom_processcommand, 0, 0, TwistersFury\\ChatBot\\Command\\AbstractCommand, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_command_boom_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Command_Boom, processCommand, arginfo_twistersfury_chatbot_command_boom_processcommand, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
