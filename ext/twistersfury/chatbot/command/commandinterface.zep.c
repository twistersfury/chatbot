
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_CommandInterface)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Command, CommandInterface, twistersfury_chatbot, command_commandinterface, twistersfury_chatbot_command_commandinterface_method_entry);

	zend_class_implements(twistersfury_chatbot_command_commandinterface_ce, 1, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\job\\jobinterface")));
	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Command_CommandInterface, getQueueOptions);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Command_CommandInterface, getPacket);
