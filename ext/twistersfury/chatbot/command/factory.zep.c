
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/operators.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Factory)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Command, Factory, twistersfury_chatbot, command_factory, twistersfury_chatbot_factory_abstractfactory_ce, twistersfury_chatbot_command_factory_method_entry, 0);

	return SUCCESS;
}

/**
 * Returns the adapters for the factory
 */
PHP_METHOD(TwistersFury_ChatBot_Command_Factory, getServices)
{
	zval _1;
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	zephir_create_array(&_0, 2, 0);
	add_assoc_stringl_ex(&_0, SL("HELLO"), SL("TwistersFury\\Chatbot\\Command\\Hello"));
	add_assoc_stringl_ex(&_0, SL("BOOM"), SL("TwistersFury\\Chatbot\\Command\\Boom"));
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getconfiguredcommands", NULL, 0);
	zephir_check_call_status();
	zephir_fast_array_merge(return_value, &_0, &_1);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Command_Factory, getInterfaceName)
{
	zval *this_ptr = getThis();



	RETURN_STRING("TwistersFury\\ChatBot\\Command\\CommandInterface");
}

PHP_METHOD(TwistersFury_ChatBot_Command_Factory, getConfiguredCommands)
{
	zval commands, _0, _1, _2, _3, _4$$3, _5$$3, _6$$3, _7$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&commands);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_7$$3);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&commands);
	array_init(&commands);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "config");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "chatCommands");
	ZEPHIR_CALL_METHOD(&_3, &_1, "has", NULL, 0, &_2);
	zephir_check_call_status();
	if (zephir_is_true(&_3)) {
		ZEPHIR_CALL_METHOD(&_4$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_6$$3);
		ZVAL_STRING(&_6$$3, "config");
		ZEPHIR_CALL_METHOD(&_5$$3, &_4$$3, "get", NULL, 0, &_6$$3);
		zephir_check_call_status();
		zephir_read_property(&_7$$3, &_5$$3, ZEND_STRL("chatCommands"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&commands, &_7$$3, "toarray", NULL, 0);
		zephir_check_call_status();
	}
	RETURN_CCTOR(&commands);
}

