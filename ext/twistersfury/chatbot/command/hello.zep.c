
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/fcall.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Hello)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Command, Hello, twistersfury_chatbot, command_hello, twistersfury_chatbot_command_abstractcommand_ce, twistersfury_chatbot_command_hello_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Command_Hello, processCommand)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Hello Command");
	ZEPHIR_CALL_METHOD(NULL, &_0, "debug", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "World!");
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "sendmessage", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_THIS();
}

