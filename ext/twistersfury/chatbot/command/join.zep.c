
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Join)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Command, Join, twistersfury_chatbot, command_join, twistersfury_chatbot_command_abstractcommand_ce, twistersfury_chatbot_command_join_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Command_Join, processCommand)
{
	zval _1;
	zval _0, _2, _3, _4, _5, _6, _7, _8, _9;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 1, 0);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_3);
	zephir_read_property(&_3, &_2, ZEND_STRL("channel"), PH_NOISY_CC);
	zephir_array_update_string(&_1, SL("channel"), &_3, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(&_4);
	ZVAL_STRING(&_4, "Join Command");
	ZEPHIR_CALL_METHOD(NULL, &_0, "debug", NULL, 0, &_4, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getpacketbuilder", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_7, this_ptr, "getpacket", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_8, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_9, &_8, ZEND_STRL("channel"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_6, &_5, "buildjoinpacket", NULL, 0, &_7, &_9);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "handlemessage", NULL, 0, &_6);
	zephir_check_call_status();
	RETURN_THIS();
}

