
extern zend_class_entry *twistersfury_chatbot_command_join_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Join);

PHP_METHOD(TwistersFury_ChatBot_Command_Join, processCommand);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_command_join_processcommand, 0, 0, TwistersFury\\ChatBot\\Command\\AbstractCommand, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_command_join_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Command_Join, processCommand, arginfo_twistersfury_chatbot_command_join_processcommand, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
