
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/string.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Parser)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Command, Parser, twistersfury_chatbot, command_parser, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_command_parser_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Command_Parser, parse)
{
	zval _8;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, message, command, _0, _1, _2, _5, _6, _7, _3$$3, _4$$3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&command);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_8);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_packet_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&message, packet, "getmessage", NULL, 0);
	zephir_check_call_status();
	ZVAL_LONG(&_0, 0);
	ZVAL_LONG(&_1, 1);
	ZEPHIR_INIT_VAR(&_2);
	zephir_substr(&_2, &message, 0 , 1 , 0);
	if (EXPECTED(ZEPHIR_IS_STRING_IDENTICAL(&_2, "!"))) {
		ZVAL_LONG(&_3$$3, 1);
		ZEPHIR_INIT_VAR(&_4$$3);
		zephir_substr(&_4$$3, &message, 1 , 0, ZEPHIR_SUBSTR_NO_LENGTH);
		ZEPHIR_CPY_WRT(&message, &_4$$3);
	}
	ZEPHIR_CALL_METHOD(&command, this_ptr, "parsesimple", NULL, 15, &message);
	zephir_check_call_status();
	zephir_array_update_string(&command, SL("packet"), packet, PH_COPY | PH_SEPARATE);
	ZEPHIR_OBS_VAR(&_5);
	zephir_read_property(&_5, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_6);
	ZVAL_STRING(&_6, "Parsed Message");
	ZEPHIR_CALL_METHOD(NULL, &_5, "debug", NULL, 0, &_6);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_7, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_8);
	zephir_create_array(&_8, 1, 0);
	zephir_array_fast_append(&_8, &command);
	ZEPHIR_INIT_NVAR(&_6);
	ZVAL_STRING(&_6, "Phalcon\\Config\\Config");
	ZEPHIR_RETURN_CALL_METHOD(&_7, "get", NULL, 0, &_6, &_8);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Command_Parser, parseSimple)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *message_param = NULL, splitMessage, _0, _1, _2, _3, _4, _5;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&splitMessage);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	if (UNEXPECTED(Z_TYPE_P(message_param) != IS_STRING && Z_TYPE_P(message_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'message' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(message_param) == IS_STRING)) {
		zephir_get_strval(&message, message_param);
	} else {
		ZEPHIR_INIT_VAR(&message);
	}


	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, " ");
	ZEPHIR_INIT_VAR(&splitMessage);
	zephir_fast_explode(&splitMessage, &_0, &message, 2 );
	zephir_create_array(return_value, 2, 0);
	ZEPHIR_INIT_VAR(&_1);
	zephir_array_fetch_long(&_2, &splitMessage, 0, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Command/Parser.zep", 40);
	zephir_fast_strtoupper(&_1, &_2);
	zephir_array_update_string(return_value, SL("adapter"), &_1, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_1);
	ZEPHIR_INIT_VAR(&_3);
	zephir_array_fetch_long(&_4, &splitMessage, 1, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Command/Parser.zep", 41);
	if (zephir_is_true(&_4)) {
		ZEPHIR_OBS_NVAR(&_3);
		zephir_array_fetch_long(&_3, &splitMessage, 1, PH_NOISY, "twistersfury/chatbot/Command/Parser.zep", 41);
	} else {
		ZEPHIR_INIT_NVAR(&_3);
		ZVAL_STRING(&_3, "");
	}
	ZEPHIR_INIT_VAR(&_5);
	ZVAL_STRING(&_5, " ");
	zephir_fast_explode(&_1, &_5, &_3, LONG_MAX);
	zephir_array_update_string(return_value, SL("params"), &_1, PH_COPY | PH_SEPARATE);
	RETURN_MM();
}

