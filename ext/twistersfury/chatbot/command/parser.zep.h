
extern zend_class_entry *twistersfury_chatbot_command_parser_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Command_Parser);

PHP_METHOD(TwistersFury_ChatBot_Command_Parser, parse);
PHP_METHOD(TwistersFury_ChatBot_Command_Parser, parseSimple);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_command_parser_parse, 0, 1, Phalcon\\Config\\Config, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_command_parser_parsesimple, 0, 1, IS_ARRAY, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_command_parser_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Command_Parser, parse, arginfo_twistersfury_chatbot_command_parser_parse, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Command_Parser, parseSimple, arginfo_twistersfury_chatbot_command_parser_parsesimple, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
