
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Config_Channel)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Config, Channel, twistersfury_chatbot, connection_config_channel, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\injectable")), twistersfury_chatbot_connection_config_channel_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_connection_config_channel_ce, SL("channelName"), ZEND_ACC_PRIVATE);
	/**
	 * @var ConfigInterface
	 */
	zend_declare_property_null(twistersfury_chatbot_connection_config_channel_ce, SL("config"), ZEND_ACC_PRIVATE);
	/**
	 * @var ManagerInterface
	 */
	zend_declare_property_null(twistersfury_chatbot_connection_config_channel_ce, SL("eventsManager"), ZEND_ACC_PRIVATE);
	zend_class_implements(twistersfury_chatbot_connection_config_channel_ce, 1, zephir_get_internal_ce(SL("phalcon\\events\\eventsawareinterface")));
	zend_class_implements(twistersfury_chatbot_connection_config_channel_ce, 1, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\interfaces\\initializationaware")));
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, getChannelName)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "channelName");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, __construct)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *channelName_param = NULL, *baseConfig, baseConfig_sub;
	zval channelName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channelName);
	ZVAL_UNDEF(&baseConfig_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(channelName)
		Z_PARAM_OBJECT_OF_CLASS(baseConfig, zephir_get_internal_ce(SL("phalcon\\config\\configinterface")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &channelName_param, &baseConfig);
	if (UNEXPECTED(Z_TYPE_P(channelName_param) != IS_STRING && Z_TYPE_P(channelName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'channelName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(channelName_param) == IS_STRING)) {
		zephir_get_strval(&channelName, channelName_param);
	} else {
		ZEPHIR_INIT_VAR(&channelName);
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("channelName"), &channelName);
	zephir_update_property_zval(this_ptr, ZEND_STRL("config"), baseConfig);
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, getEventsManager)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "eventsManager");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, setEventsManager)
{
	zval *eventsManager, eventsManager_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&eventsManager_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(eventsManager, zephir_get_internal_ce(SL("phalcon\\events\\managerinterface")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &eventsManager);


	zephir_update_property_zval(this_ptr, ZEND_STRL("eventsManager"), eventsManager);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, initialize)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "Phalcon\\Events\\Manager");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "seteventsmanager", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "loadlisteners", NULL, 16);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, loadListeners)
{
	zval _0, _1, _2, listener$$3, instance$$3, _3$$3, _4$$3, _5$$3, *_6$$3, _7$$3, _8$$4, _10$$4, _11$$4, _12$$4, _13$$4, _14$$4, _15$$4, _16$$5, _17$$5, _18$$5, _19$$5, _20$$5, _21$$5, _22$$5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_9 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&listener$$3);
	ZVAL_UNDEF(&instance$$3);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_10$$4);
	ZVAL_UNDEF(&_11$$4);
	ZVAL_UNDEF(&_12$$4);
	ZVAL_UNDEF(&_13$$4);
	ZVAL_UNDEF(&_14$$4);
	ZVAL_UNDEF(&_15$$4);
	ZVAL_UNDEF(&_16$$5);
	ZVAL_UNDEF(&_17$$5);
	ZVAL_UNDEF(&_18$$5);
	ZVAL_UNDEF(&_19$$5);
	ZVAL_UNDEF(&_20$$5);
	ZVAL_UNDEF(&_21$$5);
	ZVAL_UNDEF(&_22$$5);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "listeners");
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &_2);
	zephir_check_call_status();
	if (zephir_is_true(&_1)) {
		ZEPHIR_CALL_METHOD(&_3$$3, this_ptr, "getconfig", NULL, 0);
		zephir_check_call_status();
		zephir_read_property(&_4$$3, &_3$$3, ZEND_STRL("listeners"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&_5$$3, &_4$$3, "getiterator", NULL, 0);
		zephir_check_call_status();
		zephir_is_iterable(&_5$$3, 0, "twistersfury/chatbot/Connection/Config/Channel.zep", 58);
		if (Z_TYPE_P(&_5$$3) == IS_ARRAY) {
			ZEND_HASH_FOREACH_VAL(Z_ARRVAL_P(&_5$$3), _6$$3)
			{
				ZEPHIR_INIT_NVAR(&listener$$3);
				ZVAL_COPY(&listener$$3, _6$$3);
				ZEPHIR_CALL_METHOD(&_8$$4, this_ptr, "getdi", &_9, 0);
				zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&_11$$4);
				ZVAL_STRING(&_11$$4, "eventsManager");
				ZEPHIR_CALL_METHOD(&_10$$4, &_8$$4, "get", NULL, 0, &_11$$4);
				zephir_check_call_status();
				zephir_read_property(&_12$$4, &listener$$3, ZEND_STRL("event"), PH_NOISY_CC | PH_READONLY);
				ZEPHIR_CALL_METHOD(&_13$$4, this_ptr, "getdi", &_9, 0);
				zephir_check_call_status();
				zephir_read_property(&_15$$4, &listener$$3, ZEND_STRL("listener"), PH_NOISY_CC | PH_READONLY);
				ZEPHIR_CALL_METHOD(&_14$$4, &_13$$4, "get", NULL, 0, &_15$$4);
				zephir_check_call_status();
				ZEPHIR_CALL_METHOD(&instance$$3, &_10$$4, "attach", NULL, 0, &_12$$4, &_14$$4);
				zephir_check_call_status();
			} ZEND_HASH_FOREACH_END();
		} else {
			ZEPHIR_CALL_METHOD(NULL, &_5$$3, "rewind", NULL, 0);
			zephir_check_call_status();
			while (1) {
				ZEPHIR_CALL_METHOD(&_7$$3, &_5$$3, "valid", NULL, 0);
				zephir_check_call_status();
				if (!zend_is_true(&_7$$3)) {
					break;
				}
				ZEPHIR_CALL_METHOD(&listener$$3, &_5$$3, "current", NULL, 0);
				zephir_check_call_status();
					ZEPHIR_CALL_METHOD(&_16$$5, this_ptr, "getdi", &_9, 0);
					zephir_check_call_status();
					ZEPHIR_INIT_NVAR(&_18$$5);
					ZVAL_STRING(&_18$$5, "eventsManager");
					ZEPHIR_CALL_METHOD(&_17$$5, &_16$$5, "get", NULL, 0, &_18$$5);
					zephir_check_call_status();
					zephir_read_property(&_19$$5, &listener$$3, ZEND_STRL("event"), PH_NOISY_CC | PH_READONLY);
					ZEPHIR_CALL_METHOD(&_20$$5, this_ptr, "getdi", &_9, 0);
					zephir_check_call_status();
					zephir_read_property(&_22$$5, &listener$$3, ZEND_STRL("listener"), PH_NOISY_CC | PH_READONLY);
					ZEPHIR_CALL_METHOD(&_21$$5, &_20$$5, "get", NULL, 0, &_22$$5);
					zephir_check_call_status();
					ZEPHIR_CALL_METHOD(&instance$$3, &_17$$5, "attach", NULL, 0, &_19$$5, &_21$$5);
					zephir_check_call_status();
				ZEPHIR_CALL_METHOD(NULL, &_5$$3, "next", NULL, 0);
				zephir_check_call_status();
			}
		}
		ZEPHIR_INIT_NVAR(&listener$$3);
	}
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Channel, getConfig)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "config");
}

