
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Config_Manager)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Config, Manager, twistersfury_chatbot, connection_config_manager, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\injectable")), twistersfury_chatbot_connection_config_manager_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_connection_config_manager_ce, SL("channelConfigs"), ZEND_ACC_PRIVATE);
	twistersfury_chatbot_connection_config_manager_ce->create_object = zephir_init_properties_TwistersFury_ChatBot_Connection_Config_Manager;

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Manager, get)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *channelName_param = NULL, _0, _1, _2$$3;
	zval channelName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channelName);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2$$3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(channelName)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &channelName_param);
	if (UNEXPECTED(Z_TYPE_P(channelName_param) != IS_STRING && Z_TYPE_P(channelName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'channelName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(channelName_param) == IS_STRING)) {
		zephir_get_strval(&channelName, channelName_param);
	} else {
		ZEPHIR_INIT_VAR(&channelName);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "normalize", NULL, 17, &channelName);
	zephir_check_call_status();
	zephir_get_strval(&channelName, &_0);
	zephir_read_property(&_1, this_ptr, ZEND_STRL("channelConfigs"), PH_NOISY_CC | PH_READONLY);
	if (!(zephir_array_isset(&_1, &channelName))) {
		ZEPHIR_CALL_METHOD(&_2$$3, this_ptr, "loadconfig", NULL, 18, &channelName);
		zephir_check_call_status();
		zephir_update_property_array(this_ptr, SL("channelConfigs"), &channelName, &_2$$3);
	}
	RETURN_MM_MEMBER(getThis(), "channelConfigs");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Manager, loadConfig)
{
	zval _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *channelName_param = NULL, baseConfig, _0, _2;
	zval channelName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channelName);
	ZVAL_UNDEF(&baseConfig);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(channelName)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &channelName_param);
	if (UNEXPECTED(Z_TYPE_P(channelName_param) != IS_STRING && Z_TYPE_P(channelName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'channelName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(channelName_param) == IS_STRING)) {
		zephir_get_strval(&channelName, channelName_param);
	} else {
		ZEPHIR_INIT_VAR(&channelName);
	}


	ZEPHIR_CALL_METHOD(&baseConfig, this_ptr, "loadbaseconfig", NULL, 19, &channelName);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 2, 0);
	zephir_array_fast_append(&_1, &channelName);
	zephir_array_fast_append(&_1, &baseConfig);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "Phalcon\\Config\\Config");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_2, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Manager, loadBaseConfig)
{
	zend_bool _3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *channelName_param = NULL, _0, _1, _2, _4, _5, _6, _9, _10, _7$$3, _8$$3;
	zval channelName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channelName);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&_8$$3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(channelName)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &channelName_param);
	if (UNEXPECTED(Z_TYPE_P(channelName_param) != IS_STRING && Z_TYPE_P(channelName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'channelName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(channelName_param) == IS_STRING)) {
		zephir_get_strval(&channelName, channelName_param);
	} else {
		ZEPHIR_INIT_VAR(&channelName);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "channels");
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &_2);
	zephir_check_call_status();
	_3 = !zephir_is_true(&_1);
	if (!(_3)) {
		ZEPHIR_CALL_METHOD(&_4, this_ptr, "getconfig", NULL, 0);
		zephir_check_call_status();
		zephir_read_property(&_5, &_4, ZEND_STRL("channels"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&_6, &_5, "has", NULL, 0, &channelName);
		zephir_check_call_status();
		_3 = !zephir_is_true(&_6);
	}
	if (UNEXPECTED(_3)) {
		ZEPHIR_CALL_METHOD(&_7$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_8$$3);
		ZVAL_STRING(&_8$$3, "Phalcon\\Config\\Config");
		ZEPHIR_RETURN_CALL_METHOD(&_7$$3, "get", NULL, 0, &_8$$3);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_CALL_METHOD(&_9, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_10, &_9, ZEND_STRL("channels"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_RETURN_CALL_METHOD(&_10, "get", NULL, 0, &channelName);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Config_Manager, normalize)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *channelName_param = NULL, _0, _1, _2;
	zval channelName;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channelName);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(channelName)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &channelName_param);
	if (UNEXPECTED(Z_TYPE_P(channelName_param) != IS_STRING && Z_TYPE_P(channelName_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'channelName' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(channelName_param) == IS_STRING)) {
		zephir_get_strval(&channelName, channelName_param);
	} else {
		ZEPHIR_INIT_VAR(&channelName);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "helper");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_1, "friendly", NULL, 0, &channelName);
	zephir_check_call_status();
	RETURN_MM();
}

zend_object *zephir_init_properties_TwistersFury_ChatBot_Connection_Config_Manager(zend_class_entry *class_type)
{
		zval _0, _1$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
		ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1$$3);
	

		ZEPHIR_MM_GROW();
	
	{
		zval local_this_ptr, *this_ptr = &local_this_ptr;
		ZEPHIR_CREATE_OBJECT(this_ptr, class_type);
		zephir_read_property_ex(&_0, this_ptr, ZEND_STRL("channelConfigs"), PH_NOISY_CC | PH_READONLY);
		if (Z_TYPE_P(&_0) == IS_NULL) {
			ZEPHIR_INIT_VAR(&_1$$3);
			array_init(&_1$$3);
			zephir_update_property_zval_ex(this_ptr, ZEND_STRL("channelConfigs"), &_1$$3);
		}
		ZEPHIR_MM_RESTORE();
		return Z_OBJ_P(this_ptr);
	}
}

