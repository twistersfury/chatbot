
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Driver_DriverInterface)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Driver, DriverInterface, twistersfury_chatbot, connection_driver_driverinterface, twistersfury_chatbot_connection_driver_driverinterface_method_entry);

	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, openConnection);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, closeConnection);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, sendPacket);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, readPacket);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, keepAlive);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, isConnected);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, getPacketFactory);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, getConnectionName);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Driver_DriverInterface, initialize);
