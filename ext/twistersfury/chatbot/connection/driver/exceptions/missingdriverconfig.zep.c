
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "ext/spl/spl_exceptions.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Driver_Exceptions_MissingDriverConfig)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Driver\\Exceptions, MissingDriverConfig, twistersfury_chatbot, connection_driver_exceptions_missingdriverconfig, spl_ce_RuntimeException, NULL, 0);

	return SUCCESS;
}

