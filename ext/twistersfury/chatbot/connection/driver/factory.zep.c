
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Driver_Factory)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Driver, Factory, twistersfury_chatbot, connection_driver_factory, twistersfury_chatbot_factory_abstractfactory_ce, twistersfury_chatbot_connection_driver_factory_method_entry, 0);

	return SUCCESS;
}

/**
 * Returns the adapters for the factory
 */
PHP_METHOD(TwistersFury_ChatBot_Connection_Driver_Factory, getServices)
{
	zval *this_ptr = getThis();



	zephir_create_array(return_value, 2, 0);
	add_assoc_stringl_ex(return_value, SL("irc"), SL("TwistersFury\\Chatbot\\Irc\\Driver\\Classic"));
	add_assoc_stringl_ex(return_value, SL("twitch"), SL("TwistersFury\\ChatBot\\Twitch\\Driver\\Irc"));
	return;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Driver_Factory, getInterfaceName)
{
	zval *this_ptr = getThis();



	RETURN_STRING("TwistersFury\\ChatBot\\Connection\\Driver\\DriverInterface");
}

