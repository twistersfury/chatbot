
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Helper_Throttle)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Helper, Throttle, twistersfury_chatbot, connection_helper_throttle, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\abstractconfiginjectable")), NULL, 0);

	return SUCCESS;
}

