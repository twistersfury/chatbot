
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/exception.h"
#include "kernel/concat.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Manager)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection, Manager, twistersfury_chatbot, connection_manager, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_connection_manager_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_connection_manager_ce, SL("config"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_connection_manager_ce, SL("driver"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_connection_manager_ce, SL("eventsManager"), ZEND_ACC_PRIVATE);
	zend_class_implements(twistersfury_chatbot_connection_manager_ce, 1, zephir_get_internal_ce(SL("phalcon\\events\\eventsawareinterface")));
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getEventsManager)
{
	zval _0, _1$$3, _2$$3, _3$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_3$$3);


	ZEPHIR_MM_GROW();

	zephir_read_property(&_0, this_ptr, ZEND_STRL("eventsManager"), PH_NOISY_CC | PH_READONLY);
	if (UNEXPECTED(Z_TYPE_P(&_0) == IS_NULL)) {
		ZEPHIR_CALL_METHOD(&_1$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_3$$3);
		ZVAL_STRING(&_3$$3, "eventsManager");
		ZEPHIR_CALL_METHOD(&_2$$3, &_1$$3, "get", NULL, 0, &_3$$3);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "seteventsmanager", NULL, 0, &_2$$3);
		zephir_check_call_status();
	}
	RETURN_MM_MEMBER(getThis(), "eventsManager");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, setEventsManager)
{
	zval *eventsManager, eventsManager_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&eventsManager_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(eventsManager, zephir_get_internal_ce(SL("phalcon\\events\\managerinterface")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &eventsManager);


	zephir_update_property_zval(this_ptr, ZEND_STRL("eventsManager"), eventsManager);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, __construct)
{
	zval *config, config_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&config_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(config, zephir_get_internal_ce(SL("phalcon\\config\\config")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &config);


	zephir_update_property_zval(this_ptr, ZEND_STRL("config"), config);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getDriver)
{
	zval _0, _1$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1$$3);


	ZEPHIR_MM_GROW();

	zephir_read_property(&_0, this_ptr, ZEND_STRL("driver"), PH_NOISY_CC | PH_READONLY);
	if (Z_TYPE_P(&_0) == IS_NULL) {
		ZEPHIR_CALL_METHOD(&_1$$3, this_ptr, "loaddriver", NULL, 20);
		zephir_check_call_status();
		zephir_update_property_zval(this_ptr, ZEND_STRL("driver"), &_1$$3);
	}
	RETURN_MM_MEMBER(getThis(), "driver");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getConfig)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "config");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, loadDriver)
{
	zval _9;
	zval driver, _0, _1, _2, _3, _6, _7, _8, _10, _11, _12, _13, _14, _15, _4$$3, _5$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&driver);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_13);
	ZVAL_UNDEF(&_14);
	ZVAL_UNDEF(&_15);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_9);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("driver"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CPY_WRT(&driver, &_1);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, &_2, "has", NULL, 0, &driver);
	zephir_check_call_status();
	if (UNEXPECTED(!zephir_is_true(&_3))) {
		ZEPHIR_INIT_VAR(&_4$$3);
		object_init_ex(&_4$$3, twistersfury_chatbot_connection_driver_exceptions_missingdriverconfig_ce);
		ZEPHIR_INIT_VAR(&_5$$3);
		ZEPHIR_CONCAT_SV(&_5$$3, "Configuration Does Not Exist: ", &driver);
		ZEPHIR_CALL_METHOD(NULL, &_4$$3, "__construct", NULL, 21, &_5$$3);
		zephir_check_call_status();
		zephir_throw_exception_debug(&_4$$3, "twistersfury/chatbot/Connection/Manager.zep", 58);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_8);
	ZVAL_STRING(&_8, "logger");
	ZEPHIR_CALL_METHOD(&_7, &_6, "get", NULL, 0, &_8);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_9);
	zephir_create_array(&_9, 2, 0);
	zephir_array_update_string(&_9, SL("driver"), &driver, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_10, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_11, &_10, "get", NULL, 0, &driver);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_12, &_11, "toarray", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_9, SL("config"), &_12, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_8);
	ZVAL_STRING(&_8, "Initializing Driver");
	ZEPHIR_CALL_METHOD(NULL, &_7, "debug", NULL, 0, &_8, &_9);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_12, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_8);
	ZVAL_STRING(&_8, "driverFactory");
	ZEPHIR_CALL_METHOD(&_13, &_12, "get", NULL, 0, &_8);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_14, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_15, &_14, "get", NULL, 0, &driver);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_13, "load", NULL, 0, &_15);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, sendPacket)
{
	zval _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, _0, _2, _3, _4, _5;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_packet_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "geteventsmanager", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 1, 0);
	zephir_array_update_string(&_1, SL("packet"), packet, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "driver:sendPacket");
	ZEPHIR_CALL_METHOD(NULL, &_0, "fire", NULL, 0, &_2, this_ptr, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_4, &_3, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Sending Packet");
	ZEPHIR_CALL_METHOD(NULL, &_4, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_5, "sendpacket", NULL, 0, packet);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, readPacket)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_0, "readpacket", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, openConnection)
{
	zval _0, _1, _2, _3, _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "geteventsmanager", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "driver:openConnection");
	ZEPHIR_CALL_METHOD(NULL, &_0, "fire", NULL, 0, &_1, this_ptr);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "logger");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "Open Connection");
	ZEPHIR_CALL_METHOD(NULL, &_3, "debug", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_4, "openconnection", NULL, 0);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, closeConnection)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_0, "closeconnection", NULL, 0);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, isConnected)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_0, "isconnected", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, keepAlive)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_packet_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_0, "keepalive", NULL, 0, packet);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, buildAndSend)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message, message_sub, _0, _1, _2, sendPacket;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&sendPacket);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(message, twistersfury_chatbot_connection_packet_interfaces_packet_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Build and Send");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	if (zephir_instance_of_ev(message, twistersfury_chatbot_connection_packet_interfaces_outgoingpacket_ce)) {
		ZEPHIR_CPY_WRT(&sendPacket, message);
	} else {
		ZEPHIR_CALL_METHOD(&sendPacket, this_ptr, "buildpacket", NULL, 22, message);
		zephir_check_call_status();
	}
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "sendpacket", NULL, 0, &sendPacket);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, buildPacket)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message, message_sub, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message_sub);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(message, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_0, "buildpacket", NULL, 0, message);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getPacketFactory)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdriver", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_0, "getpacketfactory", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

