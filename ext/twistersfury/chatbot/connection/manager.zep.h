
extern zend_class_entry *twistersfury_chatbot_connection_manager_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Manager);

PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getEventsManager);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, setEventsManager);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, __construct);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getDriver);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getConfig);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, loadDriver);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, sendPacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, readPacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, openConnection);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, closeConnection);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, isConnected);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, keepAlive);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, buildAndSend);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, buildPacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Manager, getPacketFactory);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_geteventsmanager, 0, 0, Phalcon\\Events\\ManagerInterface, 1)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_seteventsmanager, 0, 1, IS_VOID, 0)

	ZEND_ARG_OBJ_INFO(0, eventsManager, Phalcon\\Events\\ManagerInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_connection_manager___construct, 0, 0, 1)
	ZEND_ARG_OBJ_INFO(0, config, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_getdriver, 0, 0, TwistersFury\\ChatBot\\Connection\\Driver\\DriverInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_getconfig, 0, 0, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_loaddriver, 0, 0, TwistersFury\\ChatBot\\Connection\\Driver\\DriverInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_sendpacket, 0, 1, TwistersFury\\ChatBot\\Connection\\Manager, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_readpacket, 0, 0, TwistersFury\\ChatBot\\Connection\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_openconnection, 0, 0, TwistersFury\\ChatBot\\Connection\\Manager, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_closeconnection, 0, 0, TwistersFury\\ChatBot\\Connection\\Manager, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_isconnected, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_keepalive, 0, 1, TwistersFury\\ChatBot\\Connection\\Manager, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_buildandsend, 0, 1, TwistersFury\\ChatBot\\Connection\\Manager, 0)
	ZEND_ARG_OBJ_INFO(0, message, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_buildpacket, 0, 1, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\OutgoingPacket, 0)
	ZEND_ARG_OBJ_INFO(0, message, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_manager_getpacketfactory, 0, 0, TwistersFury\\ChatBot\\Connection\\Packet\\AbstractFactory, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_connection_manager_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, getEventsManager, arginfo_twistersfury_chatbot_connection_manager_geteventsmanager, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, setEventsManager, arginfo_twistersfury_chatbot_connection_manager_seteventsmanager, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, __construct, arginfo_twistersfury_chatbot_connection_manager___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, getDriver, arginfo_twistersfury_chatbot_connection_manager_getdriver, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, getConfig, arginfo_twistersfury_chatbot_connection_manager_getconfig, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, loadDriver, arginfo_twistersfury_chatbot_connection_manager_loaddriver, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, sendPacket, arginfo_twistersfury_chatbot_connection_manager_sendpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, readPacket, arginfo_twistersfury_chatbot_connection_manager_readpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, openConnection, arginfo_twistersfury_chatbot_connection_manager_openconnection, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, closeConnection, arginfo_twistersfury_chatbot_connection_manager_closeconnection, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, isConnected, arginfo_twistersfury_chatbot_connection_manager_isconnected, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, keepAlive, arginfo_twistersfury_chatbot_connection_manager_keepalive, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, buildAndSend, arginfo_twistersfury_chatbot_connection_manager_buildandsend, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, buildPacket, arginfo_twistersfury_chatbot_connection_manager_buildpacket, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Connection_Manager, getPacketFactory, arginfo_twistersfury_chatbot_connection_manager_getpacketfactory, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
