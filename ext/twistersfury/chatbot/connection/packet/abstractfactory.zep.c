
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_AbstractFactory)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Packet, AbstractFactory, twistersfury_chatbot, connection_packet_abstractfactory, twistersfury_chatbot_factory_abstractfactory_ce, twistersfury_chatbot_connection_packet_abstractfactory_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zephir_declare_class_constant_string(twistersfury_chatbot_connection_packet_abstractfactory_ce, SL("OUTGOING_CHANNEL"), "_CHANNEL");

	zephir_declare_class_constant_string(twistersfury_chatbot_connection_packet_abstractfactory_ce, SL("OUTGOING_DIRECT"), "_DIRECT");

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_AbstractFactory, buildResponsePacket)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval type;
	zval *packet, packet_sub, *type_param = NULL, _0, _2, _1$$3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&type);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
		Z_PARAM_OPTIONAL
		Z_PARAM_STR_OR_NULL(type)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &packet, &type_param);
	if (!type_param) {
		ZEPHIR_INIT_VAR(&type);
	} else {
	if (UNEXPECTED(Z_TYPE_P(type_param) != IS_STRING && Z_TYPE_P(type_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'type' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(type_param) == IS_STRING)) {
		zephir_get_strval(&type, type_param);
	} else {
		ZEPHIR_INIT_VAR(&type);
	}
	}


	ZEPHIR_CALL_METHOD(&_0, packet, "ischannel", NULL, 0);
	zephir_check_call_status();
	if (zephir_is_true(&_0)) {
		ZEPHIR_INIT_VAR(&_1$$3);
		ZVAL_STRING(&_1$$3, "_CHANNEL");
		ZEPHIR_RETURN_CALL_METHOD(this_ptr, "quickload", NULL, 0, &_1$$3);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "_DIRECT");
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "quickload", NULL, 0, &_2);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_AbstractFactory, buildJoinPacket)
{
	zval _1, _4, _5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval channel;
	zval *packet, packet_sub, *channel_param = NULL, _0, _2, _3, _6;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&channel);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
		Z_PARAM_STR(channel)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &packet, &channel_param);
	zephir_get_strval(&channel, channel_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 1, 0);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 1, 0);
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 1, 0);
	zephir_array_update_string(&_5, SL("channel"), &channel, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_4, &_5);
	ZEPHIR_INIT_VAR(&_6);
	ZVAL_STRING(&_6, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_6, &_4);
	zephir_check_call_status();
	zephir_array_fast_append(&_1, &_3);
	ZEPHIR_INIT_NVAR(&_6);
	ZVAL_STRING(&_6, "TwistersFury\\ChatBot\\Connection\\Packet\\Join");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_6, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

