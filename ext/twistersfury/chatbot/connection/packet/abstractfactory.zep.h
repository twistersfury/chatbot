
extern zend_class_entry *twistersfury_chatbot_connection_packet_abstractfactory_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_AbstractFactory);

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_AbstractFactory, buildResponsePacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_AbstractFactory, buildJoinPacket);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_abstractfactory_buildresponsepacket, 0, 0, 1)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
	ZEND_ARG_TYPE_INFO(0, type, IS_STRING, 1)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_abstractfactory_buildjoinpacket, 0, 0, 2)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
	ZEND_ARG_TYPE_INFO(0, channel, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_connection_packet_abstractfactory_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_AbstractFactory, buildResponsePacket, arginfo_twistersfury_chatbot_connection_packet_abstractfactory_buildresponsepacket, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_AbstractFactory, buildJoinPacket, arginfo_twistersfury_chatbot_connection_packet_abstractfactory_buildjoinpacket, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
