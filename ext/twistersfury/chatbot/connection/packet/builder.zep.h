
extern zend_class_entry *twistersfury_chatbot_connection_packet_builder_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Builder);

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, handleMessage);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, sendMessage);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, buildJob);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, buildResponsePacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, buildPacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, buildJoinPacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Builder, getManager);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_handlemessage, 0, 1, TwistersFury\\ChatBot\\Connection\\Packet\\AbstractCommand, 0)
	ZEND_ARG_OBJ_INFO(0, message, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\OutgoingPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_sendmessage, 0, 2, TwistersFury\\ChatBot\\Connection\\Packet\\Builder, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_buildjob, 0, 1, TwistersFury\\ChatBot\\Connection\\Job\\SendPacket, 0)
	ZEND_ARG_ARRAY_INFO(0, options, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_buildresponsepacket, 0, 1, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\OutgoingPacket, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_buildpacket, 0, 2, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\OutgoingPacket, 0)
	ZEND_ARG_TYPE_INFO(0, type, IS_STRING, 0)
	ZEND_ARG_OBJ_INFO(0, config, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_buildjoinpacket, 0, 2, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Join, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
	ZEND_ARG_TYPE_INFO(0, channel, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_packet_builder_getmanager, 0, 0, TwistersFury\\ChatBot\\Connection\\Manager, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_connection_packet_builder_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, handleMessage, arginfo_twistersfury_chatbot_connection_packet_builder_handlemessage, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, sendMessage, arginfo_twistersfury_chatbot_connection_packet_builder_sendmessage, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, buildJob, arginfo_twistersfury_chatbot_connection_packet_builder_buildjob, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, buildResponsePacket, arginfo_twistersfury_chatbot_connection_packet_builder_buildresponsepacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, buildPacket, arginfo_twistersfury_chatbot_connection_packet_builder_buildpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, buildJoinPacket, arginfo_twistersfury_chatbot_connection_packet_builder_buildjoinpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Packet_Builder, getManager, arginfo_twistersfury_chatbot_connection_packet_builder_getmanager, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
