
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Interfaces_HasCommands)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces, HasCommands, twistersfury_chatbot, connection_packet_interfaces_hascommands, twistersfury_chatbot_connection_packet_interfaces_hascommands_method_entry);

	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_HasCommands, buildCommands);
