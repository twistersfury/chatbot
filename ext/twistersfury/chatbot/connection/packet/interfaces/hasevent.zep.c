
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Interfaces_HasEvent)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces, HasEvent, twistersfury_chatbot, connection_packet_interfaces_hasevent, twistersfury_chatbot_connection_packet_interfaces_hasevent_method_entry);

	zend_class_implements(twistersfury_chatbot_connection_packet_interfaces_hasevent_ce, 1, zephir_get_internal_ce(SL("phalcon\\events\\eventsawareinterface")));
	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_HasEvent, getEventName);
