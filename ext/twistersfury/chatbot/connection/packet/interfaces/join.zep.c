
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Interfaces_Join)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces, Join, twistersfury_chatbot, connection_packet_interfaces_join, twistersfury_chatbot_connection_packet_interfaces_join_method_entry);

	zend_class_implements(twistersfury_chatbot_connection_packet_interfaces_join_ce, 1, twistersfury_chatbot_connection_packet_interfaces_outgoingpacket_ce);
	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_Join, getChannel);
