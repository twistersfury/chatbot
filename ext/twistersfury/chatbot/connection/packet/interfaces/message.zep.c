
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Interfaces_Message)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces, Message, twistersfury_chatbot, connection_packet_interfaces_message, twistersfury_chatbot_connection_packet_interfaces_message_method_entry);

	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_Message, getMessage);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_Message, getQueueOptions);
