
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Interfaces_User)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces, User, twistersfury_chatbot, connection_packet_interfaces_user, twistersfury_chatbot_connection_packet_interfaces_user_method_entry);

	zend_class_implements(twistersfury_chatbot_connection_packet_interfaces_user_ce, 1, twistersfury_chatbot_connection_packet_interfaces_packet_ce);
	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_User, getDisplayName);
ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Packet_Interfaces_User, getName);
