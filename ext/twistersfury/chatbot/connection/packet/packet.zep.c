
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/operators.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Packet)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Packet, Packet, twistersfury_chatbot, connection_packet_packet, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\abstractconfiginjectable")), twistersfury_chatbot_connection_packet_packet_method_entry, 0);

	zend_declare_property_string(twistersfury_chatbot_connection_packet_packet_ce, SL("connection"), "", ZEND_ACC_PRIVATE);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, getRawMessage)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("raw"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, getMessage)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("message"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, getFrom)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("user"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, buildPacket)
{
	zval *this_ptr = getThis();



	RETURN_STRING("");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, getConnection)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "connection");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, setConnection)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *connection_param = NULL;
	zval connection;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&connection);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(connection)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &connection_param);
	if (UNEXPECTED(Z_TYPE_P(connection_param) != IS_STRING && Z_TYPE_P(connection_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'connection' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(connection_param) == IS_STRING)) {
		zephir_get_strval(&connection, connection_param);
	} else {
		ZEPHIR_INIT_VAR(&connection);
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("connection"), &connection);
	RETURN_THIS();
}

/**
 * Note: Per PHP Docs, this will not be called if the class defines __serialize
 */
PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, serialize)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_FUNCTION("serialize", NULL, 3, &_0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, unserialize)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *data, data_sub, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&data_sub);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ZVAL(data)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &data);


	ZEPHIR_CALL_FUNCTION(&_0, "unserialize", NULL, 4, data);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "setconfig", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Packet, isChannel)
{
	zval *this_ptr = getThis();



	RETURN_BOOL(0);
}

