
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/operators.h"
#include "kernel/memory.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_Unknown)
{
	ZEPHIR_REGISTER_CLASS(TwistersFury\\ChatBot\\Connection\\Packet, Unknown, twistersfury_chatbot, connection_packet_unknown, twistersfury_chatbot_connection_packet_unknown_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_connection_packet_unknown_ce, SL("connection"), ZEND_ACC_PRIVATE);
	zend_class_implements(twistersfury_chatbot_connection_packet_unknown_ce, 1, twistersfury_chatbot_connection_packet_unknowninterface_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_Unknown, setConnection)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *connection_param = NULL;
	zval connection;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&connection);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(connection)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &connection_param);
	if (UNEXPECTED(Z_TYPE_P(connection_param) != IS_STRING && Z_TYPE_P(connection_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'connection' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(connection_param) == IS_STRING)) {
		zephir_get_strval(&connection, connection_param);
	} else {
		ZEPHIR_INIT_VAR(&connection);
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("connection"), &connection);
	ZEPHIR_MM_RESTORE();
}

