
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/operators.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Packet_User)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Packet, User, twistersfury_chatbot, connection_packet_user, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\abstractconfiginjectable")), twistersfury_chatbot_connection_packet_user_method_entry, 0);

	zend_class_implements(twistersfury_chatbot_connection_packet_user_ce, 1, twistersfury_chatbot_connection_packet_interfaces_user_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_User, getDisplayName)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("displayName"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_User, getName)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();



	ZEPHIR_MM_GROW();

	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "getdisplayname", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_User, getMessage)
{
	zval *this_ptr = getThis();



	RETURN_STRING("");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_User, getConnection)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("connection"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Packet_User, setConnection)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *connection_param = NULL, _0, _1;
	zval connection;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&connection);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(connection)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &connection_param);
	if (UNEXPECTED(Z_TYPE_P(connection_param) != IS_STRING && Z_TYPE_P(connection_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'connection' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(connection_param) == IS_STRING)) {
		zephir_get_strval(&connection, connection_param);
	} else {
		ZEPHIR_INIT_VAR(&connection);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "connection");
	ZEPHIR_CALL_METHOD(NULL, &_0, "set", NULL, 0, &_1, &connection);
	zephir_check_call_status();
	RETURN_THIS();
}

