
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Throttle_Exceptions_Throttled)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Throttle\\Exceptions, Throttled, twistersfury_chatbot, connection_throttle_exceptions_throttled, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\exceptions\\nonfatalexception")), NULL, 0);

	zend_declare_property_string(twistersfury_chatbot_connection_throttle_exceptions_throttled_ce, SL("message"), "Throttle Limit Reached", ZEND_ACC_PROTECTED);
	return SUCCESS;
}

