
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Throttle_Interfaces_ThrottledPacket)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Connection\\Throttle\\Interfaces, ThrottledPacket, twistersfury_chatbot, connection_throttle_interfaces_throttledpacket, twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_method_entry);

	zend_class_implements(twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_ce, 1, twistersfury_chatbot_connection_packet_interfaces_outgoingpacket_ce);
	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Connection_Throttle_Interfaces_ThrottledPacket, getTo);
