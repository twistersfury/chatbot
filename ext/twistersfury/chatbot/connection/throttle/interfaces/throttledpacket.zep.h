
extern zend_class_entry *twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Throttle_Interfaces_ThrottledPacket);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_getto, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_method_entry) {
	PHP_ABSTRACT_ME(TwistersFury_ChatBot_Connection_Throttle_Interfaces_ThrottledPacket, getTo, arginfo_twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_getto)
	PHP_FE_END
};
