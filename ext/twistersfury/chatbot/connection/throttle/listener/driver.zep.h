
extern zend_class_entry *twistersfury_chatbot_connection_throttle_listener_driver_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver);

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, __construct);
PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, sendPacket);
PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, getThrottleManager);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_connection_throttle_listener_driver___construct, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_connection_throttle_listener_driver_sendpacket, 0, 2, IS_VOID, 0)

	ZEND_ARG_OBJ_INFO(0, event, TwistersFury\\ChatBot\\Connection\\Throttle\\Listener\\Event, 0)
	ZEND_ARG_OBJ_INFO(0, manger, TwistersFury\\ChatBot\\Connection\\Throttle\\Manager, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, data, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, data, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_connection_throttle_listener_driver_getthrottlemanager, 0, 0, TwistersFury\\ChatBot\\Connection\\Throttle\\Manager, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_connection_throttle_listener_driver_method_entry) {
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, __construct, arginfo_twistersfury_chatbot_connection_throttle_listener_driver___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
#else
	PHP_ME(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, __construct, NULL, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
#endif
	PHP_ME(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, sendPacket, arginfo_twistersfury_chatbot_connection_throttle_listener_driver_sendpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Connection_Throttle_Listener_Driver, getThrottleManager, arginfo_twistersfury_chatbot_connection_throttle_listener_driver_getthrottlemanager, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
