
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Throttle_Manager)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Throttle, Manager, twistersfury_chatbot, connection_throttle_manager, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_connection_throttle_manager_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_connection_throttle_manager_ce, SL("trackers"), ZEND_ACC_PROTECTED);
	twistersfury_chatbot_connection_throttle_manager_ce->create_object = zephir_init_properties_TwistersFury_ChatBot_Connection_Throttle_Manager;

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Manager, getTracker)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *channel_param = NULL, tracker, _0;
	zval channel;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channel);
	ZVAL_UNDEF(&tracker);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(channel)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &channel_param);
	zephir_get_strval(&channel, channel_param);


	ZEPHIR_OBS_VAR(&tracker);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "gettrackers", NULL, 0);
	zephir_check_call_status();
	if (!(zephir_array_isset_fetch(&tracker, &_0, &channel, 0))) {
		ZEPHIR_CALL_METHOD(&tracker, this_ptr, "buildtracker", NULL, 0, &channel);
		zephir_check_call_status();
		zephir_update_property_array(this_ptr, SL("trackers"), &channel, &tracker);
	}
	RETURN_CCTOR(&tracker);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Manager, getTrackers)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "trackers");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Manager, canSend)
{
	zval _5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, canSend, _0, _1, _2, _3, _4, _6;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&canSend);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_5);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_1, packet, "getto", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "gettracker", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&canSend, &_0, "cansend", NULL, 0, packet);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	ZVAL_STRING(&_4, "logger");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 2, 0);
	ZEPHIR_CALL_METHOD(&_6, packet, "getto", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_5, SL("channel"), &_6, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_5, SL("canSend"), &canSend, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_4);
	ZVAL_STRING(&_4, "Throttle Can Send Check");
	ZEPHIR_CALL_METHOD(NULL, &_3, "debug", NULL, 0, &_4, &_5);
	zephir_check_call_status();
	RETURN_CCTOR(&canSend);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Manager, buildTracker)
{
	zval _12;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *channel_param = NULL, config, _0, _1, _2, _3, _4, _5, _6, _7, _11, _8$$3, _9$$3, _10$$3;
	zval channel;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channel);
	ZVAL_UNDEF(&config);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_9$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_12);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(channel)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &channel_param);
	if (UNEXPECTED(Z_TYPE_P(channel_param) != IS_STRING && Z_TYPE_P(channel_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'channel' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(channel_param) == IS_STRING)) {
		zephir_get_strval(&channel, channel_param);
	} else {
		ZEPHIR_INIT_VAR(&channel);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "config");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	zephir_read_property(&_3, &_1, ZEND_STRL("throttle"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_OBS_VAR(&config);
	zephir_read_property(&config, &_3, ZEND_STRL("defaults"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "config");
	ZEPHIR_CALL_METHOD(&_5, &_4, "get", NULL, 0, &_2);
	zephir_check_call_status();
	zephir_read_property(&_6, &_5, ZEND_STRL("throttle"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_7, &_6, "has", NULL, 0, &channel);
	zephir_check_call_status();
	if (zephir_is_true(&_7)) {
		ZEPHIR_CALL_METHOD(&_8$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		zephir_read_property(&_9$$3, &_8$$3, ZEND_STRL("throttle"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&_10$$3, &_9$$3, "get", NULL, 0, &channel);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, &config, "merge", NULL, 0, &_10$$3);
		zephir_check_call_status();
	}
	ZEPHIR_CALL_METHOD(&_11, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_12);
	zephir_create_array(&_12, 1, 0);
	zephir_array_fast_append(&_12, &config);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "TwistersFury\\ChatBot\\Connection\\Throttle\\Tracker");
	ZEPHIR_RETURN_CALL_METHOD(&_11, "get", NULL, 0, &_2, &_12);
	zephir_check_call_status();
	RETURN_MM();
}

zend_object *zephir_init_properties_TwistersFury_ChatBot_Connection_Throttle_Manager(zend_class_entry *class_type)
{
		zval _0, _1$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
		ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1$$3);
	

		ZEPHIR_MM_GROW();
	
	{
		zval local_this_ptr, *this_ptr = &local_this_ptr;
		ZEPHIR_CREATE_OBJECT(this_ptr, class_type);
		zephir_read_property_ex(&_0, this_ptr, ZEND_STRL("trackers"), PH_NOISY_CC | PH_READONLY);
		if (Z_TYPE_P(&_0) == IS_NULL) {
			ZEPHIR_INIT_VAR(&_1$$3);
			array_init(&_1$$3);
			zephir_update_property_zval_ex(this_ptr, ZEND_STRL("trackers"), &_1$$3);
		}
		ZEPHIR_MM_RESTORE();
		return Z_OBJ_P(this_ptr);
	}
}

