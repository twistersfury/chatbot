
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "kernel/exception.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/concat.h"
#include "kernel/string.h"
#include "ext/date/php_date.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Connection_Throttle_Tracker)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Connection\\Throttle, Tracker, twistersfury_chatbot, connection_throttle_tracker, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\abstractconfiginjectable")), twistersfury_chatbot_connection_throttle_tracker_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_connection_throttle_tracker_ce, SL("throttleLog"), ZEND_ACC_PRIVATE);
	zend_declare_property_long(twistersfury_chatbot_connection_throttle_tracker_ce, SL("throttleCount"), 0, ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_connection_throttle_tracker_ce, SL("messageLog"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_connection_throttle_tracker_ce, SL("filterDate"), ZEND_ACC_PRIVATE);
	twistersfury_chatbot_connection_throttle_tracker_ce->create_object = zephir_init_properties_TwistersFury_ChatBot_Connection_Throttle_Tracker;
	zephir_declare_class_constant_string(twistersfury_chatbot_connection_throttle_tracker_ce, SL("CACHE_KEY"), "TwistersFury.ChatBot.Connection.Throttle");

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, getMaxCount)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("count"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, getCacheTtl)
{
	zval *this_ptr = getThis();



	RETURN_LONG(3);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, getSeconds)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("seconds"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, getCount)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "throttleCount");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, canSend)
{
	zend_bool _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, _0, _1, _2, _3, _5, _6, _7;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "canSend");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "isthrottledbycount", NULL, 0);
	zephir_check_call_status();
	_4 = zephir_is_true(&_3);
	if (!(_4)) {
		ZEPHIR_CALL_METHOD(&_6, packet, "getmessage", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_5, this_ptr, "isthrottledbymessage", NULL, 0, &_6);
		zephir_check_call_status();
		_4 = zephir_is_true(&_5);
	}
	if (_4) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_CALL_METHOD(&_7, this_ptr, "getcurrenttime", NULL, 0);
	zephir_check_call_status();
	zephir_update_property_array_append(this_ptr, SL("throttleLog"), &_7);
	RETURN_MM_BOOL(1);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, isThrottledByMessage)
{
	zval _3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_7 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, cacheKey, _0, _1, _2, _4, _5, _6, _8, _9, _10, _11, _12, _13, _14;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&cacheKey);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_13);
	ZVAL_UNDEF(&_14);
	ZVAL_UNDEF(&_3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	if (UNEXPECTED(Z_TYPE_P(message_param) != IS_STRING && Z_TYPE_P(message_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'message' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(message_param) == IS_STRING)) {
		zephir_get_strval(&message, message_param);
	} else {
		ZEPHIR_INIT_VAR(&message);
	}


	ZEPHIR_CALL_METHOD(&cacheKey, this_ptr, "buildcachekey", NULL, 0, &message);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 3, 0);
	zephir_array_update_string(&_3, SL("cacheKey"), &cacheKey, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "cache");
	ZEPHIR_CALL_METHOD(&_5, &_4, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, &_5, "has", NULL, 0, &cacheKey);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("hasKey"), &_6, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getcachettl", &_7, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("ttl"), &_6, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Tracker:isThrottledByMessage");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "cache");
	ZEPHIR_CALL_METHOD(&_8, &_6, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_9, &_8, "has", NULL, 0, &cacheKey);
	zephir_check_call_status();
	if (zephir_is_true(&_9)) {
		RETURN_MM_BOOL(1);
	}
	ZEPHIR_CALL_METHOD(&_10, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "cache");
	ZEPHIR_CALL_METHOD(&_11, &_10, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_13, this_ptr, "getcurrenttime", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_14, this_ptr, "getcachettl", &_7, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_12, &_11, "set", NULL, 0, &cacheKey, &_13, &_14);
	zephir_check_call_status();
	if (!(zephir_is_true(&_12))) {
		ZEPHIR_THROW_EXCEPTION_DEBUG_STR(zend_ce_exception, "Failed To Save Cache", "twistersfury/chatbot/Connection/Throttle/Tracker.zep", 73);
		return;
	}
	RETURN_MM_BOOL(0);
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, buildCacheKey)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *message_param = NULL, _0;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	if (UNEXPECTED(Z_TYPE_P(message_param) != IS_STRING && Z_TYPE_P(message_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'message' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(message_param) == IS_STRING)) {
		zephir_get_strval(&message, message_param);
	} else {
		ZEPHIR_INIT_VAR(&message);
	}


	ZEPHIR_INIT_VAR(&_0);
	zephir_md5(&_0, &message);
	ZEPHIR_CONCAT_SSV(return_value, "TwistersFury.ChatBot.Connection.Throttle", ".", &_0);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, isThrottledByCount)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "buildfilterdate", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "setfilterdate", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "filterlog", NULL, 25);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getcount", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getmaxcount", NULL, 0);
	zephir_check_call_status();
	RETURN_MM_BOOL(ZEPHIR_GT(&_1, &_2));
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, getCurrentTime)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "DateTime");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, getFilterDate)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "filterDate");
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, setFilterDate)
{
	zval *filterDate, filterDate_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&filterDate_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(filterDate, php_date_get_date_ce())
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &filterDate);


	zephir_update_property_zval(this_ptr, ZEND_STRL("filterDate"), filterDate);
	RETURN_THISW();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, filterLog)
{
	zval _4;
	zval _0, _1, _2, _3, _5, _6, _7;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_4);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "filter");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	zephir_read_property(&_3, this_ptr, ZEND_STRL("throttleLog"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 2, 0);
	zephir_array_fast_append(&_4, this_ptr);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "filterDate");
	zephir_array_fast_append(&_4, &_2);
	ZEPHIR_CALL_FUNCTION(&_5, "array_filter", NULL, 26, &_3, &_4);
	zephir_check_call_status();
	zephir_update_property_zval(this_ptr, ZEND_STRL("throttleLog"), &_5);
	zephir_read_property(&_6, this_ptr, ZEND_STRL("throttleLog"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_INIT_ZVAL_NREF(_7);
	ZVAL_LONG(&_7, zephir_fast_count_int(&_6));
	zephir_update_property_zval(this_ptr, ZEND_STRL("throttleCount"), &_7);
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, filterDate)
{
	zval _3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *sentAt, sentAt_sub, _0, _1, _2, _4, _5;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&sentAt_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(sentAt, php_date_get_date_ce())
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &sentAt);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 2, 0);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Y-m-d H:i:s");
	ZEPHIR_CALL_METHOD(&_4, sentAt, "format", NULL, 0, &_2);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("sentAt"), &_4, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getfilterdate", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Y-m-d H:i:s");
	ZEPHIR_CALL_METHOD(&_5, &_4, "format", NULL, 0, &_2);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("filterDate"), &_5, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Tracker:filterDate");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getfilterdate", NULL, 0);
	zephir_check_call_status();
	RETURN_MM_BOOL(ZEPHIR_GT(sentAt, &_5));
}

PHP_METHOD(TwistersFury_ChatBot_Connection_Throttle_Tracker, buildFilterDate)
{
	zval _0, _1, _2, currentTime, pastInterval, _3, _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&currentTime);
	ZVAL_UNDEF(&pastInterval);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "buildFilter");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&currentTime);
	object_init_ex(&currentTime, php_date_get_date_ce());
	ZEPHIR_CALL_METHOD(NULL, &currentTime, "__construct", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&pastInterval);
	object_init_ex(&pastInterval, php_date_get_interval_ce());
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getseconds", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	ZEPHIR_CONCAT_SVS(&_4, "PT", &_3, "S");
	ZEPHIR_CALL_METHOD(NULL, &pastInterval, "__construct", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &currentTime, "sub", NULL, 0, &pastInterval);
	zephir_check_call_status();
	RETURN_CCTOR(&currentTime);
}

zend_object *zephir_init_properties_TwistersFury_ChatBot_Connection_Throttle_Tracker(zend_class_entry *class_type)
{
		zval _0, _2, _1$$3, _3$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
		ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_3$$4);
	

		ZEPHIR_MM_GROW();
	
	{
		zval local_this_ptr, *this_ptr = &local_this_ptr;
		ZEPHIR_CREATE_OBJECT(this_ptr, class_type);
		zephir_read_property_ex(&_0, this_ptr, ZEND_STRL("messageLog"), PH_NOISY_CC | PH_READONLY);
		if (Z_TYPE_P(&_0) == IS_NULL) {
			ZEPHIR_INIT_VAR(&_1$$3);
			array_init(&_1$$3);
			zephir_update_property_zval_ex(this_ptr, ZEND_STRL("messageLog"), &_1$$3);
		}
		zephir_read_property_ex(&_2, this_ptr, ZEND_STRL("throttleLog"), PH_NOISY_CC | PH_READONLY);
		if (Z_TYPE_P(&_2) == IS_NULL) {
			ZEPHIR_INIT_VAR(&_3$$4);
			array_init(&_3$$4);
			zephir_update_property_zval_ex(this_ptr, ZEND_STRL("throttleLog"), &_3$$4);
		}
		ZEPHIR_MM_RESTORE();
		return Z_OBJ_P(this_ptr);
	}
}

