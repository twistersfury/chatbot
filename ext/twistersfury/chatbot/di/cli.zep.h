
extern zend_class_entry *twistersfury_chatbot_di_cli_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Di_Cli);

PHP_METHOD(TwistersFury_ChatBot_Di_Cli, initialize);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_di_cli_initialize, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_di_cli_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Di_Cli, initialize, arginfo_twistersfury_chatbot_di_cli_initialize, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
