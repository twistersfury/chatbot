
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Di_Http)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Di, Http, twistersfury_chatbot, di_http, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\http")), NULL, 0);

	return SUCCESS;
}

