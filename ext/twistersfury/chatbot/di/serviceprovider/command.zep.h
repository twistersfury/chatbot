
extern zend_class_entry *twistersfury_chatbot_di_serviceprovider_command_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Di_ServiceProvider_Command);

PHP_METHOD(TwistersFury_ChatBot_Di_ServiceProvider_Command, registerFactory);
PHP_METHOD(TwistersFury_ChatBot_Di_ServiceProvider_Command, registerParser);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_di_serviceprovider_command_registerfactory, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_di_serviceprovider_command_registerparser, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_di_serviceprovider_command_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Di_ServiceProvider_Command, registerFactory, arginfo_twistersfury_chatbot_di_serviceprovider_command_registerfactory, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Di_ServiceProvider_Command, registerParser, arginfo_twistersfury_chatbot_di_serviceprovider_command_registerparser, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
