
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Exceptions_Throttled)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Exceptions, Throttled, twistersfury_chatbot, exceptions_throttled, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\exceptions\\nonfatalexception")), NULL, 0);

	return SUCCESS;
}

