
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/concat.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Factory_AbstractFactory)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Factory, AbstractFactory, twistersfury_chatbot, factory_abstractfactory, zephir_get_internal_ce(SL("phalcon\\factory\\abstractfactory")), twistersfury_chatbot_factory_abstractfactory_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_declare_property_null(twistersfury_chatbot_factory_abstractfactory_ce, SL("diContainer"), ZEND_ACC_PROTECTED);
	zend_class_implements(twistersfury_chatbot_factory_abstractfactory_ce, 1, zephir_get_internal_ce(SL("phalcon\\di\\injectionawareinterface")));
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, getDi)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "diContainer");
}

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, setDi)
{
	zval *container, container_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&container_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(container, zephir_get_internal_ce(SL("phalcon\\di\\diinterface")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &container);


	zephir_update_property_zval(this_ptr, ZEND_STRL("diContainer"), container);
}

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, __construct)
{
	zend_class_entry *_1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *services_param = NULL, _0;
	zval services;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&services);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(services)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &services_param);
	if (!services_param) {
		ZEPHIR_INIT_VAR(&services);
		array_init(&services);
	} else {
	ZEPHIR_OBS_COPY_OR_DUP(&services, services_param);
	}


	_1 = zephir_fetch_class_str_ex(SL("Phalcon\\Di\\Di"), ZEND_FETCH_CLASS_AUTO);
	ZEPHIR_CALL_CE_STATIC(&_0, _1, "getdefault", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "setdi", NULL, 0, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "init", NULL, 0, &services);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, load)
{
	zval _16, _18, _29, _4$$4;
	zend_class_entry *_13, *_27;
	zval adapter, _7;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *config, config_sub, __$false, exception, _0, driver, _6, _8, _9, _12, _14, _15, _17, instance, _26, _28, _1$$4, _2$$4, _3$$4, _5$$4, _10$$7, _11$$7, _19$$8, _20$$8, _21$$8, _22$$9, _23$$9, _24$$9, _25$$9;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&config_sub);
	ZVAL_BOOL(&__$false, 0);
	ZVAL_UNDEF(&exception);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&driver);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_14);
	ZVAL_UNDEF(&_15);
	ZVAL_UNDEF(&_17);
	ZVAL_UNDEF(&instance);
	ZVAL_UNDEF(&_26);
	ZVAL_UNDEF(&_28);
	ZVAL_UNDEF(&_1$$4);
	ZVAL_UNDEF(&_2$$4);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&_5$$4);
	ZVAL_UNDEF(&_10$$7);
	ZVAL_UNDEF(&_11$$7);
	ZVAL_UNDEF(&_19$$8);
	ZVAL_UNDEF(&_20$$8);
	ZVAL_UNDEF(&_21$$8);
	ZVAL_UNDEF(&_22$$9);
	ZVAL_UNDEF(&_23$$9);
	ZVAL_UNDEF(&_24$$9);
	ZVAL_UNDEF(&_25$$9);
	ZVAL_UNDEF(&adapter);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_16);
	ZVAL_UNDEF(&_18);
	ZVAL_UNDEF(&_29);
	ZVAL_UNDEF(&_4$$4);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(config, zephir_get_internal_ce(SL("phalcon\\config\\config")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &config);



	/* try_start_1: */

		ZEPHIR_CALL_METHOD(NULL, this_ptr, "checkconfig", NULL, 0, config);
		zephir_check_call_status_or_jump(try_end_1);

	try_end_1:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_0);
		ZVAL_OBJ(&_0, EG(exception));
		Z_ADDREF_P(&_0);
		if (zephir_is_instance_of(&_0, SL("Exception"))) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&exception, &_0);
			ZEPHIR_CALL_METHOD(&_1$$4, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_3$$4);
			ZVAL_STRING(&_3$$4, "logger");
			ZEPHIR_CALL_METHOD(&_2$$4, &_1$$4, "get", NULL, 0, &_3$$4);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_4$$4);
			zephir_create_array(&_4$$4, 1, 0);
			ZEPHIR_CALL_METHOD(&_5$$4, config, "toarray", NULL, 0);
			zephir_check_call_status();
			zephir_array_update_string(&_4$$4, SL("config"), &_5$$4, PH_COPY | PH_SEPARATE);
			ZEPHIR_INIT_NVAR(&_3$$4);
			ZVAL_STRING(&_3$$4, "Configuration Error");
			ZEPHIR_CALL_METHOD(NULL, &_2$$4, "debug", NULL, 0, &_3$$4, &_4$$4);
			zephir_check_call_status();
			zephir_throw_exception_debug(&exception, "twistersfury/chatbot/Factory/AbstractFactory.zep", 47);
			ZEPHIR_MM_RESTORE();
			return;
		}
	}
	ZEPHIR_OBS_VAR(&_6);
	zephir_read_property(&_6, config, ZEND_STRL("adapter"), PH_NOISY_CC);
	zephir_cast_to_string(&_7, &_6);
	ZEPHIR_CPY_WRT(&adapter, &_7);

	/* try_start_2: */

		ZEPHIR_CALL_METHOD(&driver, this_ptr, "getservice", NULL, 0, &adapter);
		zephir_check_call_status_or_jump(try_end_2);

	try_end_2:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_8);
		ZVAL_OBJ(&_8, EG(exception));
		Z_ADDREF_P(&_8);
		ZEPHIR_INIT_VAR(&_9);
		if (zephir_is_instance_of(&_8, SL("Exception"))) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&_9, &_8);
			ZEPHIR_OBS_NVAR(&driver);
			zephir_read_property(&driver, config, ZEND_STRL("adapter"), PH_NOISY_CC);
		}
	}
	if (!(zephir_class_exists(&driver, 1))) {
		ZEPHIR_INIT_VAR(&_10$$7);
		object_init_ex(&_10$$7, zephir_get_internal_ce(SL("phalcon\\di\\exception")));
		ZEPHIR_INIT_VAR(&_11$$7);
		ZEPHIR_CONCAT_SV(&_11$$7, "Driver Doesn't Exist: ", &driver);
		ZEPHIR_CALL_METHOD(NULL, &_10$$7, "__construct", NULL, 0, &_11$$7);
		zephir_check_call_status();
		zephir_throw_exception_debug(&_10$$7, "twistersfury/chatbot/Factory/AbstractFactory.zep", 64);
		ZEPHIR_MM_RESTORE();
		return;
	}
	_13 = zephir_fetch_class_str_ex(SL("Phalcon\\Di\\Di"), ZEND_FETCH_CLASS_AUTO);
	ZEPHIR_CALL_CE_STATIC(&_12, _13, "getdefault", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_15);
	ZVAL_STRING(&_15, "logger");
	ZEPHIR_CALL_METHOD(&_14, &_12, "get", NULL, 0, &_15);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_16);
	zephir_create_array(&_16, 2, 0);
	zephir_array_update_string(&_16, SL("driver"), &driver, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_17, config, "toarray", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_16, SL("config"), &_17, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_15);
	ZVAL_STRING(&_15, "Factory");
	ZEPHIR_CALL_METHOD(NULL, &_14, "debug", NULL, 0, &_15, &_16);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_18);
	zephir_create_array(&_18, 1, 0);
	zephir_array_fast_append(&_18, config);
	ZEPHIR_INIT_VAR(&instance);
	ZEPHIR_LAST_CALL_STATUS = zephir_create_instance_params(&instance, &driver, &_18);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_17, this_ptr, "getinterfacename", NULL, 0);
	zephir_check_call_status();
	if (!(Z_TYPE_P(&instance) == IS_OBJECT)) {
		ZEPHIR_INIT_VAR(&_19$$8);
		object_init_ex(&_19$$8, spl_ce_InvalidArgumentException);
		zephir_read_property(&_20$$8, config, ZEND_STRL("adapter"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_INIT_VAR(&_21$$8);
		ZEPHIR_CONCAT_SV(&_21$$8, "Invalid Adapter Name: ", &_20$$8);
		ZEPHIR_CALL_METHOD(NULL, &_19$$8, "__construct", NULL, 2, &_21$$8);
		zephir_check_call_status();
		zephir_throw_exception_debug(&_19$$8, "twistersfury/chatbot/Factory/AbstractFactory.zep", 78);
		ZEPHIR_MM_RESTORE();
		return;
	} else if (!(zephir_is_instance_of(&instance, Z_STRVAL_P(&_17), Z_STRLEN_P(&_17)))) {
		ZEPHIR_INIT_VAR(&_22$$9);
		object_init_ex(&_22$$9, spl_ce_InvalidArgumentException);
		ZEPHIR_INIT_VAR(&_23$$9);
		zephir_get_class(&_23$$9, &instance, 0);
		ZEPHIR_CALL_METHOD(&_24$$9, this_ptr, "getinterfacename", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_25$$9);
		ZEPHIR_CONCAT_SVSV(&_25$$9, "Adapter ", &_23$$9, " Must Implement ", &_24$$9);
		ZEPHIR_CALL_METHOD(NULL, &_22$$9, "__construct", NULL, 2, &_25$$9);
		zephir_check_call_status();
		zephir_throw_exception_debug(&_22$$9, "twistersfury/chatbot/Factory/AbstractFactory.zep", 80);
		ZEPHIR_MM_RESTORE();
		return;
	}
	_27 = zephir_fetch_class_str_ex(SL("Phalcon\\Di\\Di"), ZEND_FETCH_CLASS_AUTO);
	ZEPHIR_CALL_CE_STATIC(&_26, _27, "getdefault", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_15);
	ZVAL_STRING(&_15, "logger");
	ZEPHIR_CALL_METHOD(&_28, &_26, "get", NULL, 0, &_15);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_29);
	zephir_create_array(&_29, 2, 0);
	zephir_array_update_string(&_29, SL("driver"), &driver, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_29, SL("initialize"), &__$false, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_15);
	ZVAL_STRING(&_15, "Factory Initialize");
	ZEPHIR_CALL_METHOD(NULL, &_28, "debug", NULL, 0, &_15, &_29);
	zephir_check_call_status();
	if ((zephir_method_exists_ex(&instance, ZEND_STRL("initialize")) == SUCCESS)) {
		ZEPHIR_CALL_METHOD(NULL, &instance, "initialize", NULL, 0);
		zephir_check_call_status();
	}
	RETURN_CCTOR(&instance);
}

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, quickLoad)
{
	zval _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *adapter_param = NULL, _0;
	zval adapter;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&adapter);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(adapter)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &adapter_param);
	zephir_get_strval(&adapter, adapter_param);


	ZEPHIR_INIT_VAR(&_0);
	object_init_ex(&_0, zephir_get_internal_ce(SL("phalcon\\config\\config")));
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 1, 0);
	zephir_array_update_string(&_1, SL("adapter"), &adapter, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(NULL, &_0, "__construct", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "load", NULL, 0, &_0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, getInterfaceName)
{
}

