
extern zend_class_entry *twistersfury_chatbot_factory_abstractfactory_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Factory_AbstractFactory);

PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, getDi);
PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, setDi);
PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, __construct);
PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, load);
PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, quickLoad);
PHP_METHOD(TwistersFury_ChatBot_Factory_AbstractFactory, getInterfaceName);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_factory_abstractfactory_getdi, 0, 0, Phalcon\\Di\\DiInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_factory_abstractfactory_setdi, 0, 1, IS_VOID, 0)

	ZEND_ARG_OBJ_INFO(0, container, Phalcon\\Di\\DiInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_factory_abstractfactory___construct, 0, 0, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, services, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, services, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_factory_abstractfactory_load, 0, 0, 1)
	ZEND_ARG_OBJ_INFO(0, config, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_factory_abstractfactory_quickload, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, adapter, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_factory_abstractfactory_getinterfacename, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_factory_abstractfactory_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Factory_AbstractFactory, getDi, arginfo_twistersfury_chatbot_factory_abstractfactory_getdi, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Factory_AbstractFactory, setDi, arginfo_twistersfury_chatbot_factory_abstractfactory_setdi, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Factory_AbstractFactory, __construct, arginfo_twistersfury_chatbot_factory_abstractfactory___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(TwistersFury_ChatBot_Factory_AbstractFactory, load, arginfo_twistersfury_chatbot_factory_abstractfactory_load, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Factory_AbstractFactory, quickLoad, arginfo_twistersfury_chatbot_factory_abstractfactory_quickload, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Factory_AbstractFactory, getInterfaceName, arginfo_twistersfury_chatbot_factory_abstractfactory_getinterfacename, ZEND_ACC_ABSTRACT|ZEND_ACC_PUBLIC)
	PHP_FE_END
};
