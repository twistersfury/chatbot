
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "ext/spl/spl_exceptions.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Factory_Exceptions_Invalid)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Factory\\Exceptions, Invalid, twistersfury_chatbot, factory_exceptions_invalid, spl_ce_InvalidArgumentException, NULL, 0);

	return SUCCESS;
}

