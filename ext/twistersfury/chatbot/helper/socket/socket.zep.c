
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Helper_Socket_Socket)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Helper\\Socket, Socket, twistersfury_chatbot, helper_socket_socket, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_helper_socket_socket_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_helper_socket_socket_ce, SL("config"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_helper_socket_socket_ce, SL("handle"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_helper_socket_socket_ce, SL("rawSocket"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_helper_socket_socket_ce, SL("lastError"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_helper_socket_socket_ce, SL("lastErrorNumber"), ZEND_ACC_PRIVATE);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, __construct)
{
	zval *config, config_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&config_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(config, zephir_get_internal_ce(SL("phalcon\\config\\config")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &config);


	zephir_update_property_zval(this_ptr, ZEND_STRL("config"), config);
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, connect)
{
	zval _0, _1, _2, _4, _5, _6, _7, _8, _9, _11, _10$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_3 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_10$$4);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getrawsocket", NULL, 5);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getconfig", &_3, 0);
	zephir_check_call_status();
	zephir_read_property(&_4, &_2, ZEND_STRL("host"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getconfig", &_3, 0);
	zephir_check_call_status();
	zephir_read_property(&_6, &_5, ZEND_STRL("port"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_INIT_VAR(&_7);
	ZEPHIR_CALL_METHOD(&_8, this_ptr, "getconfig", &_3, 0);
	zephir_check_call_status();
	zephir_read_property(&_9, &_8, ZEND_STRL("timeout"), PH_NOISY_CC | PH_READONLY);
	if (!(zephir_is_true(&_9))) {
		ZEPHIR_INIT_NVAR(&_7);
		ZVAL_LONG(&_7, 30);
	} else {
		ZEPHIR_CALL_METHOD(&_10$$4, this_ptr, "getconfig", &_3, 0);
		zephir_check_call_status();
		ZEPHIR_OBS_NVAR(&_7);
		zephir_read_property(&_7, &_10$$4, ZEND_STRL("timeout"), PH_NOISY_CC);
	}
	ZEPHIR_CALL_METHOD(&_1, &_0, "open", NULL, 0, &_4, &_6, &_7);
	zephir_check_call_status();
	zephir_update_property_zval(this_ptr, ZEND_STRL("handle"), &_1);
	ZEPHIR_CALL_METHOD(&_11, this_ptr, "isconnected", NULL, 0);
	zephir_check_call_status();
	if (UNEXPECTED(!zephir_is_true(&_11))) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "raiseexception", NULL, 6);
		zephir_check_call_status();
	}
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, close)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getrawsocket", NULL, 5);
	zephir_check_call_status();
	zephir_read_property(&_1, this_ptr, ZEND_STRL("handle"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(NULL, &_0, "close", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, getConfig)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "config");
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, isConnected)
{
	zend_bool _1, _3;
	zval _0, _2, _4, _5, _6;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);


	ZEPHIR_MM_GROW();

	zephir_read_property(&_0, this_ptr, ZEND_STRL("handle"), PH_NOISY_CC | PH_READONLY);
	_1 = zephir_is_true(&_0);
	if (_1) {
		zephir_read_property(&_2, this_ptr, ZEND_STRL("handle"), PH_NOISY_CC | PH_READONLY);
		_1 = Z_TYPE_P(&_2) == IS_RESOURCE;
	}
	_3 = _1;
	if (_3) {
		ZEPHIR_CALL_METHOD(&_4, this_ptr, "getrawsocket", NULL, 5);
		zephir_check_call_status();
		zephir_read_property(&_6, this_ptr, ZEND_STRL("handle"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&_5, &_4, "eof", NULL, 0, &_6);
		zephir_check_call_status();
		_3 = !ZEPHIR_IS_TRUE_IDENTICAL(&_5);
	}
	RETURN_MM_BOOL(_3);
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, send)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, _0, _1, _2;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	zephir_get_strval(&message, message_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "isconnected", NULL, 0);
	zephir_check_call_status();
	if (!(zephir_is_true(&_0))) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "raiseexception", NULL, 6);
		zephir_check_call_status();
	}
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getrawsocket", NULL, 5);
	zephir_check_call_status();
	zephir_read_property(&_2, this_ptr, ZEND_STRL("handle"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(NULL, &_1, "write", NULL, 0, &_2, &message);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, read)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "isconnected", NULL, 0);
	zephir_check_call_status();
	if (!(zephir_is_true(&_0))) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "raiseexception", NULL, 6);
		zephir_check_call_status();
	}
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getrawsocket", NULL, 5);
	zephir_check_call_status();
	zephir_read_property(&_2, this_ptr, ZEND_STRL("handle"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_RETURN_CALL_METHOD(&_1, "get", NULL, 0, &_2);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, raiseException)
{
	zval _2;
	zval _0, _1, _3, _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	zephir_create_array(&_2, 2, 0);
	ZEPHIR_OBS_VAR(&_3);
	zephir_read_property(&_3, this_ptr, ZEND_STRL("lastError"), PH_NOISY_CC);
	zephir_array_fast_append(&_2, &_3);
	ZEPHIR_OBS_NVAR(&_3);
	zephir_read_property(&_3, this_ptr, ZEND_STRL("lastErrorNumber"), PH_NOISY_CC);
	zephir_array_fast_append(&_2, &_3);
	ZEPHIR_INIT_VAR(&_4);
	ZVAL_STRING(&_4, "TwistersFury\\ChatBot\\Helper\\Socket\\ConnectionException");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_4, &_2);
	zephir_check_call_status();
	zephir_throw_exception_debug(&_1, "twistersfury/chatbot/Helper/Socket/Socket.zep", 75);
	ZEPHIR_MM_RESTORE();
	return;
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, getRawSocket)
{
	zval _0, _1$$3, _2$$3, _3$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_3$$3);


	ZEPHIR_MM_GROW();

	zephir_read_property(&_0, this_ptr, ZEND_STRL("rawSocket"), PH_NOISY_CC | PH_READONLY);
	if (UNEXPECTED(!zephir_is_true(&_0))) {
		ZEPHIR_CALL_METHOD(&_1$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_3$$3);
		ZVAL_STRING(&_3$$3, "TwistersFury\\ChatBot\\Helper\\Socket\\Wrapper");
		ZEPHIR_CALL_METHOD(&_2$$3, &_1$$3, "get", NULL, 0, &_3$$3);
		zephir_check_call_status();
		zephir_update_property_zval(this_ptr, ZEND_STRL("rawSocket"), &_2$$3);
	}
	RETURN_MM_MEMBER(getThis(), "rawSocket");
}

