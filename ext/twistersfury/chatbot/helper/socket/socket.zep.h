
extern zend_class_entry *twistersfury_chatbot_helper_socket_socket_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Helper_Socket_Socket);

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, __construct);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, connect);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, close);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, getConfig);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, isConnected);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, send);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, read);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, raiseException);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Socket, getRawSocket);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket___construct, 0, 0, 1)
	ZEND_ARG_OBJ_INFO(0, config, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_connect, 0, 0, TwistersFury\\ChatBot\\Helper\\Socket\\Socket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_close, 0, 0, TwistersFury\\ChatBot\\Helper\\Socket\\Socket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_getconfig, 0, 0, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_isconnected, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_send, 0, 1, TwistersFury\\ChatBot\\Helper\\Socket\\Socket, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_read, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_raiseexception, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_socket_getrawsocket, 0, 0, TwistersFury\\ChatBot\\Helper\\Socket\\Wrapper, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_helper_socket_socket_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, __construct, arginfo_twistersfury_chatbot_helper_socket_socket___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, connect, arginfo_twistersfury_chatbot_helper_socket_socket_connect, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, close, arginfo_twistersfury_chatbot_helper_socket_socket_close, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, getConfig, arginfo_twistersfury_chatbot_helper_socket_socket_getconfig, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, isConnected, arginfo_twistersfury_chatbot_helper_socket_socket_isconnected, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, send, arginfo_twistersfury_chatbot_helper_socket_socket_send, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, read, arginfo_twistersfury_chatbot_helper_socket_socket_read, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, raiseException, arginfo_twistersfury_chatbot_helper_socket_socket_raiseexception, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Socket, getRawSocket, arginfo_twistersfury_chatbot_helper_socket_socket_getrawsocket, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
