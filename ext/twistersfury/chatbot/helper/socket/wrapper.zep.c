
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/file.h"
#include "kernel/string.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Helper_Socket_Wrapper)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Helper\\Socket, Wrapper, twistersfury_chatbot, helper_socket_wrapper, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_helper_socket_wrapper_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_helper_socket_wrapper_ce, SL("errorNo"), ZEND_ACC_PRIVATE);
	zend_declare_property_null(twistersfury_chatbot_helper_socket_wrapper_ce, SL("errorMessage"), ZEND_ACC_PRIVATE);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, open)
{
	zval _7;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	double timeout;
	zend_long port, ZEPHIR_LAST_CALL_STATUS;
	zval *host_param = NULL, *port_param = NULL, *timeout_param = NULL, __$null, __$false, handle, _0, _1, _2, _3, _4, _5, _6, _8;
	zval host;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&host);
	ZVAL_NULL(&__$null);
	ZVAL_BOOL(&__$false, 0);
	ZVAL_UNDEF(&handle);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_7);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STR(host)
		Z_PARAM_LONG(port)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL_OR_NULL(timeout)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &host_param, &port_param, &timeout_param);
	zephir_get_strval(&host, host_param);
	port = zephir_get_intval(port_param);
	if (!timeout_param) {
		timeout = 0;
	} else {
		timeout = zephir_get_doubleval(timeout_param);
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("errorMessage"), &__$null);
	zephir_update_property_zval(this_ptr, ZEND_STRL("errorNo"), &__$null);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("errorNo"), PH_NOISY_CC | PH_READONLY);
	zephir_read_property(&_1, this_ptr, ZEND_STRL("errorMessage"), PH_NOISY_CC | PH_READONLY);
	ZVAL_LONG(&_2, port);
	ZVAL_DOUBLE(&_3, timeout);
	ZEPHIR_MAKE_REF(&_0);
	ZEPHIR_MAKE_REF(&_1);
	ZEPHIR_CALL_FUNCTION(&handle, "fsockopen", NULL, 27, &host, &_2, &_0, &_1, &_3);
	ZEPHIR_UNREF(&_0);
	ZEPHIR_UNREF(&_1);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(NULL, "stream_set_blocking", NULL, 28, &handle, &__$false);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_6);
	ZVAL_STRING(&_6, "logger");
	ZEPHIR_CALL_METHOD(&_5, &_4, "get", NULL, 0, &_6);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_7);
	zephir_create_array(&_7, 2, 0);
	ZEPHIR_OBS_VAR(&_8);
	zephir_read_property(&_8, this_ptr, ZEND_STRL("errorNo"), PH_NOISY_CC);
	zephir_array_update_string(&_7, SL("errorNo"), &_8, PH_COPY | PH_SEPARATE);
	ZEPHIR_OBS_NVAR(&_8);
	zephir_read_property(&_8, this_ptr, ZEND_STRL("errorMessage"), PH_NOISY_CC);
	zephir_array_update_string(&_7, SL("errorMessage"), &_8, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_6);
	ZVAL_STRING(&_6, "Raw Socket Open");
	ZEPHIR_CALL_METHOD(NULL, &_5, "debug", NULL, 0, &_6, &_7);
	zephir_check_call_status();
	RETURN_CCTOR(&handle);
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, close)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *socket, socket_sub, _0, _1, _2;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&socket_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_RESOURCE(socket)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &socket);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Raw Socket Close");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	RETURN_MM_BOOL(zephir_fclose(socket));
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, get)
{
	zval _3$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *socket, socket_sub, message, _0$$3, _1$$3, _2$$3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&socket_sub);
	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_3$$3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_RESOURCE(socket)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &socket);


	ZEPHIR_CALL_FUNCTION(&message, "fgets", NULL, 29, socket);
	zephir_check_call_status();
	if (!ZEPHIR_IS_FALSE_IDENTICAL(&message)) {
		ZEPHIR_CALL_METHOD(&_0$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_2$$3);
		ZVAL_STRING(&_2$$3, "logger");
		ZEPHIR_CALL_METHOD(&_1$$3, &_0$$3, "get", NULL, 0, &_2$$3);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_3$$3);
		zephir_create_array(&_3$$3, 1, 0);
		zephir_array_update_string(&_3$$3, SL("message"), &message, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_NVAR(&_2$$3);
		ZVAL_STRING(&_2$$3, "Raw Socket Get");
		ZEPHIR_CALL_METHOD(NULL, &_1$$3, "debug", NULL, 0, &_2$$3, &_3$$3);
		zephir_check_call_status();
	}
	RETURN_CCTOR(&message);
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, eof)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *socket, socket_sub, eof;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&socket_sub);
	ZVAL_UNDEF(&eof);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_RESOURCE(socket)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &socket);


	ZEPHIR_INIT_VAR(&eof);
	ZVAL_BOOL(&eof, zephir_feof(socket));
	RETURN_CCTOR(&eof);
}

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, write)
{
	zval _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval message;
	zval *socket, socket_sub, *message_param = NULL, result, _0, _1, _2, _3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&socket_sub);
	ZVAL_UNDEF(&result);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_4);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_RESOURCE(socket)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &socket, &message_param);
	zephir_get_strval(&message, message_param);


	ZVAL_LONG(&_0, zephir_fast_strlen_ev(&message));
	ZEPHIR_CALL_FUNCTION(&result, "fwrite", NULL, 30, socket, &message, &_0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "logger");
	ZEPHIR_CALL_METHOD(&_2, &_1, "get", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 2, 0);
	zephir_array_update_string(&_4, SL("result"), &result, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_4, SL("message"), &message, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_3);
	ZVAL_STRING(&_3, "Raw Socket Write");
	ZEPHIR_CALL_METHOD(NULL, &_2, "debug", NULL, 0, &_3, &_4);
	zephir_check_call_status();
	RETURN_CCTOR(&result);
}

