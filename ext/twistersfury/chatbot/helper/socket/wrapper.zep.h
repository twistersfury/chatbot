
extern zend_class_entry *twistersfury_chatbot_helper_socket_wrapper_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Helper_Socket_Wrapper);

PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, open);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, close);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, get);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, eof);
PHP_METHOD(TwistersFury_ChatBot_Helper_Socket_Wrapper, write);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_wrapper_open, 0, 0, 2)
	ZEND_ARG_TYPE_INFO(0, host, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, port, IS_LONG, 0)
	ZEND_ARG_TYPE_INFO(0, timeout, IS_DOUBLE, 1)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_wrapper_close, 0, 1, _IS_BOOL, 0)
	ZEND_ARG_INFO(0, socket)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_wrapper_get, 0, 0, 1)
	ZEND_ARG_INFO(0, socket)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_wrapper_eof, 0, 1, _IS_BOOL, 0)
	ZEND_ARG_INFO(0, socket)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_helper_socket_wrapper_write, 0, 2, IS_LONG, 0)
	ZEND_ARG_INFO(0, socket)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_helper_socket_wrapper_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Wrapper, open, arginfo_twistersfury_chatbot_helper_socket_wrapper_open, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Wrapper, close, arginfo_twistersfury_chatbot_helper_socket_wrapper_close, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Wrapper, get, arginfo_twistersfury_chatbot_helper_socket_wrapper_get, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Wrapper, eof, arginfo_twistersfury_chatbot_helper_socket_wrapper_eof, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Helper_Socket_Wrapper, write, arginfo_twistersfury_chatbot_helper_socket_wrapper_write, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
