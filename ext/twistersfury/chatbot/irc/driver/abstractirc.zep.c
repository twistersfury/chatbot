
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/array.h"
#include "kernel/operators.h"
#include "kernel/concat.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Driver_AbstractIrc)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Driver, AbstractIrc, twistersfury_chatbot, irc_driver_abstractirc, twistersfury_chatbot_helper_socket_socket_ce, twistersfury_chatbot_irc_driver_abstractirc_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_class_implements(twistersfury_chatbot_irc_driver_abstractirc_ce, 1, twistersfury_chatbot_connection_driver_driverinterface_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, initialize)
{
	zval _0, _1, _2, _3, _4, _5, _6;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "AbstractIrc Initialize");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_3);
	zephir_read_property(&_3, this_ptr, ZEND_STRL("connectionManager"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_4, &_3, "geteventsmanager", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "TwistersFury\\ChatBot\\Connection\\Throttle\\Listener\\Driver");
	ZEPHIR_CALL_METHOD(&_6, &_5, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "driver");
	ZEPHIR_CALL_METHOD(NULL, &_4, "attach", NULL, 0, &_2, &_6);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, getConnectionName)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("name"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, openConnection)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(NULL, this_ptr, "connect", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "sendpassword", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, &_0, "senduser", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_1, "sendnick", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, sendNick)
{
	zval _4, _5;
	zval _0, _1, _2, _3, _6, _7, _8, _9;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getpacketfactory", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 1, 0);
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 2, 0);
	add_assoc_stringl_ex(&_5, SL("adapter"), SL("NICK"));
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_7, &_6, ZEND_STRL("user"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_OBS_VAR(&_8);
	zephir_read_property(&_8, &_7, ZEND_STRL("nickname"), PH_NOISY_CC);
	zephir_array_update_string(&_5, SL("nickname"), &_8, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_4, &_5);
	ZEPHIR_INIT_VAR(&_9);
	ZVAL_STRING(&_9, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_9, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, &_0, "load", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "sendpacket", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, sendPassword)
{
	zval _8$$3, _9$$3;
	zval _0, _1, _2, _3, _4$$3, _5$$3, _6$$3, _7$$3, _10$$3, _11$$3, _12$$3, _13$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_12$$3);
	ZVAL_UNDEF(&_13$$3);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_9$$3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("user"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "password");
	ZEPHIR_CALL_METHOD(&_2, &_1, "has", NULL, 0, &_3);
	zephir_check_call_status();
	if (UNEXPECTED(zephir_is_true(&_2))) {
		ZEPHIR_CALL_METHOD(&_4$$3, this_ptr, "getpacketfactory", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_6$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_8$$3);
		zephir_create_array(&_8$$3, 1, 0);
		ZEPHIR_INIT_VAR(&_9$$3);
		zephir_create_array(&_9$$3, 2, 0);
		add_assoc_stringl_ex(&_9$$3, SL("adapter"), SL("PASS"));
		ZEPHIR_CALL_METHOD(&_10$$3, this_ptr, "getconfig", NULL, 0);
		zephir_check_call_status();
		zephir_read_property(&_11$$3, &_10$$3, ZEND_STRL("user"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_OBS_VAR(&_12$$3);
		zephir_read_property(&_12$$3, &_11$$3, ZEND_STRL("password"), PH_NOISY_CC);
		zephir_array_update_string(&_9$$3, SL("password"), &_12$$3, PH_COPY | PH_SEPARATE);
		zephir_array_fast_append(&_8$$3, &_9$$3);
		ZEPHIR_INIT_VAR(&_13$$3);
		ZVAL_STRING(&_13$$3, "Phalcon\\Config\\Config");
		ZEPHIR_CALL_METHOD(&_7$$3, &_6$$3, "get", NULL, 0, &_13$$3, &_8$$3);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(&_5$$3, &_4$$3, "load", NULL, 0, &_7$$3);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "sendpacket", NULL, 0, &_5$$3);
		zephir_check_call_status();
	}
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, sendUser)
{
	zval _4, _5;
	zval _0, _1, _2, _3, _6, _7, _8, _9, _10, _11;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getpacketfactory", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 1, 0);
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 3, 0);
	add_assoc_stringl_ex(&_5, SL("adapter"), SL("USER"));
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_7, &_6, ZEND_STRL("user"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_OBS_VAR(&_8);
	zephir_read_property(&_8, &_7, ZEND_STRL("ident"), PH_NOISY_CC);
	zephir_array_update_string(&_5, SL("ident"), &_8, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_9, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_10, &_9, ZEND_STRL("user"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_OBS_NVAR(&_8);
	zephir_read_property(&_8, &_10, ZEND_STRL("realname"), PH_NOISY_CC);
	zephir_array_update_string(&_5, SL("realname"), &_8, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_4, &_5);
	ZEPHIR_INIT_VAR(&_11);
	ZVAL_STRING(&_11, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_11, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, &_0, "load", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "sendpacket", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, closeConnection)
{
	zval _4, _5;
	zval _0, _1, _2, _3, _6, _7;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getpacketfactory", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 1, 0);
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 1, 0);
	add_assoc_stringl_ex(&_5, SL("adapter"), SL("QUIT"));
	zephir_array_fast_append(&_4, &_5);
	ZEPHIR_INIT_VAR(&_6);
	ZVAL_STRING(&_6, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_6, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, &_0, "load", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, this_ptr, "sendpacket", NULL, 0, &_1);
	zephir_check_call_status();
	ZVAL_LONG(&_7, 5);
	ZEPHIR_CALL_FUNCTION(NULL, "sleep", NULL, 7, &_7);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "close", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, keepAlive)
{
	zval _7, _8;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, _0, _1, _2, _3, _4, _5, _6;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Keep Alive");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getpacketfactory", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_7);
	zephir_create_array(&_7, 1, 0);
	ZEPHIR_INIT_VAR(&_8);
	zephir_create_array(&_8, 2, 0);
	add_assoc_stringl_ex(&_8, SL("adapter"), SL("PONG"));
	zephir_array_update_string(&_8, SL("packet"), packet, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_7, &_8);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_6, &_5, "get", NULL, 0, &_2, &_7);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_4, &_3, "load", NULL, 0, &_6);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "sendpacket", NULL, 0, &_4);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, sendPacket)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, _0, _1, _2, message, _3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_outgoingpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Send Packet");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&message, packet, "buildpacket", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	ZEPHIR_CONCAT_VS(&_3, &message, "\r\n");
	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "send", NULL, 0, &_3);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, readPacket)
{
	zval _1$$4, _14$$6, _16$$6;
	zval message, packet, exception, _10, connection, _0$$4, _2$$4, _3$$4, _4$$4, _5$$5, _6$$5, _7$$5, _8$$5, _9$$5, _11$$6, _12$$6, _13$$6, _15$$6, _17$$6, _18$$6;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&packet);
	ZVAL_UNDEF(&exception);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&connection);
	ZVAL_UNDEF(&_0$$4);
	ZVAL_UNDEF(&_2$$4);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&_4$$4);
	ZVAL_UNDEF(&_5$$5);
	ZVAL_UNDEF(&_6$$5);
	ZVAL_UNDEF(&_7$$5);
	ZVAL_UNDEF(&_8$$5);
	ZVAL_UNDEF(&_9$$5);
	ZVAL_UNDEF(&_11$$6);
	ZVAL_UNDEF(&_12$$6);
	ZVAL_UNDEF(&_13$$6);
	ZVAL_UNDEF(&_15$$6);
	ZVAL_UNDEF(&_17$$6);
	ZVAL_UNDEF(&_18$$6);
	ZVAL_UNDEF(&_1$$4);
	ZVAL_UNDEF(&_14$$6);
	ZVAL_UNDEF(&_16$$6);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&message, this_ptr, "read", NULL, 0);
	zephir_check_call_status();

	/* try_start_1: */

		if (!(Z_TYPE_P(&message) == IS_STRING)) {
			ZEPHIR_CALL_METHOD(&_0$$4, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_INIT_VAR(&_1$$4);
			zephir_create_array(&_1$$4, 1, 0);
			ZEPHIR_CALL_METHOD(&_2$$4, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_INIT_VAR(&_4$$4);
			ZVAL_STRING(&_4$$4, "Phalcon\\Config\\Config");
			ZEPHIR_CALL_METHOD(&_3$$4, &_2$$4, "get", NULL, 0, &_4$$4);
			zephir_check_call_status_or_jump(try_end_1);
			zephir_array_fast_append(&_1$$4, &_3$$4);
			ZEPHIR_INIT_NVAR(&_4$$4);
			ZVAL_STRING(&_4$$4, "TwistersFury\\ChatBot\\Connection\\Packet\\EmptyPacket");
			ZEPHIR_CALL_METHOD(&packet, &_0$$4, "get", NULL, 0, &_4$$4, &_1$$4);
			zephir_check_call_status_or_jump(try_end_1);
		} else {
			ZEPHIR_CALL_METHOD(&_5$$5, this_ptr, "getpacketfactory", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_CALL_METHOD(&_6$$5, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_INIT_VAR(&_8$$5);
			ZVAL_STRING(&_8$$5, "TwistersFury\\ChatBot\\Irc\\Packet\\Parser");
			ZEPHIR_CALL_METHOD(&_7$$5, &_6$$5, "get", NULL, 0, &_8$$5);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_CALL_METHOD(&_9$$5, &_7$$5, "parse", NULL, 0, &message);
			zephir_check_call_status_or_jump(try_end_1);
			ZEPHIR_CALL_METHOD(&packet, &_5$$5, "load", NULL, 0, &_9$$5);
			zephir_check_call_status_or_jump(try_end_1);
		}

	try_end_1:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_10);
		ZVAL_OBJ(&_10, EG(exception));
		Z_ADDREF_P(&_10);
		if (zephir_is_instance_of(&_10, SL("Phalcon\\Di\\Exception"))) {
			zend_clear_exception();
			ZEPHIR_CPY_WRT(&exception, &_10);
			ZEPHIR_CALL_METHOD(&_11$$6, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_13$$6);
			ZVAL_STRING(&_13$$6, "logger");
			ZEPHIR_CALL_METHOD(&_12$$6, &_11$$6, "get", NULL, 0, &_13$$6);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_14$$6);
			zephir_create_array(&_14$$6, 1, 0);
			ZEPHIR_CALL_METHOD(&_15$$6, &exception, "getmessage", NULL, 0);
			zephir_check_call_status();
			zephir_array_update_string(&_14$$6, SL("message"), &_15$$6, PH_COPY | PH_SEPARATE);
			ZEPHIR_INIT_NVAR(&_13$$6);
			ZVAL_STRING(&_13$$6, "Unknown Packet");
			ZEPHIR_CALL_METHOD(NULL, &_12$$6, "notice", NULL, 0, &_13$$6, &_14$$6);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&_15$$6, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_16$$6);
			zephir_create_array(&_16$$6, 1, 0);
			ZEPHIR_CALL_METHOD(&_17$$6, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(&_13$$6);
			ZVAL_STRING(&_13$$6, "Phalcon\\Config\\Config");
			ZEPHIR_CALL_METHOD(&_18$$6, &_17$$6, "get", NULL, 0, &_13$$6);
			zephir_check_call_status();
			zephir_array_fast_append(&_16$$6, &_18$$6);
			ZEPHIR_INIT_NVAR(&_13$$6);
			ZVAL_STRING(&_13$$6, "TwistersFury\\ChatBot\\Connection\\Packet\\Unknown");
			ZEPHIR_CALL_METHOD(&packet, &_15$$6, "get", NULL, 0, &_13$$6, &_16$$6);
			zephir_check_call_status();
		}
	}
	ZEPHIR_CALL_METHOD(&connection, this_ptr, "getconnectionname", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &packet, "setconnection", NULL, 0, &connection);
	zephir_check_call_status();
	RETURN_CCTOR(&packet);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, getNickname)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("nickname"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, getPacketFactory)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getfactoryclass", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_0, "getshared", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, getFactoryClass)
{
	zval *this_ptr = getThis();



	RETURN_STRING("TwistersFury\\ChatBot\\Irc\\Packet\\Factory");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Driver_AbstractIrc, buildPacket)
{
	zval _1, _4, _5, _7;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message, message_sub, _0, _2, _3, _6, _8;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_7);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(message, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	zephir_create_array(&_1, 1, 0);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 1, 0);
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 2, 0);
	ZEPHIR_CALL_METHOD(&_6, message, "getto", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_5, SL("to"), &_6, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_VAR(&_7);
	zephir_create_array(&_7, 1, 0);
	ZEPHIR_CALL_METHOD(&_6, message, "getmessage", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_long(&_7, 1, &_6, PH_COPY ZEPHIR_DEBUG_PARAMS_DUMMY);
	zephir_array_update_string(&_5, SL("params"), &_7, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_4, &_5);
	ZEPHIR_INIT_VAR(&_8);
	ZVAL_STRING(&_8, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_8, &_4);
	zephir_check_call_status();
	zephir_array_fast_append(&_1, &_3);
	ZEPHIR_INIT_NVAR(&_8);
	ZVAL_STRING(&_8, "TwistersFury\\ChatBot\\Irc\\Packet\\PrivateMessage");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_8, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

