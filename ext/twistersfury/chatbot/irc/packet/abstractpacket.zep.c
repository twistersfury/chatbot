
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/string.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_AbstractPacket)
{
	ZEPHIR_REGISTER_CLASS(TwistersFury\\ChatBot\\Irc\\Packet, AbstractPacket, twistersfury_chatbot, irc_packet_abstractpacket, twistersfury_chatbot_irc_packet_abstractpacket_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_declare_property_string(twistersfury_chatbot_irc_packet_abstractpacket_ce, SL("message"), "", ZEND_ACC_PRIVATE);
	zend_declare_property_string(twistersfury_chatbot_irc_packet_abstractpacket_ce, SL("connectionName"), "irc", ZEND_ACC_PRIVATE);
	zephir_declare_class_constant_long(twistersfury_chatbot_irc_packet_abstractpacket_ce, SL("MESSAGE_PRIVATE"), 0);

	zephir_declare_class_constant_long(twistersfury_chatbot_irc_packet_abstractpacket_ce, SL("MESSAGE_NOTICE"), 1);

	zephir_declare_class_constant_long(twistersfury_chatbot_irc_packet_abstractpacket_ce, SL("MESSAGE_RAW"), 2);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getMethod)
{
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConfig)
{
	zval *this_ptr = getThis();



}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getDi)
{
	zval *this_ptr = getThis();



}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, setMessage)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, _0, _1;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	zephir_get_strval(&message, message_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "message");
	ZEPHIR_CALL_METHOD(NULL, &_0, "set", NULL, 0, &_1, &message);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getReplacements)
{
	zval *this_ptr = getThis();



	zephir_create_array(return_value, 2, 0);
	add_assoc_stringl_ex(return_value, SL("&B"), SL("\x02"));
	add_assoc_stringl_ex(return_value, SL("&C"), SL("\x03"));
	return;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getPiece)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *index_param = NULL, _0, _1, _2;
	zend_long index, ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_LONG(index)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &index_param);
	if (UNEXPECTED(Z_TYPE_P(index_param) != IS_LONG)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'index' must be of the type int"));
		RETURN_MM_NULL();
	}
	index = Z_LVAL_P(index_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("param"), PH_NOISY_CC | PH_READONLY);
	zephir_array_fetch_long(&_2, &_1, index, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/AbstractPacket.zep", 47);
	RETURN_CTOR(&_2);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getParentPacket)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("packet"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConnection)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "connectionName");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, setConnection)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *connection_param = NULL;
	zval connection;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&connection);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(connection)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &connection_param);
	if (UNEXPECTED(Z_TYPE_P(connection_param) != IS_STRING && Z_TYPE_P(connection_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'connection' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(connection_param) == IS_STRING)) {
		zephir_get_strval(&connection, connection_param);
	} else {
		ZEPHIR_INIT_VAR(&connection);
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("connectionName"), &connection);
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getMessage)
{
	zval _0, _1, _2, _3, _4, _10, _11, _12, _5$$3, _6$$3, _7$$4, _8$$4, _9$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_7$$4);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_9$$4);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "message");
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "params");
	ZEPHIR_CALL_METHOD(&_4, &_3, "has", NULL, 0, &_2);
	zephir_check_call_status();
	if (zephir_is_true(&_1)) {
		ZEPHIR_CALL_METHOD(&_5$$3, this_ptr, "getconfig", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_OBS_VAR(&_6$$3);
		zephir_read_property(&_6$$3, &_5$$3, ZEND_STRL("message"), PH_NOISY_CC);
		RETURN_CCTOR(&_6$$3);
	} else if (UNEXPECTED(!zephir_is_true(&_4))) {
		ZEPHIR_CALL_METHOD(&_7$$4, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_9$$4);
		ZVAL_STRING(&_9$$4, "logger");
		ZEPHIR_CALL_METHOD(&_8$$4, &_7$$4, "get", NULL, 0, &_9$$4);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(&_9$$4);
		ZVAL_STRING(&_9$$4, "Missing Message Parameters");
		ZEPHIR_CALL_METHOD(NULL, &_8$$4, "warning", NULL, 0, &_9$$4);
		zephir_check_call_status();
		RETURN_MM_STRING("");
	}
	ZEPHIR_CALL_METHOD(&_10, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_11, &_10, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_12, &_11, "toarray", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, " ");
	zephir_fast_join(return_value, &_2, &_12);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getParameters)
{
	zval *this_ptr = getThis();



	RETURN_STRING("");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getCommandFactory)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "commandFactory");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, isChannel)
{
	zend_bool _3;
	zval _0, _2, _4, _5, _6, _7, _8, _9;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_1 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", &_1, 0);
	zephir_check_call_status();
	zephir_read_property(&_2, &_0, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	_3 = zephir_array_isset_long(&_2, 0);
	if (_3) {
		ZEPHIR_CALL_METHOD(&_4, this_ptr, "getconfig", &_1, 0);
		zephir_check_call_status();
		zephir_read_property(&_5, &_4, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
		zephir_array_fetch_long(&_6, &_5, 0, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/AbstractPacket.zep", 92);
		ZVAL_LONG(&_7, 0);
		ZVAL_LONG(&_8, 1);
		ZEPHIR_INIT_VAR(&_9);
		zephir_substr(&_9, &_6, 0 , 1 , 0);
		_3 = ZEPHIR_IS_STRING_IDENTICAL(&_9, "#");
	}
	RETURN_MM_BOOL(_3);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getFrom)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "ischannel", NULL, 0);
	zephir_check_call_status();
	if (zephir_is_true(&_0)) {
		ZEPHIR_RETURN_CALL_METHOD(this_ptr, "buildchanneluser", NULL, 1);
		zephir_check_call_status();
		RETURN_MM();
	}
	RETURN_MM_NULL();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getFromUser)
{
	zval *this_ptr = getThis();



	RETURN_NULL();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, buildChannelUser)
{
	zval _5, _7, _10, _11;
	zval name, _0, _1, _2, _3, _4, _6, _8, _9;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&name);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_OBS_VAR(&name);
	zephir_array_fetch_long(&name, &_1, 0, PH_NOISY, "twistersfury/chatbot/Irc/Packet/AbstractPacket.zep", 114);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	ZVAL_STRING(&_4, "logger");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_4);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 1, 0);
	zephir_array_update_string(&_5, SL("name"), &name, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_4);
	ZVAL_STRING(&_4, "Build Channel User");
	ZEPHIR_CALL_METHOD(NULL, &_3, "debug", NULL, 0, &_4, &_5);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_7);
	zephir_create_array(&_7, 1, 0);
	ZEPHIR_CALL_METHOD(&_8, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_10);
	zephir_create_array(&_10, 1, 0);
	ZEPHIR_INIT_VAR(&_11);
	zephir_create_array(&_11, 3, 0);
	zephir_array_update_string(&_11, SL("displayName"), &name, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_11, SL("accountName"), &name, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_11, SL("source"), &name, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_10, &_11);
	ZEPHIR_INIT_NVAR(&_4);
	ZVAL_STRING(&_4, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_9, &_8, "get", NULL, 0, &_4, &_10);
	zephir_check_call_status();
	zephir_array_fast_append(&_7, &_9);
	ZEPHIR_INIT_NVAR(&_4);
	ZVAL_STRING(&_4, "TwistersFury\\ChatBot\\Irc\\Packet\\User");
	ZEPHIR_RETURN_CALL_METHOD(&_6, "get", NULL, 0, &_4, &_7);
	zephir_check_call_status();
	RETURN_MM();
}

