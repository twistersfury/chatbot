
extern zend_class_entry *twistersfury_chatbot_irc_packet_abstractpacket_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_AbstractPacket);

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getMethod);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConfig);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getDi);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, setMessage);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getReplacements);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getPiece);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getParentPacket);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConnection);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, setConnection);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getMessage);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getParameters);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getCommandFactory);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, isChannel);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getFrom);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getFromUser);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, buildChannelUser);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getmethod, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getconfig, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getdi, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_setmessage, 0, 1, TwistersFury\\ChatBot\\Irc\\Packet\\AbstractPacket, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getreplacements, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getpiece, 0, 1, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, index, IS_LONG, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getparentpacket, 0, 0, TwistersFury\\ChatBot\\Irc\\Packet\\AbstractPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getconnection, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_setconnection, 0, 1, TwistersFury\\ChatBot\\Irc\\Packet\\AbstractPacket, 0)
	ZEND_ARG_TYPE_INFO(0, connection, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getmessage, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getparameters, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getcommandfactory, 0, 0, TwistersFury\\ChatBot\\Irc\\Packet\\Factory, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_ischannel, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getfrom, 0, 0, TwistersFury\\ChatBot\\Irc\\Packet\\User, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getfromuser, 0, 0, TwistersFury\\ChatBot\\Irc\\Packet\\User, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_abstractpacket_buildchanneluser, 0, 0, TwistersFury\\ChatBot\\Irc\\Packet\\User, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_irc_packet_abstractpacket_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getMethod, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getmethod, ZEND_ACC_ABSTRACT|ZEND_ACC_PROTECTED)
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConfig, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getconfig, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConfig, NULL, ZEND_ACC_PUBLIC)
#endif
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getDi, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getdi, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getDi, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, setMessage, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_setmessage, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getReplacements, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getreplacements, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getPiece, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getpiece, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getParentPacket, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getparentpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getConnection, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getconnection, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, setConnection, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_setconnection, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getMessage, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getmessage, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getParameters, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getparameters, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getCommandFactory, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getcommandfactory, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, isChannel, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_ischannel, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getFrom, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getfrom, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, getFromUser, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_getfromuser, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_AbstractPacket, buildChannelUser, arginfo_twistersfury_chatbot_irc_packet_abstractpacket_buildchanneluser, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
