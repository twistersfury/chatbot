
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Factory)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet, Factory, twistersfury_chatbot, irc_packet_factory, twistersfury_chatbot_connection_packet_abstractfactory_ce, twistersfury_chatbot_irc_packet_factory_method_entry, 0);

	return SUCCESS;
}

/**
 * Returns the adapters for the factory
 */
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Factory, getServices)
{
	zval *this_ptr = getThis();



	zephir_create_array(return_value, 18, 0);
	add_assoc_stringl_ex(return_value, SL("PING"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Ping"));
	add_assoc_stringl_ex(return_value, SL("PONG"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Pong"));
	add_assoc_stringl_ex(return_value, SL("USER"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\User"));
	add_assoc_stringl_ex(return_value, SL("PASS"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Pass"));
	add_assoc_stringl_ex(return_value, SL("NICK"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Nickname"));
	add_assoc_stringl_ex(return_value, SL("NOTICE"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Notice"));
	add_assoc_stringl_ex(return_value, SL("ERROR"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Error"));
	add_assoc_stringl_ex(return_value, SL("QUIT"), SL("TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\QuitPacket"));
	add_assoc_stringl_ex(return_value, SL("PRIVMSG"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\PrivateMessage"));
	add_assoc_stringl_ex(return_value, SL("001"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Welcome"));
	add_assoc_stringl_ex(return_value, SL("002"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\YourHost"));
	add_assoc_stringl_ex(return_value, SL("003"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Created"));
	add_assoc_stringl_ex(return_value, SL("004"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\MyInfo"));
	add_assoc_stringl_ex(return_value, SL("005"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\ISupport"));
	add_assoc_stringl_ex(return_value, SL("MODE"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Mode"));
	add_assoc_stringl_ex(return_value, SL("CAP"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Capability"));
	add_assoc_stringl_ex(return_value, SL("_CHANNEL"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing\\Channel"));
	add_assoc_stringl_ex(return_value, SL("_DIRECT"), SL("TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing\\PrivateMessage"));
	return;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Factory, getInterfaceName)
{
	zval *this_ptr = getThis();



	RETURN_STRING("TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet");
}

