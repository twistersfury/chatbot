
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/concat.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Notice)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet, Notice, twistersfury_chatbot, irc_packet_notice, twistersfury_chatbot_irc_packet_abstractpacket_ce, twistersfury_chatbot_irc_packet_notice_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Notice, getMethod)
{
	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getnickname", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CONCAT_SV(return_value, "NOTICE ", &_0);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Notice, getNickname)
{
	zval *this_ptr = getThis();



	RETURN_STRING("");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Notice, getMessage)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	zephir_array_fetch_long(&_2, &_1, 1, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/Notice.zep", 17);
	RETURN_CTOR(&_2);
}

