
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/string.h"
#include "kernel/memory.h"
#include "kernel/array.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/concat.h"
#include "kernel/operators.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing, AbstractOutgoing, twistersfury_chatbot, irc_packet_outgoing_abstractoutgoing, twistersfury_chatbot_irc_packet_abstractpacket_ce, twistersfury_chatbot_irc_packet_outgoing_abstractoutgoing_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_class_implements(twistersfury_chatbot_irc_packet_outgoing_abstractoutgoing_ce, 1, twistersfury_chatbot_connection_throttle_interfaces_throttledpacket_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing, getMethod)
{
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing, buildPacket)
{
	zval _0, _1, _2, _3, _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getreplacements", NULL, 0);
	zephir_check_call_status();
	zephir_array_keys(&_0, &_1);
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getreplacements", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "buildrawpacket", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_4);
	zephir_fast_str_replace(&_4, &_0, &_2, &_3);
	RETURN_CCTOR(&_4);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing, buildRawPacket)
{
	zval _0, _1, _2, _3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getmethod", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getto", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getparameters", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getmessage", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CONCAT_VSVSVSV(return_value, &_0, " ", &_1, " ", &_2, " :", &_3);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing, getTo)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("to"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_AbstractOutgoing, setTo)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *to_param = NULL, _0, _1;
	zval to;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&to);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(to)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &to_param);
	zephir_get_strval(&to, to_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "to");
	ZEPHIR_CALL_METHOD(NULL, &_0, "set", NULL, 0, &_1, &to);
	zephir_check_call_status();
	RETURN_THIS();
}

