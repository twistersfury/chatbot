
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Outgoing_Channel)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing, Channel, twistersfury_chatbot, irc_packet_outgoing_channel, twistersfury_chatbot_irc_packet_outgoing_abstractoutgoing_ce, twistersfury_chatbot_irc_packet_outgoing_channel_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_Channel, getMethod)
{
	zval *this_ptr = getThis();



	RETURN_STRING("PRIVMSG");
}

