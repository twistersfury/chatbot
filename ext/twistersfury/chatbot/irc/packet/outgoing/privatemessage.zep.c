
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Outgoing_PrivateMessage)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing, PrivateMessage, twistersfury_chatbot, irc_packet_outgoing_privatemessage, twistersfury_chatbot_irc_packet_outgoing_abstractoutgoing_ce, twistersfury_chatbot_irc_packet_outgoing_privatemessage_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Outgoing_PrivateMessage, getMethod)
{
	zval *this_ptr = getThis();



	RETURN_STRING("PRIVMSG");
}

