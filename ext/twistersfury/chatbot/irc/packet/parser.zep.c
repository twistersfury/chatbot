
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/string.h"
#include "kernel/operators.h"
#include "kernel/concat.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Parser)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet, Parser, twistersfury_chatbot, irc_packet_parser, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_irc_packet_parser_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parse)
{
	zval _29, _30;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, _0, _1, _2, _3, _4, _5, _13, _14, _15, _23, _24, _25, _27, _28, _31, _32, _6$$3, _7$$3, _8$$3, _9$$3, _10$$3, _12$$3, _16$$4, _17$$4, _18$$4, _19$$4, _20$$4, _22$$4;
	zval message, tags, prefix, command, params, _26, _11$$3, _21$$4;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&tags);
	ZVAL_UNDEF(&prefix);
	ZVAL_UNDEF(&command);
	ZVAL_UNDEF(&params);
	ZVAL_UNDEF(&_26);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_21$$4);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_13);
	ZVAL_UNDEF(&_14);
	ZVAL_UNDEF(&_15);
	ZVAL_UNDEF(&_23);
	ZVAL_UNDEF(&_24);
	ZVAL_UNDEF(&_25);
	ZVAL_UNDEF(&_27);
	ZVAL_UNDEF(&_28);
	ZVAL_UNDEF(&_31);
	ZVAL_UNDEF(&_32);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_9$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_12$$3);
	ZVAL_UNDEF(&_16$$4);
	ZVAL_UNDEF(&_17$$4);
	ZVAL_UNDEF(&_18$$4);
	ZVAL_UNDEF(&_19$$4);
	ZVAL_UNDEF(&_20$$4);
	ZVAL_UNDEF(&_22$$4);
	ZVAL_UNDEF(&_29);
	ZVAL_UNDEF(&_30);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	if (UNEXPECTED(Z_TYPE_P(message_param) != IS_STRING && Z_TYPE_P(message_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'message' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(message_param) == IS_STRING)) {
		zephir_get_strval(&message, message_param);
	} else {
		ZEPHIR_INIT_VAR(&message);
	}


	ZVAL_LONG(&_0, 0);
	ZVAL_LONG(&_1, -2);
	ZEPHIR_INIT_VAR(&_2);
	zephir_substr(&_2, &message, 0 , -2 , 0);
	zephir_get_strval(&message, &_2);
	ZEPHIR_INIT_VAR(&tags);
	ZEPHIR_INIT_VAR(&prefix);
	ZEPHIR_INIT_VAR(&command);
	ZEPHIR_INIT_VAR(&params);
	ZVAL_LONG(&_3, 0);
	ZVAL_LONG(&_4, 1);
	ZEPHIR_INIT_VAR(&_5);
	zephir_substr(&_5, &message, 0 , 1 , 0);
	if (UNEXPECTED(ZEPHIR_IS_STRING_IDENTICAL(&_5, "@"))) {
		ZEPHIR_INIT_VAR(&_6$$3);
		ZVAL_STRING(&_6$$3, " ");
		ZEPHIR_INIT_VAR(&_7$$3);
		zephir_fast_strpos(&_7$$3, &message, &_6$$3, 0 );
		ZVAL_LONG(&_8$$3, 1);
		ZVAL_LONG(&_9$$3, (zephir_get_numberval(&_7$$3) - 1));
		ZEPHIR_INIT_NVAR(&tags);
		zephir_substr(&tags, &message, 1 , zephir_get_intval(&_9$$3), 0);
		ZEPHIR_INIT_VAR(&_10$$3);
		ZEPHIR_INIT_VAR(&_11$$3);
		ZEPHIR_CONCAT_SVS(&_11$$3, "@", &tags, " ");
		ZEPHIR_INIT_VAR(&_12$$3);
		ZVAL_STRING(&_12$$3, "");
		zephir_fast_str_replace(&_10$$3, &_11$$3, &_12$$3, &message);
		zephir_get_strval(&message, &_10$$3);
	}
	ZVAL_LONG(&_13, 0);
	ZVAL_LONG(&_14, 1);
	ZEPHIR_INIT_VAR(&_15);
	zephir_substr(&_15, &message, 0 , 1 , 0);
	if (EXPECTED(ZEPHIR_IS_STRING_IDENTICAL(&_15, ":"))) {
		ZEPHIR_INIT_VAR(&_16$$4);
		ZVAL_STRING(&_16$$4, " ");
		ZEPHIR_INIT_VAR(&_17$$4);
		zephir_fast_strpos(&_17$$4, &message, &_16$$4, 0 );
		ZVAL_LONG(&_18$$4, 1);
		ZVAL_LONG(&_19$$4, (zephir_get_numberval(&_17$$4) - 1));
		ZEPHIR_INIT_NVAR(&prefix);
		zephir_substr(&prefix, &message, 1 , zephir_get_intval(&_19$$4), 0);
		ZEPHIR_INIT_VAR(&_20$$4);
		ZEPHIR_INIT_VAR(&_21$$4);
		ZEPHIR_CONCAT_SVS(&_21$$4, ":", &prefix, " ");
		ZEPHIR_INIT_VAR(&_22$$4);
		ZVAL_STRING(&_22$$4, "");
		zephir_fast_str_replace(&_20$$4, &_21$$4, &_22$$4, &message);
		zephir_get_strval(&message, &_20$$4);
	}
	ZEPHIR_INIT_VAR(&_23);
	ZVAL_STRING(&_23, " ");
	ZEPHIR_INIT_VAR(&_24);
	zephir_fast_strpos(&_24, &message, &_23, 0 );
	ZVAL_LONG(&_25, 0);
	ZEPHIR_INIT_NVAR(&command);
	zephir_substr(&command, &message, 0 , zephir_get_intval(&_24), 0);
	ZEPHIR_INIT_VAR(&_26);
	ZEPHIR_CONCAT_VS(&_26, &command, " ");
	ZEPHIR_INIT_VAR(&_27);
	ZVAL_STRING(&_27, "");
	ZEPHIR_INIT_NVAR(&params);
	zephir_fast_str_replace(&params, &_26, &_27, &message);
	ZEPHIR_CALL_METHOD(&_28, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_29);
	zephir_create_array(&_29, 1, 0);
	ZEPHIR_INIT_VAR(&_30);
	zephir_create_array(&_30, 4, 0);
	zephir_array_update_string(&_30, SL("adapter"), &command, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_31, this_ptr, "parseprefix", NULL, 31, &prefix);
	zephir_check_call_status();
	zephir_array_update_string(&_30, SL("user"), &_31, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_31, this_ptr, "parseparams", NULL, 32, &params);
	zephir_check_call_status();
	zephir_array_update_string(&_30, SL("params"), &_31, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_31, this_ptr, "parsetags", NULL, 33, &tags);
	zephir_check_call_status();
	zephir_array_update_string(&_30, SL("tags"), &_31, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_29, &_30);
	ZEPHIR_INIT_VAR(&_32);
	ZVAL_STRING(&_32, "Phalcon\\Config\\Config");
	ZEPHIR_RETURN_CALL_METHOD(&_28, "get", NULL, 0, &_32, &_29);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parsePrefix)
{
	zval _5, _8, _9;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *prefix_param = NULL, _0, pieces, namePieces, source, accountName, displayName, _1, _4, _6, _7, _10, _2$$5, _3$$5;
	zval prefix;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&prefix);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&pieces);
	ZVAL_UNDEF(&namePieces);
	ZVAL_UNDEF(&source);
	ZVAL_UNDEF(&accountName);
	ZVAL_UNDEF(&displayName);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_2$$5);
	ZVAL_UNDEF(&_3$$5);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(prefix)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &prefix_param);
	if (UNEXPECTED(Z_TYPE_P(prefix_param) != IS_STRING && Z_TYPE_P(prefix_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'prefix' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(prefix_param) == IS_STRING)) {
		zephir_get_strval(&prefix, prefix_param);
	} else {
		ZEPHIR_INIT_VAR(&prefix);
	}


	if (zephir_fast_strlen_ev(&prefix) == 0) {
		RETURN_MM_BOOL(0);
	}
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "@");
	ZEPHIR_INIT_VAR(&pieces);
	zephir_fast_explode(&pieces, &_1, &prefix, 2 );
	ZEPHIR_OBS_VAR(&source);
	if (!(zephir_array_isset_long_fetch(&source, &pieces, 1, 0))) {
		ZEPHIR_OBS_NVAR(&source);
		zephir_array_fetch_long(&source, &pieces, 0, PH_NOISY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 58);
	} else {
		zephir_array_fetch_long(&_2$$5, &pieces, 0, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 60);
		ZEPHIR_INIT_VAR(&_3$$5);
		ZVAL_STRING(&_3$$5, "!");
		ZEPHIR_INIT_VAR(&namePieces);
		zephir_fast_explode(&namePieces, &_3$$5, &_2$$5, 2 );
		ZEPHIR_OBS_VAR(&displayName);
		zephir_array_fetch_long(&displayName, &namePieces, 0, PH_NOISY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 61);
		ZEPHIR_OBS_VAR(&accountName);
		if (!(zephir_array_isset_long_fetch(&accountName, &namePieces, 1, 0))) {
			ZEPHIR_CPY_WRT(&accountName, &displayName);
		}
	}
	ZEPHIR_CALL_METHOD(&_4, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_5);
	zephir_create_array(&_5, 1, 0);
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_8);
	zephir_create_array(&_8, 1, 0);
	ZEPHIR_INIT_VAR(&_9);
	zephir_create_array(&_9, 3, 0);
	zephir_array_update_string(&_9, SL("displayName"), &displayName, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_9, SL("accountName"), &accountName, PH_COPY | PH_SEPARATE);
	zephir_array_update_string(&_9, SL("source"), &source, PH_COPY | PH_SEPARATE);
	zephir_array_fast_append(&_8, &_9);
	ZEPHIR_INIT_VAR(&_10);
	ZVAL_STRING(&_10, "Phalcon\\Config\\Config");
	ZEPHIR_CALL_METHOD(&_7, &_6, "get", NULL, 0, &_10, &_8);
	zephir_check_call_status();
	zephir_array_fast_append(&_5, &_7);
	ZEPHIR_INIT_NVAR(&_10);
	ZVAL_STRING(&_10, "TwistersFury\\ChatBot\\Irc\\Packet\\User");
	ZEPHIR_RETURN_CALL_METHOD(&_4, "get", NULL, 0, &_10, &_5);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parseParams)
{
	zend_bool isLast;
	zval parsedParams;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS, index;
	zval *params_param = NULL, sections, section, *_0, _1, _2$$4, _3$$4, _4$$4, _5$$5, _6$$5, _7$$6, _8$$6, _9$$8, _10$$8, _11$$8, _12$$9, _13$$9, _14$$10, _15$$10;
	zval params;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&params);
	ZVAL_UNDEF(&sections);
	ZVAL_UNDEF(&section);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2$$4);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&_4$$4);
	ZVAL_UNDEF(&_5$$5);
	ZVAL_UNDEF(&_6$$5);
	ZVAL_UNDEF(&_7$$6);
	ZVAL_UNDEF(&_8$$6);
	ZVAL_UNDEF(&_9$$8);
	ZVAL_UNDEF(&_10$$8);
	ZVAL_UNDEF(&_11$$8);
	ZVAL_UNDEF(&_12$$9);
	ZVAL_UNDEF(&_13$$9);
	ZVAL_UNDEF(&_14$$10);
	ZVAL_UNDEF(&_15$$10);
	ZVAL_UNDEF(&parsedParams);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(params)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &params_param);
	zephir_get_strval(&params, params_param);


	if (UNEXPECTED(ZEPHIR_IS_NULL(&params))) {
		array_init(return_value);
		RETURN_MM();
	}
	ZEPHIR_INIT_VAR(&sections);
	zephir_fast_explode_str(&sections, SL(" "), &params, LONG_MAX);
	ZEPHIR_INIT_VAR(&parsedParams);
	array_init(&parsedParams);
	index = 0;
	isLast = 0;
	zephir_is_iterable(&sections, 0, "twistersfury/chatbot/Irc/Packet/Parser.zep", 109);
	if (Z_TYPE_P(&sections) == IS_ARRAY) {
		ZEND_HASH_FOREACH_VAL(Z_ARRVAL_P(&sections), _0)
		{
			ZEPHIR_INIT_NVAR(&section);
			ZVAL_COPY(&section, _0);
			ZVAL_LONG(&_2$$4, 0);
			ZVAL_LONG(&_3$$4, 1);
			ZEPHIR_INIT_NVAR(&_4$$4);
			zephir_substr(&_4$$4, &section, 0 , 1 , 0);
			if (ZEPHIR_IS_STRING_IDENTICAL(&_4$$4, ":")) {
				isLast = 1;
				ZVAL_LONG(&_5$$5, 1);
				ZEPHIR_INIT_NVAR(&_6$$5);
				zephir_substr(&_6$$5, &section, 1 , 0, ZEPHIR_SUBSTR_NO_LENGTH);
				ZEPHIR_CPY_WRT(&section, &_6$$5);
			} else if (isLast == 1) {
				zephir_array_fetch_long(&_7$$6, &parsedParams, index, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 99);
				ZEPHIR_INIT_NVAR(&_8$$6);
				ZEPHIR_CONCAT_VSV(&_8$$6, &_7$$6, " ", &section);
				ZEPHIR_CPY_WRT(&section, &_8$$6);
			}
			zephir_array_update_long(&parsedParams, index, &section, PH_COPY | PH_SEPARATE ZEPHIR_DEBUG_PARAMS_DUMMY);
			if (!(isLast)) {
				index = (index + 1);
			}
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, &sections, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_1, &sections, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_1)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&section, &sections, "current", NULL, 0);
			zephir_check_call_status();
				ZVAL_LONG(&_9$$8, 0);
				ZVAL_LONG(&_10$$8, 1);
				ZEPHIR_INIT_NVAR(&_11$$8);
				zephir_substr(&_11$$8, &section, 0 , 1 , 0);
				if (ZEPHIR_IS_STRING_IDENTICAL(&_11$$8, ":")) {
					isLast = 1;
					ZVAL_LONG(&_12$$9, 1);
					ZEPHIR_INIT_NVAR(&_13$$9);
					zephir_substr(&_13$$9, &section, 1 , 0, ZEPHIR_SUBSTR_NO_LENGTH);
					ZEPHIR_CPY_WRT(&section, &_13$$9);
				} else if (isLast == 1) {
					zephir_array_fetch_long(&_14$$10, &parsedParams, index, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 99);
					ZEPHIR_INIT_NVAR(&_15$$10);
					ZEPHIR_CONCAT_VSV(&_15$$10, &_14$$10, " ", &section);
					ZEPHIR_CPY_WRT(&section, &_15$$10);
				}
				zephir_array_update_long(&parsedParams, index, &section, PH_COPY | PH_SEPARATE ZEPHIR_DEBUG_PARAMS_DUMMY);
				if (!(isLast)) {
					index = (index + 1);
				}
			ZEPHIR_CALL_METHOD(NULL, &sections, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&section);
	RETURN_CTOR(&parsedParams);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parseTags)
{
	zval tagList;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *tags_param = NULL, rawTags, tagValue, tag, splitTag, *_0, _1, _2$$4, _3$$6;
	zval tags;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&tags);
	ZVAL_UNDEF(&rawTags);
	ZVAL_UNDEF(&tagValue);
	ZVAL_UNDEF(&tag);
	ZVAL_UNDEF(&splitTag);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2$$4);
	ZVAL_UNDEF(&_3$$6);
	ZVAL_UNDEF(&tagList);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(tags)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &tags_param);
	zephir_get_strval(&tags, tags_param);


	if (EXPECTED(ZEPHIR_IS_NULL(&tags))) {
		array_init(return_value);
		RETURN_MM();
	}
	ZEPHIR_INIT_VAR(&rawTags);
	zephir_fast_explode_str(&rawTags, SL(";"), &tags, LONG_MAX);
	ZEPHIR_INIT_VAR(&tagList);
	array_init(&tagList);
	zephir_is_iterable(&rawTags, 0, "twistersfury/chatbot/Irc/Packet/Parser.zep", 132);
	if (Z_TYPE_P(&rawTags) == IS_ARRAY) {
		ZEND_HASH_FOREACH_VAL(Z_ARRVAL_P(&rawTags), _0)
		{
			ZEPHIR_INIT_NVAR(&tag);
			ZVAL_COPY(&tag, _0);
			ZEPHIR_INIT_NVAR(&splitTag);
			zephir_fast_explode_str(&splitTag, SL("="), &tag, LONG_MAX);
			ZEPHIR_OBS_NVAR(&tagValue);
			if (!(zephir_array_isset_long_fetch(&tagValue, &splitTag, 1, 0))) {
				ZEPHIR_INIT_NVAR(&tagValue);
				ZVAL_BOOL(&tagValue, 1);
			}
			ZEPHIR_OBS_NVAR(&_2$$4);
			zephir_array_fetch_long(&_2$$4, &splitTag, 0, PH_NOISY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 129);
			zephir_array_update_zval(&tagList, &_2$$4, &tagValue, PH_COPY | PH_SEPARATE);
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, &rawTags, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_1, &rawTags, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_1)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&tag, &rawTags, "current", NULL, 0);
			zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&splitTag);
				zephir_fast_explode_str(&splitTag, SL("="), &tag, LONG_MAX);
				ZEPHIR_OBS_NVAR(&tagValue);
				if (!(zephir_array_isset_long_fetch(&tagValue, &splitTag, 1, 0))) {
					ZEPHIR_INIT_NVAR(&tagValue);
					ZVAL_BOOL(&tagValue, 1);
				}
				ZEPHIR_OBS_NVAR(&_3$$6);
				zephir_array_fetch_long(&_3$$6, &splitTag, 0, PH_NOISY, "twistersfury/chatbot/Irc/Packet/Parser.zep", 129);
				zephir_array_update_zval(&tagList, &_3$$6, &tagValue, PH_COPY | PH_SEPARATE);
			ZEPHIR_CALL_METHOD(NULL, &rawTags, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&tag);
	RETURN_CTOR(&tagList);
}

