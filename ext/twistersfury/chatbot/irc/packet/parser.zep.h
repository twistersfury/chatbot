
extern zend_class_entry *twistersfury_chatbot_irc_packet_parser_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Parser);

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parse);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parsePrefix);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parseParams);
PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Parser, parseTags);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_parser_parse, 0, 1, Phalcon\\Config\\Config, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_parser_parseprefix, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, prefix, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_parser_parseparams, 0, 1, IS_ARRAY, 0)
	ZEND_ARG_TYPE_INFO(0, params, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_parser_parsetags, 0, 1, IS_ARRAY, 0)
	ZEND_ARG_TYPE_INFO(0, tags, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_irc_packet_parser_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_Parser, parse, arginfo_twistersfury_chatbot_irc_packet_parser_parse, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_Parser, parsePrefix, arginfo_twistersfury_chatbot_irc_packet_parser_parseprefix, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_Parser, parseParams, arginfo_twistersfury_chatbot_irc_packet_parser_parseparams, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Irc_Packet_Parser, parseTags, arginfo_twistersfury_chatbot_irc_packet_parser_parsetags, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
