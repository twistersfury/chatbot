
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "kernel/concat.h"
#include "kernel/string.h"
#include "kernel/operators.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_PrivateMessage)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet, PrivateMessage, twistersfury_chatbot, irc_packet_privatemessage, twistersfury_chatbot_irc_packet_abstractpacket_ce, twistersfury_chatbot_irc_packet_privatemessage_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_irc_packet_privatemessage_ce, SL("eventsManager"), ZEND_ACC_PRIVATE);
	zend_class_implements(twistersfury_chatbot_irc_packet_privatemessage_ce, 1, twistersfury_chatbot_connection_packet_interfaces_message_ce);
	zend_class_implements(twistersfury_chatbot_irc_packet_privatemessage_ce, 1, twistersfury_chatbot_connection_packet_interfaces_hascommands_ce);
	zend_class_implements(twistersfury_chatbot_irc_packet_privatemessage_ce, 1, twistersfury_chatbot_connection_packet_interfaces_hasevent_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, setEventsManager)
{
	zval *manager, manager_sub;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&manager_sub);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(manager, zephir_get_internal_ce(SL("phalcon\\events\\managerinterface")))
	ZEND_PARSE_PARAMETERS_END();
#endif


	zephir_fetch_params_without_memory_grow(1, 0, &manager);


	zephir_update_property_zval(this_ptr, ZEND_STRL("eventsManager"), manager);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, getEventsManager)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "eventsManager");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, getEventName)
{
	zval *this_ptr = getThis();



	RETURN_STRING("packet:onincoming");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, getMethod)
{
	zval *this_ptr = getThis();



	RETURN_STRING("PRIVMSG");
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, getQueueOptions)
{
	zval *this_ptr = getThis();



	array_init(return_value);
	return;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, getMessage)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	zephir_array_fetch_long(&_2, &_1, 1, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/PrivateMessage.zep", 39);
	RETURN_CTOR(&_2);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, getParameters)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("to"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CONCAT_SVS(return_value, " ", &_1, " ");
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, buildCommands)
{
	zval _0, _1, _2, _3, _4, _5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Building Commands");
	ZEPHIR_CALL_METHOD(NULL, &_0, "debug", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getmessage", NULL, 0);
	zephir_check_call_status();
	ZVAL_LONG(&_3, 0);
	ZVAL_LONG(&_4, 1);
	ZEPHIR_INIT_NVAR(&_1);
	zephir_substr(&_1, &_2, 0 , 1 , 0);
	if (EXPECTED(!ZEPHIR_IS_STRING_IDENTICAL(&_1, "!"))) {
		array_init(return_value);
		RETURN_MM();
	}
	zephir_create_array(return_value, 1, 0);
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "buildcommand", NULL, 34);
	zephir_check_call_status();
	zephir_array_fast_append(return_value, &_5);
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, buildCommand)
{
	zval _0, _1, job, _2, _3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&job);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);


	ZEPHIR_MM_GROW();

	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Building Command");
	ZEPHIR_CALL_METHOD(NULL, &_0, "debug", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getcommandfactory", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "parsemessage", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&job, &_2, "load", NULL, 0, &_3);
	zephir_check_call_status();
	RETURN_CCTOR(&job);
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_PrivateMessage, parseMessage)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "commandParser");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_1, "parse", NULL, 0, this_ptr);
	zephir_check_call_status();
	RETURN_MM();
}

