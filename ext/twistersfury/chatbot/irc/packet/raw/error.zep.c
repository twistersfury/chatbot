
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Raw_Error)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet\\Raw, Error, twistersfury_chatbot, irc_packet_raw_error, twistersfury_chatbot_irc_packet_raw_ce, twistersfury_chatbot_irc_packet_raw_error_method_entry, 0);

	zend_class_implements(twistersfury_chatbot_irc_packet_raw_error_ce, 1, twistersfury_chatbot_connection_packet_errorinterface_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Raw_Error, getMessage)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	zephir_array_fetch_long(&_2, &_1, 0, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/Raw/Error.zep", 10);
	RETURN_CTOR(&_2);
}

