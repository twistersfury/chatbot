
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_Raw_Ping)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet\\Raw, Ping, twistersfury_chatbot, irc_packet_raw_ping, twistersfury_chatbot_connection_packet_keepalive_ce, twistersfury_chatbot_irc_packet_raw_ping_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_irc_packet_raw_ping_ce, SL("connection"), ZEND_ACC_PRIVATE);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Raw_Ping, setConnection)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *connection_param = NULL;
	zval connection;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&connection);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(connection)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &connection_param);
	if (UNEXPECTED(Z_TYPE_P(connection_param) != IS_STRING && Z_TYPE_P(connection_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'connection' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(connection_param) == IS_STRING)) {
		zephir_get_strval(&connection, connection_param);
	} else {
		ZEPHIR_INIT_VAR(&connection);
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("connection"), &connection);
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Irc_Packet_Raw_Ping, getSlug)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_1, &_0, ZEND_STRL("params"), PH_NOISY_CC | PH_READONLY);
	zephir_array_fetch_long(&_2, &_1, 0, PH_NOISY | PH_READONLY, "twistersfury/chatbot/Irc/Packet/Raw/Ping.zep", 16);
	RETURN_CTOR(&_2);
}

