
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"


ZEPHIR_INIT_CLASS(Twistersfury_ChatBot_Irc_Packet_Server_Capability)
{
	ZEPHIR_REGISTER_CLASS_EX(Twistersfury\\ChatBot\\Irc\\Packet\\Server, Capability, twistersfury_chatbot, irc_packet_server_capability, twistersfury_chatbot_irc_packet_raw_ce, twistersfury_chatbot_irc_packet_server_capability_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Capability, getMethod)
{
	zval *this_ptr = getThis();



	RETURN_STRING("CAP");
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Capability, buildPacket)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();



	ZEPHIR_MM_GROW();

	ZEPHIR_RETURN_CALL_METHOD(this_ptr, "getcapability", NULL, 0);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Capability, getCapability)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_1);
	zephir_read_property(&_1, &_0, ZEND_STRL("capability"), PH_NOISY_CC);
	RETURN_CCTOR(&_1);
}

