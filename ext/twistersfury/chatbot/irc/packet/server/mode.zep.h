
extern zend_class_entry *twistersfury_chatbot_irc_packet_server_mode_ce;

ZEPHIR_INIT_CLASS(Twistersfury_ChatBot_Irc_Packet_Server_Mode);

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Mode, getMethod);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_server_mode_getmethod, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_irc_packet_server_mode_method_entry) {
	PHP_ME(Twistersfury_ChatBot_Irc_Packet_Server_Mode, getMethod, arginfo_twistersfury_chatbot_irc_packet_server_mode_getmethod, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
