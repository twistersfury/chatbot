
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"


ZEPHIR_INIT_CLASS(Twistersfury_ChatBot_Irc_Packet_Server_Welcome)
{
	ZEPHIR_REGISTER_CLASS_EX(Twistersfury\\ChatBot\\Irc\\Packet\\Server, Welcome, twistersfury_chatbot, irc_packet_server_welcome, twistersfury_chatbot_irc_packet_abstractpacket_ce, twistersfury_chatbot_irc_packet_server_welcome_method_entry, 0);

	zend_class_implements(twistersfury_chatbot_irc_packet_server_welcome_ce, 1, twistersfury_chatbot_connection_packet_interfaces_hascommands_ce);
	return SUCCESS;
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, getMethod)
{
	zval *this_ptr = getThis();



	RETURN_STRING("001");
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, getEventName)
{
	zval *this_ptr = getThis();



	RETURN_STRING("packet:onWelcome");
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, buildCommands)
{
	zval commands, _13$$3, _16$$3, _17$$3, _21$$4, _24$$4, _25$$4;
	zval channel, _0, _1, _2, _3, _4, _5, _6, _7, _8, *_9, _10, _11$$3, _12$$3, _14$$3, _15$$3, _18$$3, _19$$4, _20$$4, _22$$4, _23$$4, _26$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&channel);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_12$$3);
	ZVAL_UNDEF(&_14$$3);
	ZVAL_UNDEF(&_15$$3);
	ZVAL_UNDEF(&_18$$3);
	ZVAL_UNDEF(&_19$$4);
	ZVAL_UNDEF(&_20$$4);
	ZVAL_UNDEF(&_22$$4);
	ZVAL_UNDEF(&_23$$4);
	ZVAL_UNDEF(&_26$$4);
	ZVAL_UNDEF(&commands);
	ZVAL_UNDEF(&_13$$3);
	ZVAL_UNDEF(&_16$$3);
	ZVAL_UNDEF(&_17$$3);
	ZVAL_UNDEF(&_21$$4);
	ZVAL_UNDEF(&_24$$4);
	ZVAL_UNDEF(&_25$$4);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&commands);
	array_init(&commands);
	ZEPHIR_OBS_VAR(&_0);
	zephir_read_property(&_0, this_ptr, ZEND_STRL("logger"), PH_NOISY_CC);
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "Welcome Packet");
	ZEPHIR_CALL_METHOD(NULL, &_0, "debug", NULL, 0, &_1);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_1);
	ZVAL_STRING(&_1, "config");
	ZEPHIR_CALL_METHOD(&_3, &_2, "get", NULL, 0, &_1);
	zephir_check_call_status();
	zephir_read_property(&_4, &_3, ZEND_STRL("connection"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getconnection", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, &_4, "get", NULL, 0, &_6);
	zephir_check_call_status();
	zephir_read_property(&_7, &_5, ZEND_STRL("channels"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_8, &_7, "getiterator", NULL, 0);
	zephir_check_call_status();
	zephir_is_iterable(&_8, 0, "twistersfury/chatbot/Irc/Packet/Server/Welcome.zep", 41);
	if (Z_TYPE_P(&_8) == IS_ARRAY) {
		ZEND_HASH_FOREACH_VAL(Z_ARRVAL_P(&_8), _9)
		{
			ZEPHIR_INIT_NVAR(&channel);
			ZVAL_COPY(&channel, _9);
			ZEPHIR_CALL_METHOD(&_11$$3, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(&_13$$3);
			zephir_create_array(&_13$$3, 1, 0);
			ZEPHIR_CALL_METHOD(&_14$$3, this_ptr, "getdi", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(&_16$$3);
			zephir_create_array(&_16$$3, 1, 0);
			ZEPHIR_INIT_NVAR(&_17$$3);
			zephir_create_array(&_17$$3, 2, 0);
			zephir_array_update_string(&_17$$3, SL("channel"), &channel, PH_COPY | PH_SEPARATE);
			zephir_array_update_string(&_17$$3, SL("packet"), this_ptr, PH_COPY | PH_SEPARATE);
			zephir_array_fast_append(&_16$$3, &_17$$3);
			ZEPHIR_INIT_NVAR(&_18$$3);
			ZVAL_STRING(&_18$$3, "Phalcon\\Config\\Config");
			ZEPHIR_CALL_METHOD(&_15$$3, &_14$$3, "get", NULL, 0, &_18$$3, &_16$$3);
			zephir_check_call_status();
			zephir_array_fast_append(&_13$$3, &_15$$3);
			ZEPHIR_INIT_NVAR(&_18$$3);
			ZVAL_STRING(&_18$$3, "TwistersFury\\ChatBot\\Command\\Join");
			ZEPHIR_CALL_METHOD(&_12$$3, &_11$$3, "get", NULL, 0, &_18$$3, &_13$$3);
			zephir_check_call_status();
			zephir_array_append(&commands, &_12$$3, PH_SEPARATE, "twistersfury/chatbot/Irc/Packet/Server/Welcome.zep", 38);
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, &_8, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_10, &_8, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_10)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&channel, &_8, "current", NULL, 0);
			zephir_check_call_status();
				ZEPHIR_CALL_METHOD(&_19$$4, this_ptr, "getdi", NULL, 0);
				zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&_21$$4);
				zephir_create_array(&_21$$4, 1, 0);
				ZEPHIR_CALL_METHOD(&_22$$4, this_ptr, "getdi", NULL, 0);
				zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&_24$$4);
				zephir_create_array(&_24$$4, 1, 0);
				ZEPHIR_INIT_NVAR(&_25$$4);
				zephir_create_array(&_25$$4, 2, 0);
				zephir_array_update_string(&_25$$4, SL("channel"), &channel, PH_COPY | PH_SEPARATE);
				zephir_array_update_string(&_25$$4, SL("packet"), this_ptr, PH_COPY | PH_SEPARATE);
				zephir_array_fast_append(&_24$$4, &_25$$4);
				ZEPHIR_INIT_NVAR(&_26$$4);
				ZVAL_STRING(&_26$$4, "Phalcon\\Config\\Config");
				ZEPHIR_CALL_METHOD(&_23$$4, &_22$$4, "get", NULL, 0, &_26$$4, &_24$$4);
				zephir_check_call_status();
				zephir_array_fast_append(&_21$$4, &_23$$4);
				ZEPHIR_INIT_NVAR(&_26$$4);
				ZVAL_STRING(&_26$$4, "TwistersFury\\ChatBot\\Command\\Join");
				ZEPHIR_CALL_METHOD(&_20$$4, &_19$$4, "get", NULL, 0, &_26$$4, &_21$$4);
				zephir_check_call_status();
				zephir_array_append(&commands, &_20$$4, PH_SEPARATE, "twistersfury/chatbot/Irc/Packet/Server/Welcome.zep", 38);
			ZEPHIR_CALL_METHOD(NULL, &_8, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&channel);
	RETURN_CTOR(&commands);
}

