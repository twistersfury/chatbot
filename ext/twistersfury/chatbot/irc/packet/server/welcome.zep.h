
extern zend_class_entry *twistersfury_chatbot_irc_packet_server_welcome_ce;

ZEPHIR_INIT_CLASS(Twistersfury_ChatBot_Irc_Packet_Server_Welcome);

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, getMethod);
PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, getEventName);
PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, buildCommands);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_server_welcome_getmethod, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_server_welcome_geteventname, 0, 0, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_irc_packet_server_welcome_buildcommands, 0, 0, IS_ARRAY, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_irc_packet_server_welcome_method_entry) {
	PHP_ME(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, getMethod, arginfo_twistersfury_chatbot_irc_packet_server_welcome_getmethod, ZEND_ACC_PROTECTED)
	PHP_ME(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, getEventName, arginfo_twistersfury_chatbot_irc_packet_server_welcome_geteventname, ZEND_ACC_PUBLIC)
	PHP_ME(Twistersfury_ChatBot_Irc_Packet_Server_Welcome, buildCommands, arginfo_twistersfury_chatbot_irc_packet_server_welcome_buildcommands, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
