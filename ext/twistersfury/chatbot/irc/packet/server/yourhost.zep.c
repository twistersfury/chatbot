
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(Twistersfury_ChatBot_Irc_Packet_Server_YourHost)
{
	ZEPHIR_REGISTER_CLASS_EX(Twistersfury\\ChatBot\\Irc\\Packet\\Server, YourHost, twistersfury_chatbot, irc_packet_server_yourhost, twistersfury_chatbot_irc_packet_raw_ce, twistersfury_chatbot_irc_packet_server_yourhost_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(Twistersfury_ChatBot_Irc_Packet_Server_YourHost, getMethod)
{
	zval *this_ptr = getThis();



	RETURN_STRING("002");
}

