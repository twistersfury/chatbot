
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Irc_Packet_User)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Irc\\Packet, User, twistersfury_chatbot, irc_packet_user, twistersfury_chatbot_connection_packet_user_ce, NULL, 0);

	return SUCCESS;
}

