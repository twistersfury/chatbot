
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Job_RegisterListener)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Job, RegisterListener, twistersfury_chatbot, job_registerlistener, zephir_get_internal_ce(SL("twistersfury\\phalcon\\queue\\job\\abstractjob")), twistersfury_chatbot_job_registerlistener_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Job_RegisterListener, handle)
{
	zval _0, _1, _2, _3, _4, _5, _6, _7, _8;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "eventsManager");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_4, &_3, ZEND_STRL("event"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_7, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_8, &_7, ZEND_STRL("listener"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_CALL_METHOD(&_6, &_5, "get", NULL, 0, &_8);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_1, "attach", NULL, 0, &_4, &_6);
	zephir_check_call_status();
	RETURN_THIS();
}

