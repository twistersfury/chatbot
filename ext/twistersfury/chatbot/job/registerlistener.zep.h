
extern zend_class_entry *twistersfury_chatbot_job_registerlistener_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Job_RegisterListener);

PHP_METHOD(TwistersFury_ChatBot_Job_RegisterListener, handle);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_job_registerlistener_handle, 0, 0, TwistersFury\\Phalcon\\Queue\\Job\\JobInterface, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_job_registerlistener_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Job_RegisterListener, handle, arginfo_twistersfury_chatbot_job_registerlistener_handle, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
