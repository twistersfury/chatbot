
#ifdef HAVE_CONFIG_H
#include "../../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../../php_ext.h"
#include "../../../../../ext.h"

#include <Zend/zend_exceptions.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Interfaces_Packet_Incoming)
{
	ZEPHIR_REGISTER_INTERFACE(TwistersFury\\ChatBot\\Listener\\Interfaces\\Packet, Incoming, twistersfury_chatbot, listener_interfaces_packet_incoming, twistersfury_chatbot_listener_interfaces_packet_incoming_method_entry);

	return SUCCESS;
}

ZEPHIR_DOC_METHOD(TwistersFury_ChatBot_Listener_Interfaces_Packet_Incoming, onIncoming);
