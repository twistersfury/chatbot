
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "kernel/object.h"
#include "kernel/concat.h"
#include "kernel/operators.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"
#include "kernel/string.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Packet_AbstractPacket)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Listener\\Packet, AbstractPacket, twistersfury_chatbot, listener_packet_abstractpacket, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\injectable")), twistersfury_chatbot_listener_packet_abstractpacket_method_entry, ZEND_ACC_EXPLICIT_ABSTRACT_CLASS);

	zend_class_implements(twistersfury_chatbot_listener_packet_abstractpacket_ce, 1, zephir_get_internal_ce(SL("twistersfury\\shared\\di\\interfaces\\initializationaware")));
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, initialize)
{
	zval _3;
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 1, 0);
	ZEPHIR_INIT_NVAR(&_2);
	zephir_get_class(&_2, this_ptr, 0);
	zephir_array_update_string(&_3, SL("class"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "Listener Initialized");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2, &_3);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getPacketBuilder)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "packetBuilder");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, sendMessage)
{
	zval _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, *packet, packet_sub, _0, _1, _2, _3, _5, _6$$3;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_4);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(message)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 0, &message_param, &packet);
	zephir_get_strval(&message, message_param);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	zephir_get_class(&_2, this_ptr, 0);
	ZEPHIR_INIT_VAR(&_3);
	ZEPHIR_CONCAT_VS(&_3, &_2, "::sendMessage");
	ZEPHIR_INIT_VAR(&_4);
	zephir_create_array(&_4, 1, 0);
	zephir_array_update_string(&_4, SL("message"), &message, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_3, &_4);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, this_ptr, "isenabled", NULL, 0);
	zephir_check_call_status();
	if (UNEXPECTED(zephir_is_true(&_5))) {
		ZEPHIR_CALL_METHOD(&_6$$3, this_ptr, "getpacketbuilder", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_METHOD(NULL, &_6$$3, "sendmessage", NULL, 0, &message, packet);
		zephir_check_call_status();
	}
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, isEnabled)
{
	zval _0, _1, _2;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);


	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "enabled");
	ZVAL_BOOL(&_2, 1);
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getcacheorconfigvalue", NULL, 0, &_1, &_2);
	zephir_check_call_status();
	RETURN_MM_BOOL(zephir_get_boolval(&_0));
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getListenerConfig)
{
	zend_bool _3;
	zval _0, _1, _2, _4, _5, _6, _9, _10, _11, _7$$3, _8$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_11);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&_8$$3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "listenerConfig");
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &_2);
	zephir_check_call_status();
	_3 = !zephir_is_true(&_1);
	if (!(_3)) {
		ZEPHIR_CALL_METHOD(&_4, this_ptr, "getconfig", NULL, 0);
		zephir_check_call_status();
		zephir_read_property(&_5, &_4, ZEND_STRL("listenerConfig"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_INIT_NVAR(&_2);
		zephir_get_class(&_2, this_ptr, 0);
		ZEPHIR_CALL_METHOD(&_6, &_5, "has", NULL, 0, &_2);
		zephir_check_call_status();
		_3 = !zephir_is_true(&_6);
	}
	if (_3) {
		ZEPHIR_CALL_METHOD(&_7$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_8$$3);
		ZVAL_STRING(&_8$$3, "Phalcon\\Config\\Config");
		ZEPHIR_RETURN_CALL_METHOD(&_7$$3, "get", NULL, 0, &_8$$3);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_CALL_METHOD(&_9, this_ptr, "getconfig", NULL, 0);
	zephir_check_call_status();
	zephir_read_property(&_10, &_9, ZEND_STRL("listenerConfig"), PH_NOISY_CC | PH_READONLY);
	ZEPHIR_INIT_VAR(&_11);
	zephir_get_class(&_11, this_ptr, 0);
	ZEPHIR_RETURN_CALL_METHOD(&_10, "get", NULL, 0, &_11);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getCacheOrConfigValue)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *key_param = NULL, *defaultValue = NULL, defaultValue_sub, __$null, cacheKey, _0, _1, _3, _2$$3;
	zval key;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&key);
	ZVAL_UNDEF(&defaultValue_sub);
	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&cacheKey);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_2$$3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(key)
		Z_PARAM_OPTIONAL
		Z_PARAM_ZVAL_OR_NULL(defaultValue)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &key_param, &defaultValue);
	if (UNEXPECTED(Z_TYPE_P(key_param) != IS_STRING && Z_TYPE_P(key_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'key' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(key_param) == IS_STRING)) {
		zephir_get_strval(&key, key_param);
	} else {
		ZEPHIR_INIT_VAR(&key);
	}
	if (!defaultValue) {
		defaultValue = &defaultValue_sub;
		defaultValue = &__$null;
	}


	ZEPHIR_CALL_METHOD(&cacheKey, this_ptr, "buildcachekey", NULL, 0, &key);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getcache", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, &_0, "has", NULL, 0, &cacheKey);
	zephir_check_call_status();
	if (zephir_is_true(&_1)) {
		ZEPHIR_CALL_METHOD(&_2$$3, this_ptr, "getcache", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_RETURN_CALL_METHOD(&_2$$3, "get", NULL, 0, &cacheKey);
		zephir_check_call_status();
		RETURN_MM();
	}
	ZEPHIR_CALL_METHOD(&_3, this_ptr, "getlistenerconfig", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_RETURN_CALL_METHOD(&_3, "get", NULL, 0, &key, defaultValue);
	zephir_check_call_status();
	RETURN_MM();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, sendPacket)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *packet, packet_sub, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_listener_packet_outgoingpacket_ce)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &packet);


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getpacketbuilder", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(NULL, &_0, "handlemessage", NULL, 0, packet);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, onIncoming)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval data, _3;
	zval *event, event_sub, *packet, packet_sub, *data_param = NULL, _0, _1, _2, _4, _5, _6;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&event_sub);
	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&data);
	ZVAL_UNDEF(&_3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_OBJECT_OF_CLASS(event, zephir_get_internal_ce(SL("phalcon\\events\\event")))
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(data)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &event, &packet, &data_param);
	if (!data_param) {
		ZEPHIR_INIT_VAR(&data);
		array_init(&data);
	} else {
		zephir_get_arrval(&data, data_param);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "logger");
	ZEPHIR_CALL_METHOD(&_1, &_0, "get", NULL, 0, &_2);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	zephir_create_array(&_3, 5, 0);
	ZEPHIR_INIT_NVAR(&_2);
	zephir_get_class(&_2, this_ptr, 0);
	zephir_array_update_string(&_3, SL("class"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	zephir_get_class(&_2, packet, 0);
	zephir_array_update_string(&_3, SL("packet"), &_2, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_4, packet, "getmessage", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("message"), &_4, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_4, packet, "getfrom", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_5, &_4, "getname", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("from"), &_5, PH_COPY | PH_SEPARATE);
	ZEPHIR_CALL_METHOD(&_5, packet, "getfromuser", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, &_5, "getname", NULL, 0);
	zephir_check_call_status();
	zephir_array_update_string(&_3, SL("user"), &_6, PH_COPY | PH_SEPARATE);
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "onIncoming");
	ZEPHIR_CALL_METHOD(NULL, &_1, "debug", NULL, 0, &_2, &_3);
	zephir_check_call_status();
	RETURN_MM_BOOL(1);
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, buildCacheKey)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *suffix_param = NULL, _0, _1, _2, _3;
	zval suffix;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&suffix);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(suffix)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &suffix_param);
	if (UNEXPECTED(Z_TYPE_P(suffix_param) != IS_STRING && Z_TYPE_P(suffix_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'suffix' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(suffix_param) == IS_STRING)) {
		zephir_get_strval(&suffix, suffix_param);
	} else {
		ZEPHIR_INIT_VAR(&suffix);
	}


	ZEPHIR_INIT_VAR(&_0);
	ZEPHIR_INIT_VAR(&_1);
	zephir_get_class(&_1, this_ptr, 0);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "\\");
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, ".");
	zephir_fast_str_replace(&_0, &_2, &_3, &_1);
	ZEPHIR_CONCAT_VSV(return_value, &_0, ".", &suffix);
	RETURN_MM();
}

