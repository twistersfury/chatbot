
extern zend_class_entry *twistersfury_chatbot_listener_packet_abstractpacket_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Packet_AbstractPacket);

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, initialize);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getPacketBuilder);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, sendMessage);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, isEnabled);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getListenerConfig);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getCacheOrConfigValue);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, sendPacket);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, onIncoming);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, buildCacheKey);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_initialize, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_getpacketbuilder, 0, 0, TwistersFury\\ChatBot\\Connection\\Packet\\Builder, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_sendmessage, 0, 2, TwistersFury\\ChatBot\\Listener\\Packet\\AbstractPacket, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_isenabled, 0, 0, _IS_BOOL, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_getlistenerconfig, 0, 0, Phalcon\\Config\\Config, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_getcacheorconfigvalue, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, key, IS_STRING, 0)
	ZEND_ARG_INFO(0, defaultValue)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_sendpacket, 0, 1, TwistersFury\\ChatBot\\Listener\\Packet\\AbstractPacket, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Listener\\Packet\\OutgoingPacket, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_onincoming, 0, 2, _IS_BOOL, 0)
	ZEND_ARG_OBJ_INFO(0, event, Phalcon\\Events\\Event, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, data, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, data, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_abstractpacket_buildcachekey, 0, 1, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, suffix, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_listener_packet_abstractpacket_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, initialize, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_initialize, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getPacketBuilder, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_getpacketbuilder, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, sendMessage, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_sendmessage, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, isEnabled, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_isenabled, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getListenerConfig, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_getlistenerconfig, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, getCacheOrConfigValue, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_getcacheorconfigvalue, ZEND_ACC_PROTECTED)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, sendPacket, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_sendpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, onIncoming, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_onincoming, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_AbstractPacket, buildCacheKey, arginfo_twistersfury_chatbot_listener_packet_abstractpacket_buildcachekey, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
