
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/operators.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Packet_Curse)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Listener\\Packet, Curse, twistersfury_chatbot, listener_packet_curse, twistersfury_chatbot_listener_packet_abstractpacket_ce, twistersfury_chatbot_listener_packet_curse_method_entry, 0);

	zend_class_implements(twistersfury_chatbot_listener_packet_curse_ce, 1, twistersfury_chatbot_listener_interfaces_packet_incoming_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Curse, onIncoming)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_0 = NULL;
	zval data;
	zval *event, event_sub, *packet, packet_sub, *data_param = NULL, _1, _2, _3, _4$$3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&event_sub);
	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&data);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_OBJECT_OF_CLASS(event, zephir_get_internal_ce(SL("phalcon\\events\\event")))
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(data)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &event, &packet, &data_param);
	if (!data_param) {
		ZEPHIR_INIT_VAR(&data);
		array_init(&data);
	} else {
		zephir_get_arrval(&data, data_param);
	}


	ZEPHIR_CALL_PARENT(NULL, twistersfury_chatbot_listener_packet_curse_ce, getThis(), "onincoming", &_0, 0, event, packet, &data);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, packet, "getmessage", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "curse");
	ZEPHIR_CALL_FUNCTION(&_3, "stripos", NULL, 35, &_1, &_2);
	zephir_check_call_status();
	if (!ZEPHIR_IS_FALSE_IDENTICAL(&_3)) {
		ZEPHIR_INIT_VAR(&_4$$3);
		ZVAL_STRING(&_4$$3, "Ah, curse your sudden but inevitable betrayal!");
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "sendmessage", NULL, 0, &_4$$3, packet);
		zephir_check_call_status();
	}
	RETURN_MM_BOOL(1);
}

