
extern zend_class_entry *twistersfury_chatbot_listener_packet_curse_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Packet_Curse);

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Curse, onIncoming);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_curse_onincoming, 0, 2, _IS_BOOL, 0)
	ZEND_ARG_OBJ_INFO(0, event, Phalcon\\Events\\Event, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, data, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, data, 0)
#endif
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_listener_packet_curse_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Curse, onIncoming, arginfo_twistersfury_chatbot_listener_packet_curse_onincoming, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
