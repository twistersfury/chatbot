
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/operators.h"
#include "kernel/array.h"
#include "kernel/string.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/exception.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Packet_Quotes)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Listener\\Packet, Quotes, twistersfury_chatbot, listener_packet_quotes, twistersfury_chatbot_listener_packet_abstractpacket_ce, twistersfury_chatbot_listener_packet_quotes_method_entry, 0);

	zend_declare_property_null(twistersfury_chatbot_listener_packet_quotes_ce, SL("quotes"), ZEND_ACC_PRIVATE);
	twistersfury_chatbot_listener_packet_quotes_ce->create_object = zephir_init_properties_TwistersFury_ChatBot_Listener_Packet_Quotes;

	zend_class_implements(twistersfury_chatbot_listener_packet_quotes_ce, 1, twistersfury_chatbot_listener_interfaces_packet_incoming_ce);
	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, getQuotes)
{
	zval *this_ptr = getThis();



	RETURN_MEMBER(getThis(), "quotes");
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, initialize)
{
	zval _13$$3;
	zval _1, _2, _3, _4, _5$$3, _6$$3, _7$$3, _8$$3, _9$$3, _10$$3, _11$$3, _12$$3, _14$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_0 = NULL;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_7$$3);
	ZVAL_UNDEF(&_8$$3);
	ZVAL_UNDEF(&_9$$3);
	ZVAL_UNDEF(&_10$$3);
	ZVAL_UNDEF(&_11$$3);
	ZVAL_UNDEF(&_12$$3);
	ZVAL_UNDEF(&_14$$3);
	ZVAL_UNDEF(&_13$$3);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(NULL, twistersfury_chatbot_listener_packet_quotes_ce, getThis(), "initialize", &_0, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "config");
	ZEPHIR_CALL_METHOD(&_2, &_1, "get", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_3);
	ZVAL_STRING(&_3, "quotes");
	ZEPHIR_CALL_METHOD(&_4, &_2, "has", NULL, 0, &_3);
	zephir_check_call_status();
	if (zephir_is_true(&_4)) {
		ZEPHIR_CALL_METHOD(&_5$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_7$$3);
		ZVAL_STRING(&_7$$3, "config");
		ZEPHIR_CALL_METHOD(&_6$$3, &_5$$3, "get", NULL, 0, &_7$$3);
		zephir_check_call_status();
		zephir_read_property(&_8$$3, &_6$$3, ZEND_STRL("quotes"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&_9$$3, &_8$$3, "toarray", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_CALL_FUNCTION(&_10$$3, "array_change_key_case", NULL, 36, &_9$$3);
		zephir_check_call_status();
		zephir_update_property_zval(this_ptr, ZEND_STRL("quotes"), &_10$$3);
		ZEPHIR_CALL_METHOD(&_11$$3, this_ptr, "getdi", NULL, 0);
		zephir_check_call_status();
		ZEPHIR_INIT_NVAR(&_7$$3);
		ZVAL_STRING(&_7$$3, "logger");
		ZEPHIR_CALL_METHOD(&_12$$3, &_11$$3, "get", NULL, 0, &_7$$3);
		zephir_check_call_status();
		ZEPHIR_INIT_VAR(&_13$$3);
		zephir_create_array(&_13$$3, 1, 0);
		ZEPHIR_CALL_METHOD(&_14$$3, this_ptr, "getquotes", NULL, 0);
		zephir_check_call_status();
		zephir_array_update_string(&_13$$3, SL("quotes"), &_14$$3, PH_COPY | PH_SEPARATE);
		ZEPHIR_INIT_NVAR(&_7$$3);
		ZVAL_STRING(&_7$$3, "Quotes Listner");
		ZEPHIR_CALL_METHOD(NULL, &_12$$3, "debug", NULL, 0, &_7$$3, &_13$$3);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, onIncoming)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_0 = NULL;
	zval data;
	zval *event, event_sub, *packet, packet_sub, *data_param = NULL, response, _1, _2, _3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&event_sub);
	ZVAL_UNDEF(&packet_sub);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&data);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_OBJECT_OF_CLASS(event, zephir_get_internal_ce(SL("phalcon\\events\\event")))
		Z_PARAM_OBJECT_OF_CLASS(packet, twistersfury_chatbot_connection_packet_interfaces_incomingpacket_ce)
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(data)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &event, &packet, &data_param);
	if (!data_param) {
		ZEPHIR_INIT_VAR(&data);
		array_init(&data);
	} else {
		zephir_get_arrval(&data, data_param);
	}


	ZEPHIR_CALL_PARENT(NULL, twistersfury_chatbot_listener_packet_quotes_ce, getThis(), "onincoming", &_0, 0, event, packet, &data);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, packet, "getmessage", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&response, this_ptr, "checkpacket", NULL, 37, &_1);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_2);
	zephir_gettype(&_2, &response);
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "string");
	if (ZEPHIR_IS_IDENTICAL(&_3, &_2)) {
		ZEPHIR_CALL_METHOD(NULL, this_ptr, "sendmessage", NULL, 0, &response, packet);
		zephir_check_call_status();
	}
	RETURN_MM_BOOL(1);
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, checkPacket)
{
	zend_string *_5;
	zend_ulong _4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *message_param = NULL, _0, keyword, response, _1, *_2, _3, _6$$3, _7$$5;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&keyword);
	ZVAL_UNDEF(&response);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_7$$5);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	if (UNEXPECTED(Z_TYPE_P(message_param) != IS_STRING && Z_TYPE_P(message_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'message' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(message_param) == IS_STRING)) {
		zephir_get_strval(&message, message_param);
	} else {
		ZEPHIR_INIT_VAR(&message);
	}


	ZEPHIR_CALL_METHOD(&_0, this_ptr, "normalizemessage", NULL, 38, &message);
	zephir_check_call_status();
	zephir_get_strval(&message, &_0);
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getquotes", NULL, 0);
	zephir_check_call_status();
	zephir_is_iterable(&_1, 0, "twistersfury/chatbot/Listener/Packet/Quotes.zep", 59);
	if (Z_TYPE_P(&_1) == IS_ARRAY) {
		ZEND_HASH_FOREACH_KEY_VAL(Z_ARRVAL_P(&_1), _4, _5, _2)
		{
			ZEPHIR_INIT_NVAR(&keyword);
			if (_5 != NULL) { 
				ZVAL_STR_COPY(&keyword, _5);
			} else {
				ZVAL_LONG(&keyword, _4);
			}
			ZEPHIR_INIT_NVAR(&response);
			ZVAL_COPY(&response, _2);
			ZEPHIR_INIT_NVAR(&_6$$3);
			zephir_fast_strpos(&_6$$3, &message, &keyword, 0 );
			if (!ZEPHIR_IS_FALSE_IDENTICAL(&_6$$3)) {
				RETURN_CCTOR(&response);
			}
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, &_1, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_3, &_1, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_3)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&keyword, &_1, "key", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&response, &_1, "current", NULL, 0);
			zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&_7$$5);
				zephir_fast_strpos(&_7$$5, &message, &keyword, 0 );
				if (!ZEPHIR_IS_FALSE_IDENTICAL(&_7$$5)) {
					RETURN_CCTOR(&response);
				}
			ZEPHIR_CALL_METHOD(NULL, &_1, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&response);
	ZEPHIR_INIT_NVAR(&keyword);
	RETURN_MM_BOOL(0);
}

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, normalizeMessage)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zval *message_param = NULL;
	zval message;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(message)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message_param);
	if (UNEXPECTED(Z_TYPE_P(message_param) != IS_STRING && Z_TYPE_P(message_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'message' must be of the type string"));
		RETURN_MM_NULL();
	}
	if (EXPECTED(Z_TYPE_P(message_param) == IS_STRING)) {
		zephir_get_strval(&message, message_param);
	} else {
		ZEPHIR_INIT_VAR(&message);
	}


	zephir_fast_strtolower(return_value, &message);
	RETURN_MM();
}

zend_object *zephir_init_properties_TwistersFury_ChatBot_Listener_Packet_Quotes(zend_class_entry *class_type)
{
		zval _0, _1$$3;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
		ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1$$3);
	

		ZEPHIR_MM_GROW();
	
	{
		zval local_this_ptr, *this_ptr = &local_this_ptr;
		ZEPHIR_CREATE_OBJECT(this_ptr, class_type);
		zephir_read_property_ex(&_0, this_ptr, ZEND_STRL("quotes"), PH_NOISY_CC | PH_READONLY);
		if (Z_TYPE_P(&_0) == IS_NULL) {
			ZEPHIR_INIT_VAR(&_1$$3);
			array_init(&_1$$3);
			zephir_update_property_zval_ex(this_ptr, ZEND_STRL("quotes"), &_1$$3);
		}
		ZEPHIR_MM_RESTORE();
		return Z_OBJ_P(this_ptr);
	}
}

