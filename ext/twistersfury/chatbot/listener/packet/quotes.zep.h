
extern zend_class_entry *twistersfury_chatbot_listener_packet_quotes_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Listener_Packet_Quotes);

PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, getQuotes);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, initialize);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, onIncoming);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, checkPacket);
PHP_METHOD(TwistersFury_ChatBot_Listener_Packet_Quotes, normalizeMessage);
zend_object *zephir_init_properties_TwistersFury_ChatBot_Listener_Packet_Quotes(zend_class_entry *class_type);

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_quotes_getquotes, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_quotes_initialize, 0, 0, IS_VOID, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_quotes_onincoming, 0, 2, _IS_BOOL, 0)
	ZEND_ARG_OBJ_INFO(0, event, Phalcon\\Events\\Event, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, data, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, data, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_quotes_checkpacket, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_quotes_normalizemessage, 0, 1, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, message, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_twistersfury_chatbot_listener_packet_quotes_zephir_init_properties_twistersfury_chatbot_listener_packet_quotes, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_listener_packet_quotes_method_entry) {
#if PHP_VERSION_ID >= 80000
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Quotes, getQuotes, arginfo_twistersfury_chatbot_listener_packet_quotes_getquotes, ZEND_ACC_PUBLIC)
#else
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Quotes, getQuotes, NULL, ZEND_ACC_PUBLIC)
#endif
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Quotes, initialize, arginfo_twistersfury_chatbot_listener_packet_quotes_initialize, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Quotes, onIncoming, arginfo_twistersfury_chatbot_listener_packet_quotes_onincoming, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Quotes, checkPacket, arginfo_twistersfury_chatbot_listener_packet_quotes_checkpacket, ZEND_ACC_PRIVATE)
	PHP_ME(TwistersFury_ChatBot_Listener_Packet_Quotes, normalizeMessage, arginfo_twistersfury_chatbot_listener_packet_quotes_normalizemessage, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
