
#ifdef HAVE_CONFIG_H
#include "../../../ext_config.h"
#endif

#include <php.h>
#include "../../../php_ext.h"
#include "../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"

// top statement before namespace, add to after headers
#ifdef COVERAGE
extern void __gcov_flush(void);
#endif



ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Support_Debug)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Support, Debug, twistersfury_chatbot, support_debug, zephir_get_internal_ce(SL("phalcon\\support\\debug")), twistersfury_chatbot_support_debug_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Support_Debug, flushCoverage)
{
	zval *this_ptr = getThis();



	
               #ifdef COVERAGE
                 __gcov_flush();
               #endif
            
}

