
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Twitch_Driver_Irc)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Twitch\\Driver, Irc, twistersfury_chatbot, twitch_driver_irc, twistersfury_chatbot_irc_driver_abstractirc_ce, twistersfury_chatbot_twitch_driver_irc_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Twitch_Driver_Irc, initialize)
{
	zval _1, _2, _3, _4, _5, _6, _7;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_0 = NULL;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);


	ZEPHIR_MM_GROW();

	ZEPHIR_CALL_PARENT(NULL, twistersfury_chatbot_twitch_driver_irc_ce, getThis(), "initialize", &_0, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_3);
	ZVAL_STRING(&_3, "logger");
	ZEPHIR_CALL_METHOD(&_2, &_1, "get", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_3);
	ZVAL_STRING(&_3, "Twitch IRC Initialize");
	ZEPHIR_CALL_METHOD(NULL, &_2, "debug", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_OBS_VAR(&_4);
	zephir_read_property(&_4, this_ptr, ZEND_STRL("connectionManager"), PH_NOISY_CC);
	ZEPHIR_CALL_METHOD(&_5, &_4, "geteventsmanager", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_6, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_3);
	ZVAL_STRING(&_3, "TwistersFury\\ChatBot\\Twitch\\Listener\\Packet");
	ZEPHIR_CALL_METHOD(&_7, &_6, "get", NULL, 0, &_3);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_3);
	ZVAL_STRING(&_3, "packet");
	ZEPHIR_CALL_METHOD(NULL, &_5, "attach", NULL, 0, &_3, &_7);
	zephir_check_call_status();
	RETURN_THIS();
}

PHP_METHOD(TwistersFury_ChatBot_Twitch_Driver_Irc, sendUser)
{
	zval *this_ptr = getThis();



	RETURN_THISW();
}

