
extern zend_class_entry *twistersfury_chatbot_twitch_driver_irc_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Twitch_Driver_Irc);

PHP_METHOD(TwistersFury_ChatBot_Twitch_Driver_Irc, initialize);
PHP_METHOD(TwistersFury_ChatBot_Twitch_Driver_Irc, sendUser);

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_twitch_driver_irc_initialize, 0, 0, TwistersFury\\ChatBot\\Connection\\Driver\\DriverInterface, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_twitch_driver_irc_senduser, 0, 0, TwistersFury\\ChatBot\\Irc\\Driver\\AbstractIrc, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_twitch_driver_irc_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Twitch_Driver_Irc, initialize, arginfo_twistersfury_chatbot_twitch_driver_irc_initialize, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Twitch_Driver_Irc, sendUser, arginfo_twistersfury_chatbot_twitch_driver_irc_senduser, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
