
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/operators.h"
#include "kernel/exception.h"
#include "kernel/object.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Twitch_Listener_Driver)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Twitch\\Listener, Driver, twistersfury_chatbot, twitch_listener_driver, zephir_get_internal_ce(SL("phalcon\\di\\injectable")), twistersfury_chatbot_twitch_listener_driver_method_entry, 0);

	return SUCCESS;
}

PHP_METHOD(TwistersFury_ChatBot_Twitch_Listener_Driver, sendPacket)
{
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval data;
	zval *event, event_sub, *manger, manger_sub, *data_param = NULL, packet, _0, _1, _2, _3$$4;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&event_sub);
	ZVAL_UNDEF(&manger_sub);
	ZVAL_UNDEF(&packet);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3$$4);
	ZVAL_UNDEF(&data);
#if PHP_VERSION_ID >= 80000
	bool is_null_true = 1;
	ZEND_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_OBJECT_OF_CLASS(event, twistersfury_chatbot_twitch_listener_event_ce)
		Z_PARAM_OBJECT_OF_CLASS(manger, zephir_get_internal_ce(SL("twistersfury\\chatbot\\connection\\throttle\\manager")))
		Z_PARAM_OPTIONAL
		Z_PARAM_ARRAY(data)
	ZEND_PARSE_PARAMETERS_END();
#endif


	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 1, &event, &manger, &data_param);
	if (!data_param) {
		ZEPHIR_INIT_VAR(&data);
		array_init(&data);
	} else {
		zephir_get_arrval(&data, data_param);
	}


#line 18 "/app/twistersfury/chatbot/Twitch/Listener/Driver.zep"
	ZEPHIR_OBS_VAR(&packet);
	if (!(zephir_array_isset_string_fetch(&packet, &data, SL("packet"), 0))) {
#line 16 "/app/twistersfury/chatbot/Twitch/Listener/Driver.zep"
		RETURN_MM_NULL();
	}
#line 21 "/app/twistersfury/chatbot/Twitch/Listener/Driver.zep"
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getthrottlemanager", NULL, 28);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_2, &packet, "getto", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_CALL_METHOD(&_1, &_0, "cansend", NULL, 0, &_2);
	zephir_check_call_status();
	if (!(zephir_is_true(&_1))) {
#line 20 "/app/twistersfury/chatbot/Twitch/Listener/Driver.zep"
		ZEPHIR_INIT_VAR(&_3$$4);
		object_init_ex(&_3$$4, twistersfury_chatbot_connection_throttle_exceptions_throttled_ce);
		ZEPHIR_CALL_METHOD(NULL, &_3$$4, "__construct", NULL, 29);
		zephir_check_call_status();
		zephir_throw_exception_debug(&_3$$4, "twistersfury/chatbot/Twitch/Listener/Driver.zep", 19);
		ZEPHIR_MM_RESTORE();
		return;
	}
	ZEPHIR_MM_RESTORE();
}

PHP_METHOD(TwistersFury_ChatBot_Twitch_Listener_Driver, getThrottleManager)
{
	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);


	ZEPHIR_MM_GROW();

#line 26 "/app/twistersfury/chatbot/Twitch/Listener/Driver.zep"
	ZEPHIR_CALL_METHOD(&_0, this_ptr, "getdi", NULL, 0);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&_1);
	ZVAL_STRING(&_1, "throttleManager");
	ZEPHIR_RETURN_CALL_METHOD(&_0, "get", NULL, 0, &_1);
	zephir_check_call_status();
	RETURN_MM();
}

