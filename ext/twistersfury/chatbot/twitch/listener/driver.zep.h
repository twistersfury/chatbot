
extern zend_class_entry *twistersfury_chatbot_twitch_listener_driver_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Twitch_Listener_Driver);

PHP_METHOD(TwistersFury_ChatBot_Twitch_Listener_Driver, sendPacket);
PHP_METHOD(TwistersFury_ChatBot_Twitch_Listener_Driver, getThrottleManager);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_twitch_listener_driver_sendpacket, 0, 2, IS_VOID, 0)

	ZEND_ARG_OBJ_INFO(0, event, TwistersFury\\ChatBot\\Twitch\\Listener\\Event, 0)
	ZEND_ARG_OBJ_INFO(0, manger, TwistersFury\\ChatBot\\Connection\\Throttle\\Manager, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, data, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, data, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_twitch_listener_driver_getthrottlemanager, 0, 0, TwistersFury\\ChatBot\\Connection\\Throttle\\Manager, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_twitch_listener_driver_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Twitch_Listener_Driver, sendPacket, arginfo_twistersfury_chatbot_twitch_listener_driver_sendpacket, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Twitch_Listener_Driver, getThrottleManager, arginfo_twistersfury_chatbot_twitch_listener_driver_getthrottlemanager, ZEND_ACC_PRIVATE)
	PHP_FE_END
};
