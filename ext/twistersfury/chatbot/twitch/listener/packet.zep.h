
extern zend_class_entry *twistersfury_chatbot_twitch_listener_packet_ce;

ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Twitch_Listener_Packet);

PHP_METHOD(TwistersFury_ChatBot_Twitch_Listener_Packet, onWelcome);
PHP_METHOD(TwistersFury_ChatBot_Twitch_Listener_Packet, buildCapabilityPacket);

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_twistersfury_chatbot_twitch_listener_packet_onwelcome, 0, 2, IS_VOID, 0)

	ZEND_ARG_OBJ_INFO(0, event, TwistersFury\\ChatBot\\Twitch\\Listener\\Event, 0)
	ZEND_ARG_OBJ_INFO(0, packet, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\IncomingPacket, 0)
#if PHP_VERSION_ID >= 80000
	ZEND_ARG_TYPE_INFO_WITH_DEFAULT_VALUE(0, data, IS_ARRAY, 0, "[]")
#else
	ZEND_ARG_ARRAY_INFO(0, data, 0)
#endif
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_WITH_RETURN_OBJ_INFO_EX(arginfo_twistersfury_chatbot_twitch_listener_packet_buildcapabilitypacket, 0, 1, TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\OutgoingPacket, 0)
	ZEND_ARG_TYPE_INFO(0, capability, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(twistersfury_chatbot_twitch_listener_packet_method_entry) {
	PHP_ME(TwistersFury_ChatBot_Twitch_Listener_Packet, onWelcome, arginfo_twistersfury_chatbot_twitch_listener_packet_onwelcome, ZEND_ACC_PUBLIC)
	PHP_ME(TwistersFury_ChatBot_Twitch_Listener_Packet, buildCapabilityPacket, arginfo_twistersfury_chatbot_twitch_listener_packet_buildcapabilitypacket, ZEND_ACC_PROTECTED)
	PHP_FE_END
};
