
#ifdef HAVE_CONFIG_H
#include "../../../../ext_config.h"
#endif

#include <php.h>
#include "../../../../php_ext.h"
#include "../../../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(TwistersFury_ChatBot_Twitch_Packet_Capabilities)
{
	ZEPHIR_REGISTER_CLASS_EX(TwistersFury\\ChatBot\\Twitch\\Packet, Capabilities, twistersfury_chatbot, twitch_packet_capabilities, twistersfury_chatbot_irc_packet_abstractpacket_ce, NULL, 0);

	return SUCCESS;
}

