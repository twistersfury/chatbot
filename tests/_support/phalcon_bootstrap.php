<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

return new \Phalcon\Mvc\Application(new \Phalcon\Di\FactoryDefault());
