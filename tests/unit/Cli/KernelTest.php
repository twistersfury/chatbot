<?php

namespace TwistersFury\ChatBot\Tests\Unit\Cli;

use Codeception\Stub;
use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Di\FactoryDefault;
use Phalcon\Logger\Logger;
use TwistersFury\ChatBot\Cli\Kernel;

class KernelTest extends Unit
{
    /** @var Kernel */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $di = new FactoryDefault();

        $di->set('config', new Config(['modules' => []]));
        $di->set('logger', Stub::makeEmpty(Logger::class));

        $this->testSubject = new Kernel($di, codecept_root_dir());
    }

    public function testDi()
    {
        $this->assertInstanceOf(Kernel::class, $this->testSubject);
    }
}
