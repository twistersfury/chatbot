<?php

namespace TwistersFury\ChatBot\Tests\Unit\Cli\Task;

use Codeception\Test\Unit;
use TwistersFury\ChatBot\Cli\Task\ConnectTask;

class ConnectTaskTest extends Unit
{
    /** @var ConnectTask */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new ConnectTask();
    }

    public function testDi()
    {
        $this->assertInstanceOf(ConnectTask::class, $this->testSubject);
    }
}
