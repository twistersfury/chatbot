<?php

namespace TwistersFury\ChatBot\Tests\Unit\Connection;

use Codeception\Stub;
use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger\Logger;
use TwistersFury\ChatBot\Connection\Driver\Factory;
use TwistersFury\ChatBot\Connection\Manager;
use TwistersFury\ChatBot\Connection\Driver\DriverInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;

class ManagerTest extends Unit
{
    /** @var Manager */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $config = new Config([
            'driver' => 'some-driver',
            'some-driver' => []
        ]);

        $di = new Di();
        $di->set('eventsManager', Stub::makeEmpty(EventsManager::class));
        $di->set('config', $config);
        $di->set('some-driver', $this->getMockBuilder(DriverInterface::class)->getMockForAbstractClass());
        $di->set('logger', Stub::makeEmpty(Logger::class));
        $di->set('driverFactory', Stub::makeEmpty(
            Factory::class,
            [
                'load' => $this->getMockBuilder(DriverInterface::class)->getMockForAbstractClass()
            ]
        ));

        $this->testSubject = new Manager($config);
        $this->testSubject->setDi($di);
    }

    public function testGetDriver()
    {
        $this->assertInstanceOf(DriverInterface::class, $this->testSubject->getDriver());
    }

    public function testSendPacket()
    {
        $mockPacket = Stub::makeEmpty(OutgoingPacket::class);

        $mockDriver = $this->getMockBuilder(DriverInterface::class)
                            ->onlyMethods(['sendPacket'])
                           ->getMockForAbstractClass();

        $this->testSubject->getDI()->set(
            'some-driver',
            $mockDriver
        );

        $this->assertSame($this->testSubject, $this->testSubject->sendPacket($mockPacket));
    }

    public function testReadPacket()
    {
        $mockPacket = Stub::makeEmpty(IncomingPacket::class);

        $mockDriver = $this->getMockBuilder(DriverInterface::class)
            ->onlyMethods(['readPacket'])
            ->getMockForAbstractClass();

        $this->testSubject->getDI()->set(
            'driverFactory',
            Stub::makeEmpty(
                Factory::class,
                [
                    'load' => Stub::makeEmpty(
                        DriverInterface::class,
                        [
                            'readPacket' => $mockPacket
                        ]
                    )
                ]
            )
        );

        $this->testSubject->getDI()->set(
            'some-driver',
            $mockDriver
        );

        $this->assertSame($mockPacket, $this->testSubject->readPacket());
    }
}
