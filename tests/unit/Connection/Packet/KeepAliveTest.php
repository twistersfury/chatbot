<?php

namespace TwistersFury\ChatBot\Tests\Unit\Connection\Packet;

use Codeception\Test\Unit;
use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\KeepAlive;

class KeepAliveTest extends Unit
{
    /** @var KeepAlive */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new KeepAlive(new Config([]));
    }

    public function testDi()
    {
        $this->assertInstanceOf(KeepAlive::class, $this->testSubject);
    }
}
