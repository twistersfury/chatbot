<?php

namespace TwistersFury\ChatBot\Tests\Unit\Connection\Packet;

use Codeception\Test\Unit;
use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\Packet;


/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

class PacketTest extends Unit
{
    /** @var Packet */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Packet(new Config([
            "raw" => "raw",
            "message" => "message"
        ]));
    }

    public function testGetRawMessage()
    {
        $this->assertEquals("raw", $this->testSubject->getRawMessage());
    }

    public function testGetMessage()
    {
        $this->assertEquals("message", $this->testSubject->getMessage());
    }

    public function testGetFrom()
    {
        $this->assertEquals("", $this->testSubject->getFrom());
    }

    public function testbuildPacket()
    {
        $this->assertEquals("", $this->testSubject->buildPacket());
    }
}
