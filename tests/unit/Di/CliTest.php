<?php

namespace TwistersFury\ChatBot\Tests\Unit\Di;

use Codeception\Stub;
use TwistersFury\ChatBot\Connection\Manager;
use TwistersFury\ChatBot\Di\Cli;
use TwistersFury\Shared\Di\ServiceProvider\Logger;
use TwistersFury\Shared\Di\ServiceProvider\Config as tfConfig;
use TwistersFury\ChatBot\Di\ServiceProvider\Connection;
use TwistersFury\ChatBot\Di\ServiceProvider\Command;
use TwistersFury\Phalcon\Queue\Di\ServiceProvider\Queue;
use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Logger\Logger as phLogger;

class CliTest extends Unit
{
    private const BASE_SERVICE_COUNT = 21;

    /** @var Cli */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Cli();

        $this->testSubject->set(Logger::class, Stub::makeEmpty(Logger::class));
        $this->testSubject->set(tfConfig::class, Stub::makeEmpty(tfConfig::class));
        $this->testSubject->set(Connection::class, Stub::makeEmpty(Connection::class));
        $this->testSubject->set(Command::class, Stub::makeEmpty(Command::class));
        $this->testSubject->set(Queue::class, Stub::makeEmpty(Queue::class));

        $mockLogger = Stub::makeEmpty(
            phLogger::class
        );

        $this->testSubject->set('logger', $mockLogger);
        $this->testSubject->set('config', new Config());

        $this->testSubject->initialize();
    }

    /**
     * Using Mock Registers, So Will Only Register Items In CLI
     */
    public function testInitialize()
    {
        $this->assertEquals(static::BASE_SERVICE_COUNT, count($this->testSubject->getServices()));
    }
}
