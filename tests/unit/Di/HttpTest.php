<?php

namespace TwistersFury\ChatBot\Tests\Unit;

use Codeception\Stub;
use TwistersFury\ChatBot\Di\Http;
use Codeception\Test\Unit;
use Phalcon\Config\Config;

class HttpTest extends Unit
{
    /** @var Http */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Http();
    }

    public function testDi()
    {
        $mockConfig = Stub::makeEmpty(Config::class);
        $this->testSubject->set('config', $mockConfig);
        $this->assertSame($mockConfig, $this->testSubject->get('config'));
    }
}
