<?php

namespace TwistersFury\ChatBot\Tests\Unit\Helper\Socket;

use Codeception\Stub;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use TwistersFury\ChatBot\Helper\Socket\Socket;
use TwistersFury\ChatBot\Helper\Socket\Wrapper;
use Codeception\Test\Unit;

class SocketTest extends Unit
{
    /** @var Socket */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var resource */
    private $testHandle;

    public function _before()
    {
        parent::_before();

        $config = new Config([
            'host' => 'socket.test',
            'port' => 6797
        ]);

        $di = new Di();

        $this->testSubject = new Socket($config);
        $this->testSubject->setDi($di);

        $this->testHandle = fopen(codecept_data_dir('/.gitkeep'), 'r');
    }

    public function _after()
    {
        parent::_after();

        fclose($this->testHandle);
    }

    public function testConnect()
    {
        $this->testSubject->getDi()->set(
            Wrapper::class,
            Stub::makeEmpty(
                Wrapper::class,
                [
                    'open' => Stub\Expected::once(function (string $host, int $port) {
                        $this->assertEquals('socket.test', $host);
                        $this->assertEquals(6797, $port);

                        return $this->testHandle;
                    })
                ]
            )
        );

        $this->assertInstanceOf(Socket::class, $this->testSubject->connect());
    }

    public function testSend()
    {
        $this->testSubject->getDi()->set(
            Wrapper::class,
            Stub::makeEmpty(
                Wrapper::class,
                [
                    'send' => Stub\Expected::once('Hello World'),
                    'open' => Stub\Expected::once(function () {
                        return $this->testHandle;
                    })
                ]
            )
        );

        $this->assertInstanceOf(Socket::class, $this->testSubject->connect()->send('Hello World'));
    }

    public function testRead()
    {
        $this->testSubject->getDi()->set(
            Wrapper::class,
            Stub::makeEmpty(
                Wrapper::class,
                [
                    'get' => Stub\Expected::once(
                        function () {
                            return 'Hello World';
                        }
                    ),
                    'open' => Stub\Expected::once(function () {
                        return $this->testHandle;
                    })
                ]
            )
        );

        $this->assertEquals('Hello World', $this->testSubject->connect()->read());
    }
}
