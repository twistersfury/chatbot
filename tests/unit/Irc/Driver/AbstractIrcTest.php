<?php

namespace TwistersFury\ChatBot\Tests\Unit\Irc\Driver;

use Codeception\Stub;
use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Logger\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Irc\Driver\AbstractIrc;

class AbstractIrcTest extends Unit
{
    /** @var AbstractIrc|MockObject */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $config = new Config([
            'nickname' => 'me'
        ]);

        $this->testSubject = $this->getMockBuilder(AbstractIrc::class)
            ->onlyMethods(['send'])
            ->setConstructorArgs([$config])
            ->getMockForAbstractClass();

        $di = new Di();
        $di->set('logger', Stub::makeEmpty(Logger::class));

        $this->testSubject->setDI($di);
    }

    public function testSendPacket()
    {
        $mockPacket = Stub::makeEmpty(
            OutgoingPacket::class,
            [
                'buildPacket' => 'Some Message'
            ]
        );

        $this->testSubject->expects($this->once())->method('send')
                          ->with("Some Message\r\n")
                          ->willReturn($this->testSubject);

        $this->assertSame($this->testSubject, $this->testSubject->sendPacket($mockPacket));
    }
}
