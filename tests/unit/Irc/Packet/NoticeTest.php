<?php

namespace TwistersFury\ChatBot\Tests\Unit\Irc\Packet;

use Codeception\Stub;
use Codeception\Test\Unit;
use Generator;
use Phalcon\Config\Config;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\ChatBot\Irc\Packet\Notice;

class NoticeTest extends Unit
{
    /** @var Notice|MockObject */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Notice::class)
                                  ->onlyMethods(['getNickname', 'getMessage'])
                                  ->disableOriginalConstructor()
                                  ->getMockForAbstractClass();

        $this->testSubject->method('getNickname')->willReturn('Me');
        $this->testSubject->expects($this->once())->method('getMessage')->willReturn('Message');
    }

    public function testGetMethod()
    {
        $this->assertEquals('Message', $this->testSubject->getMessage());
    }
}
