<?php

namespace TwistersFury\ChatBot\Tests\Unit\Irc\Packet\Outgoing;

use Codeception\Test\Unit;
use Generator;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\ChatBot\Irc\Packet\Outgoing\AbstractOutgoing;

class AbstractOutgoingTest extends Unit
{
    /** @var AbstractPacket|MockObject */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractOutgoing::class)
                                  ->disableOriginalConstructor()
                                  ->onlyMethods(['getMethod', 'getMessage', 'getTo'])
                                  ->getMockForAbstractClass();

        $this->testSubject->expects($this->once())->method('getMethod')->willReturn('METHOD');
        $this->testSubject->expects($this->once())->method('getTo')->willReturn('CHANNEL');
    }

    /**
     * @dataProvider dpTestBuildMessage
     */
    public function testBuildMessage(string $message, string $result)
    {
        $this->testSubject->expects($this->once())->method('getMessage')->willReturn($message);

        $this->assertEquals($result, $this->testSubject->buildPacket());
    }

    public function dpTestBuildMessage(): Generator
    {
        yield 'Normal Message' => [
            'Normal Message',
            'METHOD CHANNEL  :Normal Message'
        ];

        yield 'Replacement &B' => [
            'Replacement &B',
            "METHOD CHANNEL  :Replacement \x02"
        ];

        yield 'Replacement &C' => [
            'Replacement &C',
            "METHOD CHANNEL  :Replacement \x03"
        ];

    }
}
