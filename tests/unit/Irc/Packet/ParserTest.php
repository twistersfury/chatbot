<?php

namespace TwistersFury\ChatBot\Tests\Unit\Irc\Packet;

use Codeception\Stub;
use Codeception\Test\Unit;
use Generator;
use TwistersFury\ChatBot\Irc\Packet\Parser;
use TwistersFury\ChatBot\Irc\Packet\User;

class ParserTest extends Unit
{

    /** @var Parser */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Parser();
    }

    /**
     * @dataProvider dpTestParser
     */
    public function testBuildMessage(string $message, array $result)
    {
        $actual = $this->testSubject->parse($message . "\r\n")->toArray();

        if (isset($result['user'])) {
            $this->assertInstanceOf(
                User::class,
                $actual['user']
            );
        }

        unset($actual['user']);
        unset($result['user']);

        $this->assertEquals(
            $result,
            $actual
        );
    }

    /**
     * @return \Generator
     *
     * Note: Don't Add The \r\n To The String, Added In Test.
     */
    public function dpTestParser(): Generator
    {
        yield 'List' => [
            ':irc.example.com CAP * LIST :',
            [
                'adapter' => 'CAP',
                'user' => [
                    'source' => 'irc.example.com'
                ],
                'params' => [
                    '*',
                    'LIST',
                    ''
                ],
                'tags' => []
            ]
        ];

        yield 'LS' => [
            'CAP * LS :multi-prefix sasl',
            [
                'adapter' => 'CAP',
                'params' => [
                    '*',
                    'LS',
                    'multi-prefix sasl'
                ],
                'tags' => []
            ]
        ];

        yield 'REQ' => [
            'CAP REQ :sasl message-tags foo',
            [
                'adapter' => 'CAP',
                'params' => [
                    'REQ',
                    'sasl message-tags foo'
                ],
                'tags' => []
            ]
        ];


        yield 'Channel Message' => [
            ':dan!d@localhost PRIVMSG #chan :Hey!',
            [
                'adapter' => 'PRIVMSG',
                'user' => [
                    'source' => 'dan!d@localhost'
                ],
                'params' => [
                    '#chan',
                    'Hey!'
                ],
                'tags' => []
            ]
        ];

        yield 'Channel Message Alt' => [
            ':dan!d@localhost PRIVMSG #chan Hey!',
            [
                'adapter' => 'PRIVMSG',
                'user' => [
                    'source' => 'dan!d@localhost'
                ],
                'params' => [
                    '#chan',
                    'Hey!'
                ],
                'tags' => []
            ]
        ];


        yield 'Tags' => [
            '@id=123AB;rose :source PRIVMSG #chan Hey!',
            [
                'adapter' => 'PRIVMSG',
                'user' => [
                    'source' => 'source'
                ],
                'params' => [
                    '#chan',
                    'Hey!'
                ],
                'tags' => [
                    'id' => '123AB',
                    'rose' => true
                ]
            ]
        ];

        yield 'Tags Alt' => [
            '@url=;netsplit=tur,ty :source PRIVMSG #chan Hey!',
            [
                'adapter' => 'PRIVMSG',
                'user' => [
                    'source' => 'source'
                ],
                'params' => [
                    '#chan',
                    'Hey!'
                ],
                'tags' => [
                    'url' => '',
                    'netsplit' => 'tur,ty'
                ]
            ]
        ];
    }
}
