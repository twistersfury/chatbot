<?php

namespace TwistersFury\ChatBot\Tests\Unit\Irc\Packet;

use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\ChatBot\Irc\Packet\PrivateMessage;

class PrivateMessageTest extends Unit
{
    /** @var PrivateMessage|MockObject */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(PrivateMessage::class)
                                  ->onlyMethods(['getMessage'])
                                  ->disableOriginalConstructor()
                                  ->getMockForAbstractClass();

    }

    public function testGetEventName()
    {
        $this->assertEquals('packet:onincoming', $this->testSubject->getEventName());
    }
}
