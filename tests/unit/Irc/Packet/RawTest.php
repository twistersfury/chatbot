<?php

namespace TwistersFury\ChatBot\Tests\Unit\Irc\Packet;

use Codeception\Stub;
use Codeception\Test\Unit;
use Generator;
use Phalcon\Config\Config;
use PHPUnit\Framework\MockObject\MockObject;
use TwistersFury\ChatBot\Irc\Packet\Raw;

class RawTest extends Unit
{
    /** @var Raw|MockObject */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Raw::class)
                                  ->onlyMethods(['getMethod', 'getMessage'])
                                  ->disableOriginalConstructor()
                                  ->getMockForAbstractClass();

        $this->testSubject->expects($this->never())->method('getMethod');
    }

    /**
     * @dataProvider dpTestBuildMessage
     */
    public function testBuildMessage(string $message, string $result)
    {
        $this->testSubject->expects($this->once())->method('getMessage')->willReturn($message);

        $this->assertEquals($result, $this->testSubject->buildPacket());
    }

    public function dpTestBuildMessage(): Generator
    {
        yield 'Normal Message' => [
            'Normal Message',
            'Normal Message'
        ];
    }
}
