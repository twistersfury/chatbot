<?php

namespace TwistersFury\ChatBot\Tests\Unit;

use Codeception\Stub;
use Phalcon\Config\Config;
use Phalcon\Logger\Logger;
use TwistersFury\ChatBot\Kernel;
use Codeception\Test\Unit;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

class KernelTest extends Unit
{
    /** @var Kernel */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $di = new FactoryDefault();

        $di->set('config', new Config(['modules' => []]));
        $di->set('logger', Stub::makeEmpty(Logger::class));

        $this->testSubject = new Kernel($di, codecept_root_dir());
    }

    public function testGetApplication()
    {
        $this->assertInstanceOf(Application::class, $this->testSubject->getApplication());
    }
}
