<?php

namespace TwistersFury\ChatBot\Tests\Unit\Support;

use Codeception\Test\Unit;
use TwistersFury\ChatBot\Support\Debug;

class DebugTest extends Unit
{
    /** @var Debug */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Debug();
    }

    public function testDi()
    {
        $this->assertInstanceOf(Debug::class, $this->testSubject);
    }
}
