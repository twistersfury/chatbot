namespace TwistersFury\ChatBot\Cli\Job;

use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Di\Injectable;
use Phalcon\Events\Exception as EventException;
use Phalcon\Events\EventsAwareInterface;
use TwistersFury\ChatBot\Command\CommandInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\ErrorInterface;
use TwistersFury\ChatBot\Connection\Packet\UnknownInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\HasCommands;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\HasEvent;
use TwistersFury\ChatBot\Connection\Manager;
use TwistersFury\Phalcon\Queue\Job\JobInterface;
use TwistersFury\ChatBot\Command\Factory;

class Packet extends Injectable implements JobInterface
{
    private packet {
        get
    };

    public function __construct(<Packet> packet)
    {
        let this->packet = packet;
    }

    public function __sleep() -> array
    {
        return [
            "packet"
        ];
    }

    public function __wakeup() -> void
    {
        this->setDi(Di::getDefault());
    }

    public function handle() -> <JobInterface>
    {
        this->{"logger"}->info("Packet Job Processing Packet");

        try {
            this->processEvents();
        } catch EventException {
            this->{"logger"}->debug("Event Exception");
        }

        try {
            this->handleJobs();
        } catch Invalid {
            this->{"logger"}->debug("Invalid Command");
        }

        return this;
    }

    private function handleJobs() -> <Packet>
    {
        this->{"logger"}->info("Packet Job - Handle Jobs");

        var jobs, job;

        let jobs = this->parsePacket();

        for job in jobs {
            this->{"queue"}->put(
                job,
                job->getQueueOptions()
            );
        }

        return this;
    }

    private function processEvents() -> <Packet>
    {
        if !(this->getPacket() instanceof HasEvent) {
            return this;
        }

        this->{"logger"}->debug(
            "Packet Job - Fire Events",
            [
                "class": get_class(this->getPacket())
            ]
        );

        //Channel Specific Events
//        this->getPacket()->getChannelConfig()->getEventsManager()->fire(
//            this->getPacket()->getEventName(),
//            this->getPacket()
//        );

        //Global Events
        this->getDi()->get("eventsManager")->fire(
            this->getPacket()->getEventName(),
            this->getPacket()
        );

        return this;
    }

    private function parsePacket() -> array
    {
        if this->getPacket() instanceof HasCommands {
            this->{"logger"}->debug(
                "Parsing Packet",
                [
                    "message": this->getPacket()->getMessage(),
                    "packet": this->getPacket()
                ]
            );

            return this->getPacket()->buildCommands();
        }

        return [];
    }



    public function getOptions() -> array
    {
        return [];
    }
}
