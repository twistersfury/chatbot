namespace TwistersFury\ChatBot\Cli\Job;

use TwistersFury\Phalcon\Queue\Job\ExitJob;
use TwistersFury\Phalcon\Queue\Job\JobInterface;

class QuitJob extends ExitJob
{
    public function handle() -> <JobInterface>
    {
        this->{"logger"}->notice(
            "Initializing Exit"
        );

        this->{"irc"}->put(this);

        return parent::handle();
    }
}
