namespace TwistersFury\ChatBot\Cli\Task;

use Exception;
use Phalcon\Cli\Task;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\ErrorInterface;
use TwistersFury\ChatBot\Connection\Packet\KeepAlive;
use TwistersFury\ChatBot\Connection\Packet\EmptyPacket;
use TwistersFury\ChatBot\Connection\Packet\UnknownInterface;
use TwistersFury\ChatBot\Connection\Manager;
use TwistersFury\Phalcon\Queue\Exceptions\ExitException;
use TwistersFury\Shared\Di\Interfaces\InitializationAware;

class ConnectTask extends Task implements InitializationAware
{
    private allowExit = false;
    private wait = 100;

    public function initialize() -> void
    {
        if this->getDi()->get("dispatcher")->getParam(0) {
            let this->wait = this->getDi()->get("dispatcher")->getParam(0);
        } elseif this->getDi()->get("config")->get("wait") {
            let this->wait = this->getDi()->get("config")->get("wait");
        }
    }

    public function mainAction() -> void
    {
        this->getDi()->get("logger")->info(
            "Connect Task Start",
            [
                "start": date("Y-m-d H:i:s")
            ]
        );

        this->getManager()->openConnection();

        var packet;

        while this->allowExit === false && this->getManager()->isConnected() {
            let packet = this->getManager()->readPacket();

            this->processPacket(packet);

            try {
                if this->getManager()->getOutgoingQueue()->peekReady() {
                    this->{"logger"}->info("Outgoing Queue Ready");

                    this->getDi()->get("kernel")->getApplication()->handle(
                        [
                            "task": "TwistersFury\\Phalcon\\Queue\\Cli\\Task\\Queue",
                            "action": "consume",
                            "params": [
                                this->getManager()->getDriverName(),
                                1
                            ]
                        ]
                    );
                }
            } catch ExitException {
                break;
            }

            if likely this->getWait() > 0 {
                usleep(this->getWait());
            }
        }

        var exception;

        try {
            this->getManager()->closeConnection();
        } catch Throwable, exception {
            this->getDi()->get("logger")->warning(
                "Error Closing Connection",
                [
                    "message": exception->getMessage()
                ]
            );
        }

        this->getDi()->get("logger")->info(
            "Connect Task Stop",
            [
                "start": date("Y-m-d H:i:s")
            ]
        );
    }

    private function processPacket(<Packet> packet) -> void
    {
        //If Error Packet, Exit, As the Connection has Likely Been Closed By Now
        if unlikely packet instanceof ErrorInterface {
            this->{"logger"}->error(packet->getMessage());
            let this->allowExit = true;

            return;
        //Process KeepAlive Packets Immediately
        } elseif packet instanceof KeepAlive {
            this->{"logger"}->debug("Keep Alive Packet Recieved");

            this->getManager()->keepAlive(packet);

            return;
        //The Driver Doesn't Know How To Process This Packet, So We're Going To Ignore It
        } elseif packet instanceof UnknownInterface {
            this->getDi()->get("logger")->notice("Unknown Packet");

            return;
        } elseif packet instanceof EmptyPacket {
            this->{"logger"}->debug("Empty Packet Recieved");

            return;
        }

        this->getDi()->get("logger")->info(
            "Packet Received",
            [
                "packet": serialize(packet)
            ]
        );

        this->{"queue"}->put(
            this->getDi()->get(
                "TwistersFury\\ChatBot\\Cli\\Job\\Packet",
                [
                    packet
                ]
            )
        );
    }

    private function getManager() -> <Manager>
    {
        return this->getDi()->get("connectionManager");
    }

    protected function getWait() -> int
    {
        return this->wait;
    }
}
