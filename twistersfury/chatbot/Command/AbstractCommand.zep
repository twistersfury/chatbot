namespace TwistersFury\ChatBot\Command;

use TwistersFury\ChatBot\Connection\Manager;
use TwistersFury\ChatBot\Connection\Packet\AbstractFactory;
use TwistersFury\ChatBot\Connection\Packet\Builder;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\Phalcon\Queue\Job\JobInterface;
use TwistersFury\Phalcon\Queue\Job\AbstractJob;

abstract class AbstractCommand extends AbstractJob implements CommandInterface
{
    protected function handleMessage(<OutgoingPacket> message) -> <AbstractCommand>
    {
        this->getPacketBuilder()->handleMessage(message);

        return this;
    }

    protected function sendMessage(string message) -> <AbstractCommand>
    {
        this->getPacketBuilder()->sendMessage(
            message,
            this->getPacket()
        );

        return this;
    }

    protected function getPacketBuilder() -> <Builder>
    {
        return this->getDi()->get("packetBuilder");
    }

    public function handle() -> <JobInterface>
    {
        return this->processCommand();
    }

    public function getQueueOptions() -> array
    {
        return [];
    }

    public function getPacket() -> <Packet>
    {
        return this->getConfig()->packet;
    }

    /**
     * Note: Per PHP Docs, this will not be called if the class defines __serialize
     */
    public function serialize() -> string
    {
        return serialize(this->getConfig());
    }

    public function unserialize(var data) -> void
    {
        this->setConfig(
            unserialize(data)
        );
    }

    abstract protected function processCommand() -> <AbstractCommand>;
}
