namespace TwistersFury\ChatBot\Command;

use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;

class Boom extends AbstractCommand
{
    protected function processCommand() -> <AbstractCommand>
    {
        this->{"logger"}->debug("Boom Command");

        this->sendMessage("Baby!");

        return this;
    }
}
