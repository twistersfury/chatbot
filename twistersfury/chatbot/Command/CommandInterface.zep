namespace TwistersFury\ChatBot\Command;

use TwistersFury\Phalcon\Queue\Job\JobInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;

interface CommandInterface extends JobInterface
{
    public function getQueueOptions() -> array;
    public function getPacket() -> <Packet>;
}
