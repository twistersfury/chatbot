namespace TwistersFury\ChatBot\Command;

use InvalidArgumentException;
use Phalcon\Config\Config;
use Phalcon\Di\DiInterface;
use TwistersFury\ChatBot\Factory\AbstractFactory;

class Factory extends AbstractFactory
{
    /**
     * Returns the adapters for the factory
     */
    protected function getServices() -> array
    {
        return array_merge(
            [
                "HELLO" : "TwistersFury\\Chatbot\\Command\\Hello",
                "BOOM" : "TwistersFury\\Chatbot\\Command\\Boom"
            ],
            this->getConfiguredCommands()
        );
    }

    public function getInterfaceName() -> string
    {
        return "TwistersFury\\ChatBot\\Command\\CommandInterface";
    }

    protected function getConfiguredCommands() -> array
    {
        var commands = [];

        if this->getDi()->get("config")->has("chatCommands") {
            let commands = this->getDi()->get("config")->chatCommands->toArray();
        }

        return commands;
    }
}
