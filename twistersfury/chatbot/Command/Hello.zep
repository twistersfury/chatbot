namespace TwistersFury\ChatBot\Command;

use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;

class Hello extends AbstractCommand
{
    protected function processCommand() -> <AbstractCommand>
    {
        this->{"logger"}->debug("Hello Command");

        this->sendMessage("World!");

        return this;
    }
}
