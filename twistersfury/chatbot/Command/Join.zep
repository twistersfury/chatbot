namespace TwistersFury\ChatBot\Command;

use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\Builder;

class Join extends AbstractCommand
{
    protected function processCommand() -> <AbstractCommand>
    {
        this->{"logger"}->debug(
            "Join Command",
            [
                "channel": this->getConfig()->channel
            ]
        );

        this->handleMessage(
            this->getPacketBuilder()->buildJoinPacket(
                this->getPacket(),
                this->getConfig()->channel
            )
        );

        return this;
    }
}
