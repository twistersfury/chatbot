namespace TwistersFury\ChatBot\Command;

use Phalcon\Di\Injectable;
use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;

class Parser extends Injectable
{
    public function parse(<Packet> packet) -> <Config>
    {
        var message, command;

        let message = packet->getMessage();

        if likely substr(message, 0, 1) === "!" {
            let message = substr(message, 1);
        }

        //TODO: Add Logic To Parse Complex Items (IE: Quotes)

        let command = this->parseSimple(message);

        let command["packet"] = packet;

        this->{"logger"}->debug("Parsed Message");

        return this->getDi()->get(
            "Phalcon\Config\Config",
            [
                command
            ]
        );
    }

    private function parseSimple(string! message) -> array
    {
        var splitMessage = explode(' ', message, 2);

        return [
            "adapter": strtoupper(splitMessage[0]),
            "params": explode(' ', splitMessage[1] ? splitMessage[1] : "")
        ];
    }
}
