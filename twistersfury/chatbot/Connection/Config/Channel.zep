namespace TwistersFury\ChatBot\Connection\Config;

use Phalcon\Config\ConfigInterface;
use Phalcon\Events\EventsAwareInterface;
use Phalcon\Events\ManagerInterface;
use TwistersFury\Shared\Di\Injectable;
use TwistersFury\Shared\Di\Interfaces\InitializationAware;

class Channel extends Injectable implements EventsAwareInterface, InitializationAware
{
    private channelName {
        get
    };

    /**
     * @var ConfigInterface
     */
    private config;

    /**
      * @var ManagerInterface
      */
    private eventsManager;

    public function __construct(string! channelName, <ConfigInterface> baseConfig)
    {
        let this->channelName = channelName;
        let this->config = baseConfig;
    }

    public function getEventsManager() -> <ManagerInterface>
    {
        return this->eventsManager;
    }

    public function setEventsManager(<ManagerInterface> eventsManager) -> void
    {
        let this->eventsManager = eventsManager;
    }

    public function initialize() -> void
    {
        this->setEventsManager(this->getDi()->get("Phalcon\\Events\\Manager"));
        this->loadListeners();
    }

    private function loadListeners() -> <Channel>
    {
        if this->getConfig()->has("listeners") {
            var listener, instance;

            for listener in this->getConfig()->listeners->getIterator() {
                let instance = this->getDi()->get("eventsManager")->attach(
                    listener->event,
                    this->getDi()->get(listener->listener)
                );
            }
        }

        return this;
    }

    public function getConfig() -> <ConfigInterface>
    {
        return this->config;
    }
}
