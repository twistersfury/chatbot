namespace TwistersFury\ChatBot\Connection\Config;

use Phalcon\Config\Config;
use TwistersFury\Shared\Di\Injectable;

class Manager extends Injectable
{
    private channelConfigs = [];

    public function get(string! channelName)
    {
        let channelName = this->normalize(channelName);

        if !isset this->channelConfigs[channelName] {
            let this->channelConfigs[channelName] = this->loadConfig(channelName);
        }

        return this->channelConfigs;
    }

    private function loadConfig(string! channelName) -> <Channel>
    {
        var baseConfig;

        let baseConfig = this->loadBaseConfig(channelName);

        return this->getDi()->get(
            "Phalcon\\Config\\Config",
            [
                channelName,
                baseConfig
            ]
        );
    }

    private function loadBaseConfig(string! channelName) -> <Config>
    {
        if unlikely !this->getConfig()->has("channels") || !this->getConfig()->channels->has(channelName) {
            return this->getDi()->get("Phalcon\\Config\\Config");
        }

        return this->getConfig()->channels->get(channelName);
    }

    private function normalize(string! channelName) -> string
    {
        return this->getDi()->get("helper")->friendly(channelName);
    }
}
