namespace TwistersFury\ChatBot\Connection\Driver;

use TwistersFury\ChatBot\Connection\Packet\AbstractFactory;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;

interface DriverInterface
{
    public function openConnection() -> <DriverInterface>;
    public function closeConnection() -> <DriverInterface>;
    public function sendPacket(<OutgoingPacket> packet) -> <DriverInterface>;
    public function readPacket() -> <Packet>;
    public function keepAlive(<IncomingPacket> packet) -> <DriverInterface>;
    public function isConnected() -> bool;
    public function getPacketFactory() -> <AbstractFactory>;
    public function getConnectionName() -> string;
    public function initialize() -> <DriverInterface>;
}
