namespace TwistersFury\ChatBot\Connection\Driver;

use InvalidArgumentException;
use Phalcon\Config\Config;
use TwistersFury\ChatBot\Factory\AbstractFactory;

class Factory extends AbstractFactory
{
    /**
     * Returns the adapters for the factory
     */
    protected function getServices() -> array
    {
        return [
            "irc" : "TwistersFury\\Chatbot\\Irc\\Driver\\Classic",
            "twitch" : "TwistersFury\\ChatBot\\Twitch\\Driver\\Irc"
        ];
    }

    public function getInterfaceName() -> string
    {
        return "TwistersFury\\ChatBot\\Connection\\Driver\\DriverInterface";
    }
}
