namespace TwistersFury\ChatBot\Connection\Job;

use TwistersFury\Phalcon\Queue\Job\AbstractJob;
use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Direct;
use TwistersFury\Phalcon\Queue\Job\JobInterface;

class SendPacket extends AbstractJob
{
    public function handle() -> <JobInterface>
    {
        this->{"connectionManager"}->buildAndSend(this->getConfig()->message);

        return this;
    }

    public function serialize() -> string
    {
        return serialize(this->getConfig());
    }

    public function unserialize(var data) -> void
    {
        this->setConfig(
            unserialize(data)
        );
    }
}
