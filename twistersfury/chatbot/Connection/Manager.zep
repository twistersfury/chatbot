namespace TwistersFury\ChatBot\Connection;

use Phalcon\Config\Config;
use Phalcon\Di\Injectable;
use Phalcon\Events\EventsAwareInterface;
use Phalcon\Events\ManagerInterface;
use TwistersFury\ChatBot\Connection\Driver\DriverInterface;
use TwistersFury\ChatBot\Connection\Driver\Exceptions\MissingDriverConfig;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\AbstractFactory;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\Phalcon\Queue\Adapter\Beanstalk;

class Manager extends Injectable implements EventsAwareInterface
{
    private config;
    private driver;
    private driverName;

    private eventsManager;

    public function getEventsManager() -> <ManagerInterface> | null
    {
        if unlikely this->eventsManager === null {
            this->setEventsManager(this->getDi()->get("eventsManager"));
        }

        return this->eventsManager;
    }

    public function setEventsManager(<ManagerInterface> eventsManager) -> void
    {
        let this->eventsManager = eventsManager;
    }

    public function __construct(<Config> config)
    {
        let this->config = config;
    }

    public function getDriver() -> <DriverInterface>
    {
        if this->driver === null {
            let this->driver = this->loadDriver();
        }

        return this->driver;
    }

    public function getDriverName() -> string
    {
        if this->driverName === null {
            let this->driverName = this->loadDriverName();
        }

        return this->driverName;
    }

    private function loadDriverName() -> string
    {
        var driver = this->getConfig()->driver;

        if unlikely !this->getConfig()->has(driver) {
            throw new MissingDriverConfig("Configuration Does Not Exist: " . driver);
        }

        return driver;
    }

    public function getConfig() -> <Config>
    {
        return this->config;
    }

    private function loadDriver() -> <DriverInterface>
    {
        var driver = this->getDriverName();

        this->getDi()->get("logger")->debug(
            "Initializing Driver",
            [
                "driver": driver,
                "config": this->getConfig()->get(driver)->toArray()
            ]
        );

        return this->getDi()->get("driverFactory")->load(
            this->getConfig()->get(driver)
        );
    }

    public function sendPacket(<Packet> packet) -> <Manager>
    {
        this->getEventsManager()->fire(
            "driver:sendPacket",
            this,
            [
                "packet": packet
            ]
        );

        this->getDi()->get("logger")->debug("Sending Packet");

        this->getDriver()->sendPacket(packet);

        return this;
    }

    public function readPacket() -> <Interfaces\Packet>
    {
        return this->getDriver()->readPacket();
    }

    public function openConnection() -> <Manager>
    {
        this->getEventsManager()->fire(
            "driver:openConnection",
            this
        );

        this->getDi()->get("logger")->debug("Open Connection");

        this->getDriver()->openConnection();

        return this;
    }

    public function closeConnection() -> <Manager>
    {
        this->getDriver()->closeConnection();

        return this;
    }

    public function isConnected() -> bool
    {
        return this->getDriver()->isConnected();
    }

    public function keepAlive(<Packet> packet) -> <Manager>
    {
        this->getDriver()->keepAlive(packet);

        return this;
    }

    public function buildAndSend(<Packet> message) -> <Manager>
    {
        this->getDi()->get("logger")->debug("Build and Send");

        var sendPacket;

        if message instanceof OutgoingPacket {
            let sendPacket = message;
        } else {
            let sendPacket = this->buildPacket(message);
        }

        return this->sendPacket(
            sendPacket
        );
    }

    private function buildPacket(<IncomingPacket> message) -> <OutgoingPacket>
    {
        return this->getDriver()->buildPacket(message);
    }

    public function getPacketFactory() -> <AbstractFactory>
    {
        return this->getDriver()->getPacketFactory();
    }

    public function getOutgoingQueue() -> <Beanstalk>
    {
        return this->getDi()->get(this->getDriverName());
    }
}
