namespace TwistersFury\ChatBot\Connection\Packet;

use Phalcon\Di\DiInterface;
use TwistersFury\ChatBot\Factory\AbstractFactory as BaseAbstractFactory;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;

abstract class AbstractFactory extends BaseAbstractFactory
{
    const OUTGOING_CHANNEL = "_CHANNEL";
    const OUTGOING_DIRECT  = "_DIRECT";

    protected function buildResponsePacket(<IncomingPacket> packet, string! type = null)
    {
        if packet->isChannel() {
            return this->quickLoad(AbstractFactory::OUTGOING_CHANNEL);
        }

        return this->quickLoad(AbstractFactory::OUTGOING_DIRECT);
    }

    protected function buildJoinPacket(<IncomingPacket> packet, string channel)
    {
        return this->getDi()->get(
            "TwistersFury\\ChatBot\\Connection\\Packet\\Join",
            [
                this->getDi()->get(
                    "Phalcon\\Config\\Config",
                    [[
                        "channel": channel
                    ]]
                )
            ]
        );
    }
}
