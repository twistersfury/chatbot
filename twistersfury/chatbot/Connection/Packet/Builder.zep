namespace TwistersFury\ChatBot\Connection\Packet;

use Phalcon\Config\Config;
use Phalcon\Di\Injectable;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Join;
use TwistersFury\ChatBot\Connection\Job\SendPacket;
use TwistersFury\ChatBot\Connection\Manager;

class Builder extends Injectable
{
    public function handleMessage(<OutgoingPacket> message) -> <AbstractCommand>
    {
        this->{"logger"}->debug(
            "handleMessage",
            [
                "connection": message->getConnection()
            ]
        );

        var job = this->buildJob(
            [
                "message": message
            ]
        );

        this->getDi()->get(message->getConnection())->put(
            job
        );

        return this;
    }

    public function sendMessage(string message, <IncomingPacket> packet) -> <Builder>
    {
        return this->handleMessage(
            this->buildResponsePacket(packet)
                ->setMessage(message)
        );
    }

    protected function buildJob(array options) -> <SendPacket>
    {
        return this->getDi()->get(
            "TwistersFury\\ChatBot\\Connection\\Job\\SendPacket",
            [
                this->getDi()->get(
                    "Phalcon\Config\Config",
                    [
                        options
                    ]
                )
            ]
        );
    }

    public function buildResponsePacket(<IncomingPacket> packet) -> <OutgoingPacket>
    {
        this->{"logger"}->debug(
            "Builder::buildResponsePacket",
            [
                "class": get_class(packet)
            ]
        );

        return this->getManager()->getPacketFactory()->buildResponsePacket(packet)
            ->setTo(packet->getFrom()->getDisplayName())
            ->setConnection(packet->getConnection());
    }

    public function buildPacket(string! type, <Config> config) -> <OutgoingPacket>
    {
        return this->getManager()->getPacketFactory()->load(
            type,
            config
        );
    }

    public function buildJoinPacket(<IncomingPacket> packet, string! channel) -> <Join>
    {
        return this->getManager()->getPacketFactory()->buildJoinPacket(packet, channel)
            ->setConnection(packet->getConnection());
    }

    protected function getManager() -> <Manager>
    {
        return this->getDi()->get("connectionManager");
    }
}
