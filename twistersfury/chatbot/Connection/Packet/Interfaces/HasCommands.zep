namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface HasCommands
{
    public function buildCommands() -> array;
}
