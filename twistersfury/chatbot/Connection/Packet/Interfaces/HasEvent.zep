namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

use Phalcon\Events\EventsAwareInterface;

interface HasEvent extends EventsAwareInterface
{
    public function getEventName() -> string;
}
