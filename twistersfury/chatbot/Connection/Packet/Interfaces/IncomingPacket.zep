namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface IncomingPacket extends Packet
{
    public function isChannel() -> bool;
    public function getFrom() -> <User>;
    public function getFromUser() -> <User>;
}
