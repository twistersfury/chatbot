namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface Join extends OutgoingPacket
{
    public function getChannel() -> string;
}
