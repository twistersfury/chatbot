namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface Message
{
    public function getMessage() -> string;
    public function getQueueOptions() -> array;
}
