namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface OutgoingPacket extends Packet
{
    public function buildPacket() -> string;
}
