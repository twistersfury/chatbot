namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface Packet
{
    public function getMessage() -> string;
    public function getConnection() -> string;
    public function setConnection(string! connection) -> <Packet>;
}
