namespace TwistersFury\ChatBot\Connection\Packet\Interfaces;

interface User extends Packet
{
    public function getDisplayName() -> string;
    public function getName() -> string;
}
