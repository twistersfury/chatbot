namespace TwistersFury\ChatBot\Connection\Packet;

use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Join as JoinInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet as PacketInterface;
use TwistersFury\Shared\Di\AbstractConfigInjectable;

class Join extends AbstractConfigInjectable implements JoinInterface
{
    public function getChannel() -> string
    {
        return this->getConfig()->channel;
    }

    public function buildPacket() -> string
    {
        return "JOIN " . this->getChannel();
    }

    public function getConnection() -> string
    {
        return this->getConfig()->connection;
    }

    public function getMessage() -> string
    {
        return this->buildPacket();
    }

    public function setConnection(string! connection) -> <PacketInterface>
    {
        this->getConfig()->set("connection", connection);

        return this;
    }
}
