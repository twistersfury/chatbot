namespace TwistersFury\ChatBot\Connection\Packet;

use Phalcon\Config\Config;
use LogicException;
use TwistersFury\Shared\Di\AbstractConfigInjectable;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\User;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet as PacketInterface;

class Packet extends AbstractConfigInjectable implements PacketInterface
//implements PacketInterface
{
    private connection = "";

    public function getRawMessage() -> string
    {
        return this->getConfig()->raw;
    }

    public function getMessage() -> string
    {
        return this->getConfig()->message;
    }

    public function getFrom() -> <User>
    {
        return this->getConfig()->user;
    }

    public function buildPacket() -> string
    {
        return "";
    }

    public function getConnection() -> string
    {
        return this->connection;
    }

    public function setConnection(string! connection) -> <PacketInterface>
    {
        let this->connection = connection;

        return this;
    }

    /**
     * Note: Per PHP Docs, this will not be called if the class defines __serialize
     */
    public function serialize() -> string
    {
        return serialize(this->getConfig());
    }

    public function unserialize(var data) -> void
    {
        this->setConfig(
            unserialize(data)
        );
    }

    public function isChannel() -> bool
    {
        return false;
    }
}
