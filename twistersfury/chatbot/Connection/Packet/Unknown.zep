namespace TwistersFury\ChatBot\Connection\Packet;

class Unknown implements UnknownInterface
{
    private connection;

    public function setConnection(string! connection) -> void
    {
        let this->connection = connection;
    }
}

