namespace TwistersFury\ChatBot\Connection\Packet;

use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet as PacketInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\User as UserInterface;
use TwistersFury\Shared\Di\AbstractConfigInjectable;

class User extends AbstractConfigInjectable implements UserInterface
{
    public function getDisplayName() -> string
    {
        return this->getConfig()->displayName;
    }

    public function getName() -> string
    {
        return this->getDisplayName();
    }

    public function getMessage() -> string
    {
        return "";
    }

    public function getConnection() -> string
    {
        return this->getConfig()->connection;
    }

    public function setConnection(string! connection) -> <PacketInterface>
    {
        this->getConfig()->set("connection", connection);

        return this;
    }
}
