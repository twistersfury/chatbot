namespace TwistersFury\ChatBot\Connection\Throttle\Exceptions;

use TwistersFury\Phalcon\Queue\Exceptions\NonFatalException;

class Throttled extends NonFatalException
{
    protected message = "Throttle Limit Reached";
}
