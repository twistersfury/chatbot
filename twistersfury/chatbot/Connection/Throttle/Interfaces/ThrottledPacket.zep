namespace TwistersFury\ChatBot\Connection\Throttle\Interfaces;

use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;

interface ThrottledPacket extends OutgoingPacket
{
    public function getTo() -> string;
}
