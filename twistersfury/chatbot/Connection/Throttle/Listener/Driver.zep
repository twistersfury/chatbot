namespace TwistersFury\ChatBot\Connection\Throttle\Listener;

use Phalcon\Di\Injectable;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Throttle\Interfaces\ThrottledPacket;
use TwistersFury\ChatBot\Connection\Throttle\Exceptions\Throttled;
use TwistersFury\ChatBot\Connection\Throttle\Manager;

class Driver extends Injectable
{
    public function __construct()
    {
        \Phalcon\Di\Di::getDefault()->get("logger")->debug("Driver Throttle");
    }

    public function sendPacket(<Event> event, <Manager> manger, array data = []) -> void
    {
        var packet;

        if !(fetch packet, data["packet"]) {
            return;
        } elseif !(packet instanceof ThrottledPacket) {
            return;
        }

        this->getDi()->get("logger")->debug("driver:sendPacket");

        if !this->getThrottleManager()->canSend(packet) {
            throw new Throttled();
        }
    }

    private function getThrottleManager() -> <Manager>
    {
        return this->getDi()->get("throttleManager");
    }
}
