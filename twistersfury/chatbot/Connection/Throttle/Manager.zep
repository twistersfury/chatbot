namespace TwistersFury\ChatBot\Connection\Throttle;

use Phalcon\Config\Config;
use Phalcon\Di\Injectable;
use TwistersFury\ChatBot\Connection\Throttle\Interfaces\ThrottledPacket;

class Manager extends Injectable
{
    protected trackers = [];

    public function getTracker(string channel) -> <Tracker>
    {
        var tracker;

        if !(fetch tracker, this->getTrackers()[channel]) {
            let tracker = this->buildTracker(channel);

            let this->trackers[channel] = tracker;
        }

        return tracker;
    }

    public function getTrackers() -> array
    {
        return this->trackers;
    }

    public function canSend(<ThrottledPacket> packet) -> bool
    {
        var canSend = this->getTracker(packet->getTo())->canSend(packet);

        this->getDi()->get("logger")->debug(
            "Throttle Can Send Check",
            [
                "channel": packet->getTo(),
                "canSend": canSend
            ]
        );

        return canSend;
    }

    protected function buildTracker(string! channel) -> <Tracker>
    {
        var config = this->getDi()->get("config")->throttle->defaults;

        if this->getDi()->get("config")->throttle->has(channel) {
            config->merge(this->getDi()->throttle->get(channel));
        }

        return this->getDi()->get(
            "TwistersFury\\ChatBot\\Connection\\Throttle\\Tracker",
            [
                config
            ]
        );
    }
}
