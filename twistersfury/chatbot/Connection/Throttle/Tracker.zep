namespace TwistersFury\ChatBot\Connection\Throttle;

use DateInterval;
use DateTime;
use TwistersFury\ChatBot\Connection\Throttle\Interfaces\ThrottledPacket;
use TwistersFury\Shared\Di\AbstractConfigInjectable;

class Tracker extends AbstractConfigInjectable
{
    const CACHE_KEY = "TwistersFury.ChatBot.Connection.Throttle";

    private throttleLog = [];
    private throttleCount = 0;
    private messageLog = [];

    private filterDate;

    public function getMaxCount() -> int
    {
        return this->getConfig()->count;
    }

    public function getCacheTtl() -> int
    {
        return 3;
    }

    public function getSeconds() -> int
    {
        return this->getConfig()->seconds;
    }

    public function getCount() -> int
    {
        return this->throttleCount;
    }

    public function canSend(<ThrottledPacket> packet) -> bool
    {
        this->getDi()->get("logger")->debug("canSend");

        if this->isThrottledByCount() || this->isThrottledByMessage(packet->getMessage()) {
            return false;
        }

        let this->throttleLog[] = this->getCurrentTime();

        return true;
    }

    protected function isThrottledByMessage(string! message) -> bool
    {
        var cacheKey = this->buildCacheKey(message);

        this->getDi()->get("logger")->debug(
            "Tracker:isThrottledByMessage",
            [
                "cacheKey": cacheKey,
                "hasKey": this->getDi()->get("cache")->has(cacheKey),
                "ttl": this->getCacheTtl()
            ]
        );

        if this->getDi()->get("cache")->has(cacheKey) {
            return true;
        }

        if !this->getDi()->get("cache")->set(
            cacheKey,
            this->getCurrentTime()->format("Y-m-d H:i:s"),
            this->getCacheTtl()
        ) {
            throw new \Exception("Failed To Save Cache");
        }

        return false;
    }

    protected function buildCacheKey(string! message) -> string
    {
        return Tracker::CACHE_KEY . "." . md5(message);
    }

    protected function isThrottledByCount() -> bool
    {
        this->setFilterDate(this->buildFilterDate());

        this->filterLog();

        return this->getCount() > this->getMaxCount();
    }

    protected function getCurrentTime() -> <DateTime>
    {
        return this->getDi()->get("DateTime");
    }

    protected function getFilterDate() -> <DateTime>
    {
        return this->filterDate;
    }

    protected function setFilterDate(<DateTime> filterDate) -> <Tracker>
    {
        let this->filterDate = filterDate;

        return this;
    }

    private function filterLog() -> <Driver>
    {
        this->getDi()->get("logger")->debug("filter");

        let this->throttleLog = array_filter(
            this->throttleLog,
            [this, "filterDate"]
        );

        let this->throttleCount = count(this->throttleLog);

        return this;
    }

    protected function filterDate(<DateTime> sentAt) -> bool
    {
        this->getDi()->get("logger")->debug(
            "Tracker:filterDate",
            [
                "sentAt": sentAt->format("Y-m-d H:i:s"),
                "filterDate": this->getFilterDate()->format("Y-m-d H:i:s")
            ]
        );

        return sentAt > this->getFilterDate();
    }

    protected function buildFilterDate() -> <DateTime>
    {
        this->getDi()->get("logger")->debug("buildFilter");
        var currentTime, pastInterval;

        let currentTime = new DateTime();
        let pastInterval = new DateInterval("PT" . this->getSeconds() . "S");

        currentTime->sub(pastInterval);

        return currentTime;
    }
}
