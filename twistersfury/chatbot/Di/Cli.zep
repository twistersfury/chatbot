namespace TwistersFury\ChatBot\Di;

use TwistersFury\Shared\Di\Cli as FactoryDefault;
use TwistersFury\ChatBot\Connection\Manager;
use Exception;

class Cli extends FactoryDefault
{
    public function initialize() -> void
    {
        parent::initialize();

        this->register(this->get("TwistersFury\\ChatBot\\Di\\ServiceProvider\\Connection"));
        this->register(this->get("TwistersFury\\ChatBot\\Di\\ServiceProvider\\Command"));
        this->register(this->get("TwistersFury\\Phalcon\\Queue\\Di\ServiceProvider\\Queue"));

        this->get("logger")->addAdapter(
            "stdout",
            this->get("Phalcon\\Logger\\Adapter\\Stream", ["php://stdout"])
        );
    }
}
