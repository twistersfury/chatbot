namespace TwistersFury\ChatBot\Di\ServiceProvider;

use Phalcon\Config\Config;
use TwistersFury\Shared\Di\ServiceProvider\AbstractServiceProvider;

class Command extends AbstractServiceProvider
{
    protected function registerFactory() -> void
    {
        this->{"setShared"}(
            "commandFactory",
            function () {
                return this->{"get"}("TwistersFury\\ChatBot\\Command\\Factory");
            }
        );
    }

    protected function registerParser() -> void
    {
        this->{"setShared"}(
            "commandParser",
            function () {
                return this->{"get"}("TwistersFury\\ChatBot\\Command\\Parser");
            }
        );
    }
}
