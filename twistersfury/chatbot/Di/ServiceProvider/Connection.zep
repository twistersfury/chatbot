namespace TwistersFury\ChatBot\Di\ServiceProvider;

use Phalcon\Config\Config;
use TwistersFury\Shared\Di\ServiceProvider\AbstractServiceProvider;

class Connection extends AbstractServiceProvider
{
    protected function registerConnectionManager() -> void
    {
        this->{"setShared"}(
            "connectionManager",
            function (<Config> config = null) {
                if config === null {
                    let config = this->{"getShared"}("config")->connection;
                }

                return this->{"get"}(
                    "TwistersFury\\ChatBot\\Connection\\Manager",
                    [
                        config
                    ]
                );
            }
        );
    }

    protected function registerFactory() -> void
    {
        this->{"setShared"}(
            "driverFactory",
            function () {
                return this->{"get"}("TwistersFury\ChatBot\Connection\Driver\Factory");
            }
        );
    }

    protected function registerPacketBuilder() -> void
    {
        this->{"setShared"}(
            "packetBuilder",
            function () {
                return this->{"get"}("TwistersFury\ChatBot\Connection\Packet\Builder");
            }
        );
    }

    protected function registerThrottleManager() -> void
    {
        this->{"setShared"}(
            "throttleManager",
            function () {
                return this->{"get"}("TwistersFury\\ChatBot\\Connection\\Throttle\\Manager");
            }
        );
    }

    protected function registerConfigManager() -> void
    {
        this->{"setShared"}(
            "configManager",
            function () {
                return this->{"get"}("TwistersFury\\ChatBot\\Connection\\Config\\Manager");
            }
        );
    }
}
