namespace TwistersFury\ChatBot\Factory;

use InvalidArgumentException;
use Phalcon\Config\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\Di;
use Exception;
use Phalcon\Factory\AbstractFactory as phFactory;
use Phalcon\Di\InjectionAwareInterface;
use TwistersFury\ChatBot\Connection\Driver\DriverInterface;

abstract class AbstractFactory extends phFactory implements InjectionAwareInterface
{
    protected diContainer;

    public function getDi() -> <DiInterface>
    {
        return this->diContainer;
    }

    public function setDi(<DiInterface> container) -> void
    {
        let this->diContainer = container;
    }

    public function __construct(array! services = [])
    {
        this->setDi(Di::getDefault());

        this->init(services);
    }

    public function load(<Config> config)
    {
        var exception;

        try {
            this->checkConfig(config);
        } catch Exception, exception {
            this->getDi()->get("logger")->debug(
                "Configuration Error",
                [
                    "config": config->toArray()
                ]
            );

            throw exception;
        }


        var driver;

        string adapter;

        let adapter = (string) config->adapter;

        try {
            let driver = this->getService(adapter);
        } catch Exception {
            let driver = config->adapter;
        }

        if !class_exists(driver) {
            throw new \Phalcon\Di\Exception("Driver Doesn't Exist: " . driver);
        }

        \Phalcon\Di\Di::getDefault()->get("logger")->debug(
            "Factory",
            [
                "driver": driver,
                "config": config->toArray()
            ]
        );

        var instance = create_instance_params(driver, [config]);

        if !is_object(instance) {
            throw new InvalidArgumentException("Invalid Adapter Name: " . config->adapter);
        } elseif !(instance instanceof this->getInterfaceName()) {
            throw new InvalidArgumentException("Adapter " . get_class(instance) . " Must Implement " . this->getInterfaceName());
        }

        \Phalcon\Di\Di::getDefault()->get("logger")->debug(
            "Factory Initialize",
            [
                "driver": driver,
                "initialize": method_exists(instance, "initialize")
            ]
        );

        if method_exists(instance, "initialize") {
            instance->initialize();
        }

        return instance;
    }

    public function quickLoad(string adapter)
    {
        return this->load(
            new Config(
                [
                    "adapter": adapter
                ]
            )
        );
    }

    abstract public function getInterfaceName() -> string;
}
