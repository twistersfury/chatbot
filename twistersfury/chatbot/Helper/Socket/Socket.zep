namespace TwistersFury\ChatBot\Helper\Socket;

use Phalcon\Config\Config;
use Phalcon\Di\Injectable;
use TwistersFury\ChatBot\Helper\Socket\ConnectionException;

class Socket extends Injectable
{
    private config;
    private handle;
    private rawSocket;

    private lastError;
    private lastErrorNumber;

    public function __construct(<Config> config)
    {
        let this->config = config;
    }

    public function connect() -> <Socket>
    {
        let this->handle = this->getRawSocket()->open(
            this->getConfig()->host,
            this->getConfig()->port,
            this->getConfig()->timeout ?: 30
        );

        if unlikely !this->isConnected() {
            this->raiseException();
        }

        return this;
    }

    public function close() -> <Socket>
    {
        this->getRawSocket()->close(this->handle);

        return this;
    }

    public function getConfig() -> <Config>
    {
        return this->config;
    }

    public function isConnected() -> bool
    {
        return this->handle && is_resource(this->handle) && this->getRawSocket()->eof(this->handle) !== true;
    }

    public function send(string message) -> <Socket>
    {
        if !this->isConnected() {
            this->raiseException();
        }

        this->getRawSocket()->write(this->handle, message);

        return this;
    }

    public function read() -> string
    {
        if !this->isConnected() {
            this->raiseException();
        }

        return this->getRawSocket()->get(this->handle);
    }

    private function raiseException() -> void
    {
        throw this->getDi()->get("TwistersFury\\ChatBot\\Helper\\Socket\\ConnectionException", [this->lastError, this->lastErrorNumber]);
    }

    private function getRawSocket() -> <Wrapper>
    {
        if unlikely !this->rawSocket {
            let this->rawSocket = this->getDi()->get("TwistersFury\\ChatBot\\Helper\\Socket\\Wrapper");
        }

        return this->rawSocket;
    }
}
