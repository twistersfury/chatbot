namespace TwistersFury\ChatBot\Helper\Socket;

use Phalcon\Di\Injectable;

class Wrapper extends Injectable
{
    private errorNo;
    private errorMessage;

    public function open(string host, int port, float timeout = null) -> resource
    {
        let this->errorMessage = null;
        let this->errorNo = null;

        var handle = fsockopen(
            host,
            port,
            this->errorNo,
            this->errorMessage,
            timeout
        );

        stream_set_blocking(handle, false);

        this->getDi()->get("logger")->debug(
            "Raw Socket Open",
            [
                "errorNo": this->errorNo,
                "errorMessage": this->errorMessage
            ]
        );


        return handle;
    }

    public function close(resource socket) -> bool
    {
        this->getDi()->get("logger")->debug("Raw Socket Close");

        return fclose(socket);
    }

    public function get(resource socket) -> string | bool
    {
        var message = fgets(
            socket
        );

        if message !== false {
            this->getDi()->get("logger")->debug(
                "Raw Socket Get",
                [
                    "message": message
                ]
            );
        }

        return message;
    }

    public function eof(resource socket) -> bool
    {
        var eof = feof(socket);

        //this->getDi()->get("logger")->debug("Raw Socket EOF", ["eof": eof]);

        return eof;
    }

    public function write(resource socket, string message) -> int
    {
        var result = fwrite(
            socket,
            message,
            strlen(message)
        );

        this->getDi()->get("logger")->debug(
            "Raw Socket Write",
            [
                "result": result,
                "message": message
            ]
        );

        return result;
    }
}
