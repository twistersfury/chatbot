namespace TwistersFury\ChatBot\Irc\Driver;

use Phalcon\Config\Config;
use Phalcon\Di\Exception as DiException;
use TwistersFury\ChatBot\Connection\Driver\DriverInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\ChatBot\Connection\Packet\AbstractFactory;
use TwistersFury\ChatBot\Irc\Packet\AbstractPacket;
use TwistersFury\ChatBot\Helper\Socket\Socket;

abstract class AbstractIrc extends Socket implements DriverInterface
{
    public function initialize() -> <DriverInterface>
    {
        this->getDi()->get("logger")->debug("AbstractIrc Initialize");

        this->{"connectionManager"}->getEventsManager()->attach(
            "driver",
            this->getDi()->get("TwistersFury\\ChatBot\\Connection\\Throttle\\Listener\\Driver")
        );

        return this;
    }

    public function getConnectionName() -> string
    {
        var name = this->getConfig()->get("name");

        return name;
    }

    public function openConnection() -> <DriverInterface>
    {
        this->connect();

        return this->sendPassword()
            ->sendUser()
            ->sendNick();
    }

    protected function sendNick() -> <AbstractIrc>
    {
        return this->sendPacket(
            this->getPacketFactory()->load(
                this->getDi()->get(
                    "Phalcon\Config\Config",
                    [[
                        "adapter": "NICK",
                        "nickname": this->getConfig()->user->nickname
                    ]]
                )
            )
        );
    }

    protected function sendPassword() -> <AbstractIrc>
    {
        if likely this->getConfig()->user->has("password") {
            this->sendPacket(
                this->getPacketFactory()->load(
                    this->getDi()->get(
                        "Phalcon\Config\Config",
                        [[
                            "adapter": "PASS",
                            "password": this->getConfig()->user->password
                        ]]
                    )
                )
            );
        }

        return this;
    }

    protected function sendUser() -> <AbstractIrc>
    {
        return this->sendPacket(
            this->getPacketFactory()->load(
                this->getDi()->get(
                    "Phalcon\Config\Config",
                    [[
                        "adapter": "USER",
                        "ident": this->getConfig()->user->ident,
                        "realname": this->getConfig()->user->realname
                    ]]
                )
            )
        );
    }

    public function closeConnection() -> <DriverInterface>
    {
        this->sendPacket(
            this->getPacketFactory()->load(
                this->getDi()->get(
                    "Phalcon\Config\Config",
                    [[
                        "adapter": "QUIT"
                    ]]
                )
            )
        );

        sleep(5);

        return this->close();
    }

    public function keepAlive(<IncomingPacket> packet) -> <DriverInterface>
    {
        this->getDi()->get("logger")->debug(
            "Keep Alive"
        );

        return this->sendPacket(
            this->getPacketFactory()->load(
                this->getDi()->get(
                    "Phalcon\Config\Config",
                    [[
                        "adapter": "PONG",
                        "packet": packet
                    ]]
                )
            )
        );
    }

    public function sendPacket(<OutgoingPacket> packet) -> <DriverInterface>
    {
        this->getDi()->get("logger")->debug("Send Packet");

        var message = packet->buildPacket();

        return this->send(
            message . "\r\n"
        );
    }

    public function readPacket() -> <Packet>
    {
        var message = this->read(),
            packet,
            exception;

        try {
            if !is_string(message) {
                let packet = this->getDi()->get(
                    "TwistersFury\\ChatBot\\Connection\\Packet\\EmptyPacket",
                    [
                        this->getDi()->get("Phalcon\Config\Config")
                    ]
                );
            } else {
                 let packet = this->getPacketFactory()->load(
                     this->getDi()->get("TwistersFury\\ChatBot\\Irc\\Packet\\Parser")->parse(message)
                 );
            }
        } catch DiException, exception {
            this->getDi()->get("logger")->notice(
                "Unknown Packet",
                [
                    "message": exception->getMessage()
                ]
            );

            let packet = this->getDi()->get(
                "TwistersFury\\ChatBot\\Connection\\Packet\\Unknown",
                [
                    this->getDi()->get("Phalcon\Config\Config")
                ]
            );
        }

        var connection = this->getConnectionName();

        packet->setConnection(connection);

        return packet;
    }

    public function getNickname() -> string
    {
        return $this->getConfig()->nickname;
    }

    public function getPacketFactory() -> <AbstractFactory>
    {
        return this->getDi()->getShared(this->getFactoryClass());
    }

    protected function getFactoryClass() -> string
    {
        return "TwistersFury\\ChatBot\\Irc\\Packet\\Factory";
    }

    public function buildPacket(<IncomingPacket> message) -> <OutgoingPacket>
    {
        return this->getDi()->get(
            "TwistersFury\\ChatBot\\Irc\\Packet\\PrivateMessage",
            [
                this->getDi()->get(
                    "Phalcon\\Config\\Config",
                    [[
                        "to": message->getTo(),
                        "params": [
                            1: message->getMessage()
                        ]
                    ]]
                )
            ]
        );
    }
}
