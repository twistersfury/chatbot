namespace TwistersFury\ChatBot\Irc\Packet;

use TwistersFury\ChatBot\Connection\Config\Channel;
use TwistersFury\ChatBot\Connection\Packet\Packet;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\User;

use TwistersFury\ChatBot\Connection\Packet\Interfaces\Packet as PacketInterface;

use Serializable;

abstract class AbstractPacket extends Packet
{
    const MESSAGE_PRIVATE = 0;
    const MESSAGE_NOTICE = 1;
    const MESSAGE_RAW = 2;

    private message = "";
    private connectionName = "irc";

    abstract protected function getMethod() -> string;

    public function getChannelConfig()
    {
        //To Be Implemented
    }

    public function setMessage(string message) -> <AbstractPacket>
    {
        this->getConfig()->set("message", message);

        return this;
    }

    public function getReplacements() -> array
    {
        return [
            "&B" : "\x02",
            "&C" : "\x03"
        ];
    }

    public function getPiece(int! index) -> string
    {
        return this->getConfig()->param[index];
    }

    public function getParentPacket() -> <AbstractPacket>
    {
        return this->getConfig()->packet;
    }

    public function getConnection() -> string
    {
        return this->connectionName;
    }

    public function setConnection(string! connection) -> <PacketInterface>
    {
        let this->connectionName = connection;

        return this;
    }

    public function getMessage() -> string
    {
        if this->getConfig()->has("message") {
            return this->getConfig()->message;
        } elseif unlikely !this->getConfig()->has("params") {
            this->getDi()->get("logger")->warning("Missing Message Parameters");

            return "";
        }

        return join(' ', this->getConfig()->params->toArray());
    }

    protected function getParameters() -> string
    {
        return "";
    }

    protected function getCommandFactory() -> <Factory>
    {
        return this->getDi()->get("commandFactory");
    }

    public function isChannel() -> bool
    {
        return isset(this->getConfig()->params[0]) && substr(this->getConfig()->params[0], 0, 1) === "#";
    }

    public function getFrom() -> <User>
    {
        if this->isChannel() {
            return this->buildChannelUser();
        }

        return parent::getFrom();
    }

    public function getFromUser() -> <User>
    {
        return parent::getFrom();
    }

    private function buildChannelUser() -> <User>
    {
        var name = this->getConfig()->params[0];

        this->getDi()->get("logger")->debug(
            "Build Channel User",
            [
                "name": name
            ]
        );

        return this->getDi()->get(
            "TwistersFury\\ChatBot\\Irc\\Packet\\User",
            [
                this->getDi()->get(
                    "Phalcon\\Config\\Config",
                    [[
                        "displayName": name,
                        "accountName": name,
                        "source": name
                    ]]
                )
            ]
        );
    }
//
//    public function getChannelConfig() -> <Channel>
//    {
//        return this->getDi()->get("configManager")->get(this->getFrom()->getName());
//    }
}
