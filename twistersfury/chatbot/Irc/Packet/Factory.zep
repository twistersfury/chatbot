namespace TwistersFury\ChatBot\Irc\Packet;

use InvalidArgumentException;
use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\AbstractFactory;

class Factory extends AbstractFactory
{
    /**
     * Returns the adapters for the factory
     */
    protected function getServices() -> array
    {
        return [
            "PING"   : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Ping",
            "PONG"   : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Pong",
            "USER"   : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\User",
            "PASS"   : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Pass",
            "NICK"   : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Nickname",
            "NOTICE" : "TwistersFury\\Chatbot\\Irc\\Packet\\Notice",
            "ERROR"  : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\Error",
            "QUIT"   : "TwistersFury\\Chatbot\\Irc\\Packet\\Raw\\QuitPacket",
            "PRIVMSG": "TwistersFury\\ChatBot\\Irc\\Packet\\PrivateMessage",

            // Server Replies
            "001" : "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Welcome",
            "002" : "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\YourHost",
            "003" : "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Created",
            "004" : "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\MyInfo",
            "005" : "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\ISupport",
            "MODE": "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Mode",
            "CAP" : "TwistersFury\\ChatBot\\Irc\\Packet\\Server\\Capability",

            //Outgoing Messages
            AbstractFactory::OUTGOING_CHANNEL : "TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing\\Channel",
            AbstractFactory::OUTGOING_DIRECT : "TwistersFury\\ChatBot\\Irc\\Packet\\Outgoing\\PrivateMessage"
        ];
    }

    public function getInterfaceName() -> string
    {
        return "TwistersFury\\ChatBot\\Connection\\Packet\\Interfaces\\Packet";
    }
}
