namespace TwistersFury\ChatBot\Irc\Packet;

class Join extends AbstractPacket
{
    public function getMethod() -> string
    {
        return "JOIN";
    }

    public function getChannel() -> string
    {
        return this->getConfig()->channel;
    }

    protected function getParameters() -> string
    {
        return this->getChannel() . " ";
    }
}
