namespace TwistersFury\ChatBot\Irc\Packet;

class Notice extends AbstractPacket
{
    protected function getMethod() -> string
    {
        return "NOTICE " . this->getNickname();
    }

    protected function getNickname() -> string
    {
        return "";
    }

    public function getMessage() -> string
    {
        return this->getConfig()->params[1];
    }
}
