namespace TwistersFury\ChatBot\Irc\Packet\Outgoing;

use TwistersFury\ChatBot\Connection\Throttle\Interfaces\ThrottledPacket;
use TwistersFury\ChatBot\Irc\Packet\AbstractPacket;

abstract class AbstractOutgoing extends AbstractPacket implements ThrottledPacket
{
    abstract protected function getMethod() -> string;

    public function buildPacket() -> string
    {
        return str_replace(
            array_keys(this->getReplacements()),
            this->getReplacements(),
            this->buildRawPacket()
        );
    }

    protected function buildRawPacket() -> string
    {
        return this->getMethod() . " " . this->getTo() . " " . this->getParameters() . " :" . this->getMessage();
    }

    public function getTo() -> string
    {
        return this->getConfig()->to;
    }

    public function setTo(string to) -> <AbstractOutgoing>
    {
        this->getConfig()->set("to", to);

        return this;
    }
}
