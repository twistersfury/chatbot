namespace TwistersFury\ChatBot\Irc\Packet\Outgoing;

class Channel extends AbstractOutgoing
{
    protected function getMethod() -> string
    {
        return "PRIVMSG";
    }
}
