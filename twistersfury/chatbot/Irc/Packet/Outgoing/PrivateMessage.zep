namespace TwistersFury\ChatBot\Irc\Packet\Outgoing;

class PrivateMessage extends AbstractOutgoing
{
    protected function getMethod() -> string
    {
        return "PRIVMSG";
    }
}
