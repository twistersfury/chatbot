namespace TwistersFury\ChatBot\Irc\Packet;

use Phalcon\Config\Config;
use Phalcon\Di\Injectable;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\User;

class Parser extends Injectable
{
    public function parse(string! message) -> <Config>
    {
        // Remove Trailing CR\LF
        let message = substr(message, 0, -2);

        string tags, prefix, command, params;

        let tags = "";
        let prefix = "";
        let command = "";
        let params = "";

        if unlikely substr(message, 0, 1) === "@" {
            let tags = substr(message, 1, strpos(message, ' ') - 1);
            let message = str_replace("@" . tags . " ", "", message);
        }

        if likely substr(message, 0, 1) === ":" {
            let prefix = substr(message, 1, strpos(message, ' ') - 1);
            let message = str_replace(":" . prefix . " ", "", message);
        }

        let command = substr(message, 0, strpos(message, ' '));
        let params = str_replace(command . " ", "", message);

        return this->getDi()->get(
            "Phalcon\Config\Config",
            [[
                "adapter": command,
                "user": this->parsePrefix(prefix),
                "params": this->parseParams(params),
                "tags": this->parseTags(tags)
            ]]
        );
    }

    private function parsePrefix(string! prefix) -> <User> | bool
    {
        //alice!alice@8D16A5AD.1F723402.4D480552.IP
        //unreal4.example.irc.com
        if prefix->length() === 0 {
            return false;
        }

        var pieces, namePieces, source, accountName, displayName;

        let pieces = explode('@', prefix, 2);

        if !fetch source, pieces[1] {
            let source = pieces[0];
        } else {
            let namePieces = explode('!', pieces[0], 2);
            let displayName = namePieces[0];
            if !fetch accountName, namePieces[1] {
                let accountName = displayName;
            }
        }

        return this->getDi()->get(
            "TwistersFury\\ChatBot\\Irc\\Packet\\User",
            [
                this->getDi()->get(
                    "Phalcon\\Config\\Config",
                    [[
                        "displayName": displayName,
                        "accountName": accountName,
                        "source": source
                    ]]
                )
            ]
        );
    }

    private function parseParams(string params) -> array
    {
        if unlikely params === null {
            return [];
        }

        var sections = explode(" ", params);
        array parsedParams = [];
        int index = 0;
        bool isLast = false;
        var section;

        for section in sections {
            if substr(section, 0, 1) === ":" {
                let isLast = true;
                let section = substr(section, 1);
            } elseif isLast === true {
                let section = parsedParams[index] . " " . section;
            }

            let parsedParams[index] = section;

            if !isLast {
                let index = index + 1;
            }
        }

        return parsedParams;
    }

    private function parseTags(string tags) -> array
    {
        if likely tags === null {
            return [];
        }

        var rawTags = explode(";", tags);
        array tagList = [];

        var tagValue, tag, splitTag;

        for tag in rawTags {
            let splitTag = explode("=", tag);
            if !fetch tagValue, splitTag[1] {
                let tagValue = true;
            }

            let tagList[splitTag[0]] = tagValue;
        }

        return tagList;
    }
}
