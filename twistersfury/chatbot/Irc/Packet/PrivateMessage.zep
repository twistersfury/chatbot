namespace TwistersFury\ChatBot\Irc\Packet;

use Phalcon\Events\ManagerInterface;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\Message;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\HasCommands;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\HasEvent;

class PrivateMessage extends AbstractPacket implements Message, HasCommands, HasEvent
{
    private eventsManager;

    public function setEventsManager(<ManagerInterface> manager) -> void
    {
        let this->eventsManager = manager;
    }

    public function getEventsManager() -> <ManagerInterface> | null
    {
        return this->eventsManager;
    }

    public function getEventName() -> string
    {
        return "packet:onincoming";
    }

    protected function getMethod() -> string
    {
        return "PRIVMSG";
    }

    public function getQueueOptions() -> array
    {
        return [];
    }

    public function getMessage() -> string
    {
        return this->getConfig()->params[1];
    }

    protected function getParameters() -> string
    {
        return " " . this->getConfig()->to . " ";
    }

    public function buildCommands() -> array
    {
        this->{"logger"}->debug("Building Commands");

        if likely substr(this->getMessage(), 0, 1) !== "!" {
            return [];
        }

        return [
            this->buildCommand()
        ];
    }

    private function buildCommand() -> <CommandInterface>
    {
        this->{"logger"}->debug("Building Command");

        var job = this->getCommandFactory()->load(
            this->parseMessage()
        );

        return job;
    }

    protected function parseMessage() -> <Config>
    {
        return this->getDi()->get("commandParser")->parse(this);
    }
}
