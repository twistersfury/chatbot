namespace TwistersFury\ChatBot\Irc\Packet;

class Raw extends AbstractPacket
{
    public function buildPacket() -> string
    {
        return this->getMessage();
    }

    protected function getMethod() -> string
    {
        return "RAW";
    }
}
