namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Connection\Packet\ErrorInterface;
use TwistersFury\ChatBot\Irc\Packet\Raw;

class Error extends Raw implements ErrorInterface
{
    public function getMessage() -> string
    {
        return this->getConfig()->params[0];
    }
}
