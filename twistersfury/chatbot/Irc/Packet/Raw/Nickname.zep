namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class Nickname extends Raw
{
    public function getMessage() -> string
    {
        return "NICK " . this->getConfig()->nickname;

        //    Send('RAW', "NICK {$config['connection']['nickname']}");
    }
}
