namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class Pass extends Raw
{
    public function getMessage() -> string
    {
        return "PASS " . this->getConfig()->password;
    }
}
