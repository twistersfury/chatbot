namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Connection\Packet\KeepAlive;

class Ping extends KeepAlive
{
    public function getSlug() -> string
    {
        return this->getConfig()->params[0];
    }
}
