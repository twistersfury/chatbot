namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Irc\Packet\Raw;
use TwistersFury\ChatBot\Irc\Packet\AbstractPacket;

class Pong extends Raw
{
    public function getMessage() -> string
    {
        return "PONG " . this->getParentPacket()->{"getSlug"}();
    }
}
