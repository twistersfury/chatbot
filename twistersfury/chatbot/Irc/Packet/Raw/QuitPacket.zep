namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class QuitPacket extends Raw
{
    public function getMessage() -> string
    {
        return "QUIT";
    }
}
