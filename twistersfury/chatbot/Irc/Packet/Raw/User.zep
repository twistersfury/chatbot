namespace TwistersFury\ChatBot\Irc\Packet\Raw;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class User extends Raw
{
    public function getMessage() -> string
    {
        return "USER " . this->getConfig()->ident . " " . this->getConfig()->ident . " " . this->getConfig()->ident . " :" . this->getConfig()->realname;

        //    Send('RAW', "USER {$config['connection']['ident']} {$config['connection']['ident']} {$config['connection']['ident']} :{$config['connection']['realname']}");
    }
}
