namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class Capability extends Raw
{
    protected function getMethod() -> string
    {
        return "CAP";
    }

    public function buildPacket() -> string
    {
        return this->getCapability();
    }

    protected function getCapability() -> string
    {
        return this->getConfig()->capability;
    }
}

