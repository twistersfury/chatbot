namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class Created extends Raw
{
    protected function getMethod() -> string
    {
        return "003";
    }
}
