namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class ISupport extends Raw
{
        protected function getMethod() -> string
        {
            return "005";
        }
}
