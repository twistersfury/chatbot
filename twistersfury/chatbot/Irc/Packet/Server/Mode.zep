namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\AbstractPacket;

class Mode extends AbstractPacket
{
    protected function getMethod() -> string
    {
        return "MODE";
    }
}
