namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class MyInfo extends Raw
{
    protected function getMethod() -> string
    {
        return "004";
    }
}
