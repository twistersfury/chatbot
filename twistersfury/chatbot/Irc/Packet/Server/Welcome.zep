namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\AbstractPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\HasCommands;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\HasEvent;

class Welcome extends AbstractPacket implements HasCommands
{
    protected function getMethod() -> string
    {
        return "001";
    }

    public function getEventName() -> string
    {
        return "packet:onWelcome";
    }

    public function buildCommands() -> array
    {
        var channel;
        array commands = [];

        this->{"logger"}->debug("Welcome Packet");

        for channel in this->getDi()->get("config")->connection->get(this->getConnection())->channels->getIterator() {
            let commands[] = this->getDi()->get(
                "TwistersFury\\ChatBot\\Command\\Join",
                [
                    this->getDi()->get(
                        "Phalcon\\Config\\Config",
                        [[
                            "channel": channel,
                            "packet": this
                        ]]
                    )
                ]
            );
        }

        return commands;
    }
}
