namespace Twistersfury\ChatBot\Irc\Packet\Server;

use TwistersFury\ChatBot\Irc\Packet\Raw;

class YourHost extends Raw
{
        protected function getMethod() -> string
        {
            return "002";
        }
}
