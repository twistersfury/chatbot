namespace TwistersFury\ChatBot\Job;

use TwistersFury\Phalcon\Queue\Job\AbstractJob;
use TwistersFury\Phalcon\Queue\Job\JobInterface;

class RegisterListener extends AbstractJob
{
    public function handle() -> <JobInterface>
    {
        this->getDi()->get("eventsManager")->attach(
            this->getConfig()->event,
            this->getDi()->get(
                this->getConfig()->listener
            )
        );

        return this;
    }
}

