namespace TwistersFury\ChatBot\Listener\Interfaces\Packet;

use Phalcon\Events\Event;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;

interface Incoming
{
    public function onIncoming(<Event> event, <IncomingPacket> packet, array data = []) -> bool;
}
