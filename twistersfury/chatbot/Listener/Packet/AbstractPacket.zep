namespace TwistersFury\ChatBot\Listener\Packet;

use Phalcon\Config\Config;
use Phalcon\Events\Event;
use TwistersFury\Shared\Di\Injectable;
use TwistersFury\ChatBot\Connection\Packet\Builder;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\Shared\Di\Interfaces\InitializationAware;
use TwistersFury\ChatBot\Connection\Packet\User;

abstract class AbstractPacket extends Injectable implements InitializationAware
{
    public function initialize() -> void
    {
        this->getDi()->get("logger")->debug(
            "Listener Initialized",
            [
                "class": get_class(this)
            ]
        );
    }

    protected function getPacketBuilder() -> <Builder>
    {
        return this->getDi()->get("packetBuilder");
    }

    public function sendMessage(string message, <IncomingPacket> packet) -> <AbstractPacket>
    {
        this->getDi()->get("logger")->debug(
            get_class(this) . "::sendMessage",
            [
                "message": message
            ]
        );

        if likely this->isEnabled() {
            this->getPacketBuilder()->sendMessage(message, packet);
        }

        return this;
    }

    protected function isEnabled() -> bool
    {
        return (bool) this->getCacheOrConfigValue("enabled", true);
    }

    protected function getListenerConfig() -> <Config>
    {
        if !this->getConfig()->has("listenerConfig") || !this->getConfig()->listenerConfig->has(get_class(this)) {
            return this->getDi()->get("Phalcon\\Config\\Config");
        }

        return this->getConfig()->listenerConfig->get(get_class(this));
    }

    protected function getCacheOrConfigValue(string! key, var defaultValue = null) -> var
    {
        var cacheKey = this->buildCacheKey(key);

        if this->getCache()->has(cacheKey) {
            return this->getCache()->get(cacheKey);
        }

        return this->getListenerConfig()->get(key, defaultValue);
    }

    public function sendPacket(<OutgoingPacket> packet) -> <AbstractPacket>
    {
        this->getPacketBuilder()->handleMessage(packet);

        return this;
    }

    public function onIncoming(<Event> event, <IncomingPacket> packet, array data = []) -> bool
    {
        this->getDi()->get("logger")->debug(
            "onIncoming",
            [
                "class": get_class(this),
                "packet": get_class(packet),
                "message": packet->getMessage(),
                "from": packet->getFrom()->getName(),
                "user": packet->getFromUser()->getName()
            ]
        );

        return true;
    }

    protected function buildCacheKey(string! suffix) -> string
    {
        return str_replace("\\", ".", get_class(this)) . "." . suffix;
    }
}
