namespace TwistersFury\ChatBot\Listener\Packet;

use Phalcon\Events\Event;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\ChatBot\Listener\Interfaces\Packet\Incoming;


class Curse extends AbstractPacket implements Incoming
{
    public function onIncoming(<Event> event, <IncomingPacket> packet, array data = []) -> bool
    {
        parent::onIncoming(event, packet, data);

        if stripos(packet->getMessage(), "curse") !== false {
            this->sendMessage(
                "Ah, curse your sudden but inevitable betrayal!",
                packet
            );
        }

        return true;
    }
}
