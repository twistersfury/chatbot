namespace TwistersFury\ChatBot\Listener\Packet;

use Phalcon\Events\Event;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\ChatBot\Listener\Interfaces\Packet\Incoming;


class Quotes extends AbstractPacket implements Incoming
{
    private quotes = [] {
        get
    };

    public function initialize() -> void
    {
        parent::initialize();

        if this->getDi()->get("config")->has("quotes") {
            let this->quotes = array_change_key_case(
                this->getDi()->get("config")->quotes->toArray()
            );

            this->getDi()->get("logger")->debug(
                "Quotes Listner",
                [
                    "quotes": this->getQuotes()
                ]
            );
        }
    }

    public function onIncoming(<Event> event, <IncomingPacket> packet, array data = []) -> bool
    {
        parent::onIncoming(event, packet, data);

        var response = this->checkPacket(packet->getMessage());
        if "string" === typeof response {
            this->sendMessage(
                response,
                packet
            );
        }

        return true;
    }

    private function checkPacket(string! message) -> string | bool
    {
        let message = this->normalizeMessage(message);

        var keyword, response;

        for keyword, response in this->getQuotes() {
            if strpos(message, keyword) !== false {
                return response;
            }
        }

        return false;
    }

    private function normalizeMessage(string! message) -> string
    {
        return strtolower(message);
    }
}
