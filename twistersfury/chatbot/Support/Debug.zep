%{
// top statement before namespace, add to after headers
#ifdef COVERAGE
extern void __gcov_flush(void);
#endif
}%

namespace TwistersFury\ChatBot\Support;

use Phalcon\Support\Debug as phDebug;

class Debug extends phDebug
{
    public static function flushCoverage() -> void
    {
            %{
               #ifdef COVERAGE
                 __gcov_flush();
               #endif
            }%
    }
}
