namespace TwistersFury\ChatBot\Support;

class Version
{
    public function get()
    {
        return phpversion("tf_chatbot");
    }
}
