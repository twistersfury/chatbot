namespace TwistersFury\ChatBot\Twitch\Driver;

use TwistersFury\ChatBot\Irc\Driver\AbstractIrc;
use TwistersFury\ChatBot\Connection\Driver\DriverInterface;

class Irc extends AbstractIrc
{
    public function initialize() -> <DriverInterface>
    {
        parent::initialize();

        this->getDi()->get("logger")->debug("Twitch IRC Initialize");

        this->{"connectionManager"}->getEventsManager()->attach(
            "packet",
            this->getDi()->get("TwistersFury\\ChatBot\\Twitch\\Listener\\Packet")
        );

        return this;
    }

    protected function sendUser() -> <AbstractIrc>
    {
        return this;
    }
}
