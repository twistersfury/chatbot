namespace TwistersFury\ChatBot\Twitch\Listener;

use Phalcon\Config\Config;
use TwistersFury\ChatBot\Connection\Packet\Builder;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\IncomingPacket;
use TwistersFury\ChatBot\Connection\Packet\Interfaces\OutgoingPacket;
use TwistersFury\ChatBot\Listener\Packet\AbstractPacket;

class Packet extends AbstractPacket
{
    public function onWelcome(<Event> event, <IncomingPacket> packet, array data = []) -> void
    {
        this->sendPacket(this->buildCapabilityPacket("membership"));
        this->sendPacket(this->buildCapabilityPacket("tags"));
        this->sendPacket(this->buildCapabilityPacket("commands"));
    }

    protected function buildCapabilityPacket(string! capability) -> <OutgoingPacket>
    {
        return this->getPacketBuilder()->buildPacket(
            "CAP",
            new Config([
                "capability": "twitch.tv/" . capability
            ])
        );
    }
}
